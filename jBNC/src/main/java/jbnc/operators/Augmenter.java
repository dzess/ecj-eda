/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.operators;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeSpecs;
import jbnc.graphs.EdgeWithWeight;
import jbnc.graphs.Graph;
import jbnc.graphs.Vertex;
import jbnc.measures.QualityMeasure;
import jbnc.util.CondMutualInfo;

import java.util.LinkedList;
import java.util.List;

/**
 *  Abstract BNC Augmenter operator.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public abstract class Augmenter extends Operator {

  protected CondMutualInfo cmi = null;


  /**
   * @param  cm
   * @param  names
   * @param  activeNodes
   * @return Created graph.
   * @exception  Exception
   */
  protected static Graph createFullGraph(
      double[][] cm,
      LinkedList activeNodes,
      AttributeSpecs[] names) throws Exception {
    if (cm == null || activeNodes == null) {
      return null;
    }

    int size = cm.length;
    if (size == 0) {
      return null;
    }

    Graph graph = new Graph();

    Vertex[] vertices = new Vertex[names.length];
    for (int i = 0; i < names.length; ++i) {
      Vertex newVertex = new Vertex(names[i].getName(), i);
      vertices[i] = newVertex;
      graph.addVertex(newVertex);
    }

    for (int i = 0; i < size; ++i) {
      if (!activeNodes.contains(new Integer(i))) {
        continue;
      }

      for (int j = i + 1; j < size; ++j) {
        if (!activeNodes.contains(new Integer(j))) {
          continue;
        }

        EdgeWithWeight e = new EdgeWithWeight(vertices[i], vertices[j],
            -cm[i][j]);
        graph.addEdge(e);
      }
    }

    return graph;
  }


  public abstract Result train(List gamma,
                               List lambda, jbnc.util.FrequencyCalc fc,
                               QualityMeasure qm,
                               boolean usePriors,
                               double alpha_ijk) throws Exception;


  public class Result {
    public BayesNet bayesNet = null;
    public Double quality = null;
  }

}
