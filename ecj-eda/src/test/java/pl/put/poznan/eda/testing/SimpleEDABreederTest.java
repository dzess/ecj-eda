package pl.put.poznan.eda.testing;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import pl.put.poznan.eda.IMatchable;
import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.IModelUpdater;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.SimpleEDABreeder;
import ec.EvolutionState;
import ec.Individual;
import ec.Population;
import ec.Species;
import ec.util.Parameter;

public class SimpleEDABreederTest extends ECJTestingBase {

    private SimpleEDABreeder breeder;

    private Model model;
    private Individual individual;

    private IModelUpdater updaterMock;
    private IModelSampler samplerMock;

    @Before
    public void set_up() {

        this.setUpEvolutionStateBasic();
        this.setUpBase();

        // this value is required by SimpleEDABreeder
        state.breedthreads = 1;

        // mock the evolution state because it is too expensive
        // to provide it properly configured
        model = mock(Model.class);
        individual = mock(Individual.class);

        updaterMock = mock(IModelUpdater.class);
        samplerMock = mock(IModelSampler.class);

        this.generateMatchableStubs(samplerMock);
        this.generateMatchableStubs(updaterMock);

        Parameter updaterParameter = base.push(SimpleEDABreeder.P_UPDATER);
        Parameter samplerParameter = base.push(SimpleEDABreeder.P_SAMPLER);
        Parameter exchangeParameter = base.push(SimpleEDABreeder.P_EXCHANGE_RATIO);
        Parameter modelParam = SimpleEDABreeder.U_MODEL;
        Parameter indivudalParama = SimpleEDABreeder.U_INDIVIDUAL;

        // always true elements
        when(stateParameters.getInt(any(Parameter.class), any(Parameter.class), anyInt())).thenReturn(1);
        when(stateParameters.getDouble(eq(exchangeParameter), any(Parameter.class), anyDouble())).thenReturn(-1.0);

        this.generateGetInstanceForParameters(updaterParameter, updaterMock);
        this.generateGetInstanceForParameters(samplerParameter, samplerMock);
        this.generateGetInstanceForParameters(modelParam, model);
        this.generateGetInstanceForParameters(indivudalParama, individual);

        // create SUT
        breeder = new SimpleEDABreeder();
    }

    @SuppressWarnings("unchecked")
    private void generateMatchableStubs(IMatchable mock) {
        when(mock.matchIndividualType(any(Class.class))).thenReturn(true);
        when(mock.matchModelType(any(Class.class))).thenReturn(true);
    }

    @Test
    public void default_values_check() {
        breeder.setup(state, base);

        // check the values of defaults
        Assert.assertEquals(0.5, breeder.getExchangeRatio());
    }

    @Test(expected = IllegalStateException.class)
    public void setting_too_many_breedthreads_results_in_state_log() {
        state.breedthreads = 2;
        breeder.setup(state, base);
    }

    @Test(expected = IllegalStateException.class)
    public void setting_too_many_subpopulation_results_in_state_log() {

        when(stateParameters.getInt(any(Parameter.class), any(Parameter.class), anyInt())).thenReturn(2);
        breeder.setup(state, base);
    }

    @Test
    public void updater_and_sampler_is_instianizated_during_setup() {

        breeder.setup(state, base);

        // verification of setup being called
        verify(updaterMock, times(1)).setup(eq(state), any(Parameter.class));
        verify(samplerMock, times(1)).setup(eq(state), any(Parameter.class));

        // verification of match model being called
        verify(updaterMock, times(1)).matchModelType(model.getClass());
        verify(samplerMock, times(1)).matchModelType(model.getClass());

        // the verifying individual type methods must be called
        verify(updaterMock, times(1)).matchIndividualType(individual.getClass());
        verify(samplerMock, times(1)).matchIndividualType(individual.getClass());

        // the marking methods must be called
        verify(updaterMock, times(1)).markIndividualType(individual.getClass());
        verify(samplerMock, times(1)).markIndividualType(individual.getClass());
    }

    @Ignore(value = "trouble some to write this test with this level of entanglement")
    public void breeder_incorporates_values_from_sampler_into_new_subpopulation() {
        Individual ind2 = mock(Individual.class);
        Individual ind1 = mock(Individual.class);

        // setting up the sample methods
        List<Individual> individuals = new LinkedList<Individual>();
        individuals.add(ind1);
        individuals.add(ind2);
        when(samplerMock.sampleModel(any(Model.class), anyInt(), any(Species.class), any(EvolutionState.class))).thenReturn(individuals);

        // setting up cloning of the population
        Population mockPopulation = mock(Population.class);
        Population newMockPopoulation = mock(Population.class);

        state.population = mockPopulation;
        when(mockPopulation.emptyClone()).thenReturn(newMockPopoulation);

        breeder.setup(state, base);
        breeder.breedPopulation(state);

        // do some assertion about new population created

    }

    @Test(expected = IllegalStateException.class)
    public void not_valid_exchange_ration_logs_an_error_in_state_classes_1() {
        Parameter exchange = base.push(SimpleEDABreeder.P_EXCHANGE_RATIO);
        when(stateParameters.getDouble(eq(exchange), any(Parameter.class), anyDouble())).thenReturn(-2.0);
        breeder.setup(state, base);
    }

    @Test(expected = IllegalStateException.class)
    public void not_valid_exchange_ration_logs_an_error_in_state_classes_2() {
        Parameter exchange = base.push(SimpleEDABreeder.P_EXCHANGE_RATIO);
        when(stateParameters.getDouble(eq(exchange), any(Parameter.class), anyDouble())).thenReturn(2.0);
        breeder.setup(state, base);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void not_valid_updater_logs_an_error_in_state_classes() {

        when(updaterMock.matchModelType(any(Class.class))).thenReturn(false);
        breeder.setup(state, base);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void not_valid_sampler_logs_an_error_in_state_classes() {
        when(samplerMock.matchModelType(any(Class.class))).thenReturn(false);
        breeder.setup(state, base);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void not_valid_sampler_ind_logs_an_error_in_state_classes() {
        when(samplerMock.matchIndividualType(any(Class.class))).thenReturn(false);
        breeder.setup(state, base);
    }

    @SuppressWarnings("unchecked")
    @Test(expected = IllegalStateException.class)
    public void not_valid_updater_ind_logs_an_error_in_state_classes() {
        when(updaterMock.matchIndividualType(any(Class.class))).thenReturn(false);
        breeder.setup(state, base);
    }

}
