package pl.put.poznan.eda.testing.exchanger;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.exchanger.TopExchanger;
import pl.put.poznan.eda.testing.ECJTestingBase;
import ec.Individual;
import ec.Subpopulation;

public class TopExchangerTest extends ECJTestingBase {

    private TopExchanger exchanger;

    @Before
    public void set_up() {
        this.setUpBase();
        this.setUpEvolutionStateBasic();

        exchanger = new TopExchanger();
    }

    @Test
    public void inporporate_empty_list() {
        int indSize = 5;
        List<Individual> emptyList = new LinkedList<Individual>();
        Subpopulation srcSubpopoulation = Mockito.mock(Subpopulation.class);
        Subpopulation dstSubpopulation = Mockito.mock(Subpopulation.class);

        dstSubpopulation.individuals = new Individual[indSize];
        srcSubpopoulation.individuals = new Individual[indSize];

        for (int i = 0; i < srcSubpopoulation.individuals.length; i++) {
            srcSubpopoulation.individuals[i] = Mockito.mock(Individual.class);
        }

        exchanger.incorporate(emptyList, srcSubpopoulation, dstSubpopulation, state);

        // verify that the source subpopulation has the full array of individuals
        for (int j = 0; j < dstSubpopulation.individuals.length; j++) {
            Individual individual = dstSubpopulation.individuals[j];
            Assert.assertSame(srcSubpopoulation.individuals[j], individual);
        }

    }

    @Test
    public void incorporate_some_inviduals() {
        int indSize = 5;
        Individual ind1 = Mockito.mock(Individual.class);
        Individual ind2 = Mockito.mock(Individual.class);
        List<Individual> notEmptyList = new LinkedList<Individual>();
        notEmptyList.add(ind1);
        notEmptyList.add(ind2);

        Subpopulation srcSubpopoulation = Mockito.mock(Subpopulation.class);
        Subpopulation dstSubpopulation = Mockito.mock(Subpopulation.class);

        dstSubpopulation.individuals = new Individual[indSize];
        srcSubpopoulation.individuals = new Individual[indSize];

        for (int i = 0; i < srcSubpopoulation.individuals.length; i++) {
            srcSubpopoulation.individuals[i] = Mockito.mock(Individual.class);
        }

        exchanger.incorporate(notEmptyList, srcSubpopoulation, dstSubpopulation, state);

        // verify that the destination array is almost same as source
        for (int j = 2; j < dstSubpopulation.individuals.length; j++) {
            Individual individual = dstSubpopulation.individuals[j];
            Assert.assertSame(srcSubpopoulation.individuals[j], individual);
        }

        // verify that individuals from non empty list are present
        Assert.assertSame(ind1, dstSubpopulation.individuals[0]);
        Assert.assertSame(ind2, dstSubpopulation.individuals[1]);
    }

    @Test
    public void incorpoate_all_individuals() {
        int indSize = 5;
        List<Individual> fullList = new LinkedList<Individual>();
        Subpopulation srcSubpopoulation = Mockito.mock(Subpopulation.class);
        Subpopulation dstSubpopulation = Mockito.mock(Subpopulation.class);

        dstSubpopulation.individuals = new Individual[indSize];
        srcSubpopoulation.individuals = new Individual[indSize];

        for (int i = 0; i < srcSubpopoulation.individuals.length; i++) {
            srcSubpopoulation.individuals[i] = Mockito.mock(Individual.class);
            fullList.add(Mockito.mock(Individual.class));
        }

        exchanger.incorporate(fullList, srcSubpopoulation, dstSubpopulation, state);

        // verify that the source subpopulation has the full array of individuals
        for (int j = 0; j < dstSubpopulation.individuals.length; j++) {
            Individual individual = dstSubpopulation.individuals[j];
            Assert.assertSame(fullList.get(j), individual);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorporate_too_many_individuals_result_in_exception() {
        int indSize = 3;
        List<Individual> fullList = new LinkedList<Individual>();
        Subpopulation srcSubpopoulation = Mockito.mock(Subpopulation.class);
        Subpopulation dstSubpopulation = Mockito.mock(Subpopulation.class);

        dstSubpopulation.individuals = new Individual[indSize];
        srcSubpopoulation.individuals = new Individual[indSize];

        // list will be of size 5
        for (int i = 0; i < 5; i++) {
            fullList.add(Mockito.mock(Individual.class));
        }

        // should throw an exception
        exchanger.incorporate(fullList, srcSubpopoulation, dstSubpopulation, state);
    }

}
