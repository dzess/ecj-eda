/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc;

import jbnc.dataset.AttributeType;
import jbnc.dataset.Dataset;

/**
 *  Reads data sets in C4.5 format and converts representtion of discrete values
 *  to integers (0 to n-1).
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class ReVal {
  /**
   *  Description of the Method
   *
   * @param  args  The command line arguments
   */
  public static void main(String[] args) {
    ReVal reVal = new ReVal();
    reVal.run(args);
  }


  /**
   * @param  ext      Description of Parameter
   * @param  inStem   Description of Parameter
   * @param  outStem  Description of Parameter
   */
  private static void convert(String ext, String inStem, String outStem) {
    Dataset dataset = new Dataset();

    try {
      // Read
      System.out.print(ext + " ");
      dataset.openC45(inStem + ".names", inStem + ext);

      // Change values of attributes, but skip the class variable
      for (int i = 0; i < dataset.names.length - 1; ++i) {
        if (dataset.names[i].getType() == AttributeType.DISCRETE) {
          dataset.names[i].setType(AttributeType.DISCRETE_N,
              dataset.names[i].getStates().length);

          System.out.print(".");
        }
      }

      // Write
      dataset.saveC45(outStem + ".names", outStem + ext);
      System.out.println(" Done.");
    } catch (Exception e) {
      System.out.println("Failed: " + e.getMessage());
    }
  }


  private void run(String[] args) {
    try {
      String usageMsg = "\nUSAGE: java ReVal in_file_stem out_file_stem\n";

      System.out.println("\nReVal - Change values of discrete attributes to integers.\n");

      if (args.length != 2) {
        System.out.println(usageMsg);
        return;
      }

      String inFileStem = args[0];
      String outFileStem = args[1];

      System.out.println("Input  file stem: " + inFileStem);
      System.out.println("Output file stem: " + outFileStem);
      System.out.println("");

      convert(".data", inFileStem, outFileStem);
      convert(".test", inFileStem, outFileStem);
      convert(".all", inFileStem, outFileStem);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
