package pl.put.poznan.eda.boa.binominal;

import java.util.LinkedList;
import java.util.List;

import jb.BayesianNetworks.BayesNet;
import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.BOASampler;
import pl.put.poznan.eda.boa.util.resolver.BooleanIndexResolver;
import pl.put.poznan.eda.util.ECJDistributionSampler;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.EvolutionState;
import ec.Individual;
import ec.Species;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;

/**
 * Basic implementation of sampling of the {@linkplain BitBOAModel}.
 * 
 * <p>
 * Currently supports all the types supported by {@linkplain BitBOABase}
 * </p>
 * 
 * <p>
 * Basic sampling means that the {@linkplain BayesNet} which is central to the implementation is sampled using the <b>ancestral sampling</b>
 * method. The method is covered in C. M. Bishop book <code>Pattern Recognition Machine Learning</code>. According to some other methods it
 * is mentions in <code>Henrion 1988</code>.
 * </p>
 * 
 * <p>
 * This sampling method has no other parameters
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class BitBOASampler extends BitBOABase implements IModelSampler {

    private static final long serialVersionUID = 734992827781127139L;

    private static final int C_THREAD_NUMBER = 0;

    private BOASampler<Boolean> sampler;

    /**
     * Constructor for the ECJ callback framework.
     */
    public BitBOASampler() {
        this(new ECJDistributionSampler());
    }

    /**
     * Base constructor done for testing purposes and better configuration management
     * 
     * @param distributionSampler
     *            : the sampler of distribution which is to be used
     */
    public BitBOASampler(IDistributionSampler distributionSampler) {
        BooleanIndexResolver indexResolver = new BooleanIndexResolver();
        this.sampler = new BOASampler<Boolean>(distributionSampler, indexResolver);
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {
        state.output.exitIfErrors();
    }

    @Override
    public List<Individual> sampleModel(Model inputModel, int amount, Species species, EvolutionState state) {

        BOAModel model = (BOAModel) inputModel;

        List<Individual> newPopulationInds = new LinkedList<Individual>();

        sampler.reset();

        for (int indIndex = 0; indIndex < amount; indIndex++) {

            // create the new individual
            BitVectorIndividual ind = (BitVectorIndividual) species.newIndividual(state, C_THREAD_NUMBER);

            // sampling of the individual from the model
            Boolean[] sampledGenome = sampler.sampleGenomeFromModel(model, Boolean.class, state);

            // cause java cannot allow covariant collections this is needed
            boolean[] unboxedGenome = new boolean[sampledGenome.length];
            for (int i = 0; i < sampledGenome.length; i++) {
                unboxedGenome[i] = sampledGenome[i];
            }
            ind.setGenome(unboxedGenome);

            // add individual to the new subpopulation
            newPopulationInds.add(ind);
        }
        return newPopulationInds;
    }
}
