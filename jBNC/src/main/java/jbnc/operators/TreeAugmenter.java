/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.operators;

import jbnc.graphs.Graph;
import jbnc.graphs.MinSpanTree;
import jbnc.graphs.Vertex;
import jbnc.measures.QualityMeasure;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Constructs Tree Augmented Naive Bayes classifier (TAN).<br> <br>
 *
 * @author Jarek Sacha
 * @since June 1, 1999
 */
public class TreeAugmenter extends Augmenter {

    /**
     * It is assumed that 'gamma' and 'lambda' have no elements in common. And
     * that indexed contained them refer to numberting of variables represented
     * by 'names'.
     *
     * @param gamma
     * @param lambda
     * @param qm        not used by TreeAugment, and can be set to null.
     * @param fc        Description of Parameter
     * @param usePriors Description of Parameter
     * @param alpha_ijk Description of Parameter
     * @return Result with only the bayesian net property assigned.
     * @throws Exception
     */
    public final Result train(List gamma,
                              List lambda, jbnc.util.FrequencyCalc fc,
                              QualityMeasure qm,
                              boolean usePriors,
                              double alpha_ijk) throws Exception {
        if (gamma.size() <= 1 && (lambda == null || lambda.size() == 0)) {
            Result r = new Result();
            r.bayesNet = ClassDepend.create(null, gamma, fc.names);

            return r;
        }

        // Allocate cmi if needed
        if (cmi == null) {
            cmi = new jbnc.util.CondMutualInfo(fc);
        }

        // Compute conditianal mutual information for each pair of input nodes
        // conditioned on the class node.
        boolean[] cond = new boolean[fc.names.length];
        Iterator gammaIterator = gamma.iterator();
        while (gammaIterator.hasNext()) {
            int v = ((Integer) gammaIterator.next()).intValue();
            cond[v] = true;
        }
        double[][] cm = cmi.get(cond, usePriors, alpha_ijk);

        // Create fully connected graph for input nodes
        LinkedList allConnectedNodes = new LinkedList();
        allConnectedNodes.addAll(gamma);
        if (lambda != null) {
            allConnectedNodes.addAll(lambda);
        }
        Graph fullGraph = createFullGraph(cm, allConnectedNodes, fc.names);

        // Find minimum spanning tree
        MinSpanTree minTree = new MinSpanTree();
        Integer firstGamma = (Integer) gamma.get(0);
        Vertex rootVertex = fullGraph.getVertex(firstGamma.intValue());
        Graph treeGraph = minTree.run(fullGraph, rootVertex);

        // Create and return Byesian network
        Result r = new Result();
        r.bayesNet = ClassDepend.create(treeGraph, gamma, fc.names);

        return r;
    }

}
