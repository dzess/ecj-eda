package pl.put.poznan.eda.testing;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.mockito.Matchers;
import org.mockito.Mockito;

import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.ModelSubpopulation;
import ec.EvolutionState;
import ec.util.Output;
import ec.util.ParamClassLoadException;
import ec.util.Parameter;
import ec.util.ParameterDatabase;
import ec.vector.VectorIndividual;

/**
 * Class which holds the utility code for testing the all sorts of ECJ like classes. Especially {@linkplain EvolutionState} and
 * {@linkplain ParameterDatabase} which are used almost everywhere in ECJ.
 * 
 * Most of the competences of this code can read out just right. For now this utility class is more of trash for reusing some methods - with
 * no proper design in it.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class ECJTestingBase {

    private static final String BASE_STRING = "sample_not_used_string";

    protected Parameter base;

    protected EvolutionState state;
    protected ParameterDatabase stateParameters;
    protected Output stateOutput;

    protected ModelSubpopulation subpopulation;

    protected void setUpEvolutionStateBasic() {
        state = mock(EvolutionState.class);
        stateParameters = mock(ParameterDatabase.class);
        stateOutput = mock(Output.class);

        // make state output fatal throw a new exception
        doThrow(IllegalStateException.class).when(stateOutput).fatal(anyString());

        // mocking the evolution state
        state.parameters = stateParameters;
        state.output = stateOutput;
    }

    protected void prepareSubpopulation(int indSize, Model model, Class<? extends VectorIndividual> cls) throws Exception {
        // mock subpopulation
        subpopulation = Mockito.mock(ModelSubpopulation.class);

        Mockito.when(subpopulation.getModel()).thenReturn(model);

        // create the already had individuals
        subpopulation.individuals = new VectorIndividual[indSize];
        for (int i = 0; i < indSize; i++) {
            subpopulation.individuals[i] = cls.newInstance();
        }
    }

    protected void setUpBase() {
        // create the simples possible implementation of Parameter
        base = new Parameter(BASE_STRING);
    }

    protected void generateGetInstanceForParameters(Parameter parameter, Object returnObj) {
        when(stateParameters.getInstanceForParameter(eq(parameter), any(Parameter.class), any(Class.class))).thenReturn(returnObj);
    }

    protected void generateGetInstanceForParameterEmpty(Parameter parameter) {
        Mockito.when(
                stateParameters.getInstanceForParameter(Matchers.eq(parameter), Matchers.any(Parameter.class), Matchers.any(Class.class)))
                .thenThrow(new ParamClassLoadException("the parameter " + parameter.toString() + " not provided"));

    }

    protected void generateGetIntForParameter(Parameter parameter, int returnInt) {
        when(stateParameters.getInt(eq(parameter), any(Parameter.class), anyInt())).thenReturn(returnInt);
    }

    protected void generateGetIntForParameterWithDefault(Parameter parameter, int returnValue) {
        when(stateParameters.getIntWithDefault(eq(parameter), any(Parameter.class), anyInt())).thenReturn(returnValue);
    }

    protected void generateGetDoubleForParameterExtended(Parameter parameter, double returnDouble) {
        when(stateParameters.getDouble(eq(parameter), any(Parameter.class), anyDouble())).thenReturn(returnDouble);
    }

    protected void generateGetDoubleForParameter(Parameter parameter, double returnDouble) {
        when(stateParameters.getDouble(eq(parameter), any(Parameter.class))).thenReturn(returnDouble);
    }
}