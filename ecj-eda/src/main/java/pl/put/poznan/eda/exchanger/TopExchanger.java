package pl.put.poznan.eda.exchanger;

import java.util.List;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import pl.put.poznan.eda.IExchanger;

/**
 * Replaces the <b>k</b> first individuals from the subpopulation.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class TopExchanger implements IExchanger {

    @Override
    public void incorporate(List<Individual> individuals, Subpopulation srcSubpopoulation, Subpopulation dstSubpopulation,
            EvolutionState state) {

        if (individuals.size() > dstSubpopulation.individuals.length) {
            throw new IllegalArgumentException("list of individuals is too large");
        }
        int index = 0;
        for (Individual individual : individuals) {
            dstSubpopulation.individuals[index] = individual;
            index++;
        }

        // go with the rest from old one (do not reset index)
        while (index < dstSubpopulation.individuals.length) {
            dstSubpopulation.individuals[index] = srcSubpopoulation.individuals[index];
            index++;
        }
    }
}
