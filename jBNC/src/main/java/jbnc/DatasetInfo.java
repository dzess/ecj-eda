/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc;

import jbnc.dataset.AttributeSpecs;
import jbnc.dataset.DatasetReader;
import jbnc.dataset.NamesReader;

import java.io.File;
import java.io.FilenameFilter;
import java.text.DecimalFormat;
import java.util.Vector;

/**
 *  Reads data sets in C4.5 format and prints information about the content.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class DatasetInfo {
  static String toolName = "DatasetInfo";

//  static String version = "v.0.01";


  /**
   *  Description of the Method
   *
   * @param  args  The command line arguments
   */
  public static void main(String[] args) {
    DatasetInfo datasetInfo = new DatasetInfo();
    datasetInfo.run(args);
  }


  /**
   *  Description of the Method
   *
   * @param  filestem  Description of Parameter
   */
  public static void listFilestem(String filestem) {
    File path = new File(".");
    String[] list = path.list(new DirFilter(filestem));
    for (int i = 0; i < list.length; i++) {
      System.out.println("  " + list[i]);
    }
  }


  protected static AttributeSpecs[] infoNames(String fileName) throws Exception {
    // Read names
    AttributeSpecs[] names = NamesReader.open(fileName);
    if (names != null && names.length > 0) {
      int nbAttributes = names.length - 1;
      int nbClasses = names[names.length - 1].getStates().length;
      System.out.println("Number of classes    = " + nbClasses);
      System.out.println("Number of attributes = " + nbAttributes);
    } else {
      throw new Exception("Names file is empty");
    }

    return names;
  }


  protected static void infoData(String fileName, AttributeSpecs[] names) throws Exception {
    try {
      DatasetReader datasetReader = new DatasetReader();
      datasetReader.setDiscardIncompleteCases(true);
      Vector v = datasetReader.open(fileName, names);
      System.out.println("File '" + fileName + "'  has " + v.size() + " cases.");
      countClasses(v, names);
    } catch (Exception e) {
//      System.out.println("Unable to open '"+fileName+"' ["+e.getMessage()+"]");
      System.out.println("Unable to open '" + fileName + "'");
    }
  }


  protected static void countClasses(Vector cases, AttributeSpecs[] names) {
    // Count clases
    int classIndex = names.length - 1;
    String[] classNames = names[classIndex].getStates();
    int[] classCount = new int[classNames.length];
    int nbCases = cases.size();
    for (int i = 0; i < nbCases; ++i) {
      Vector thisCase = (Vector) (cases.get(i));
      int thisClassId = ((Integer) (thisCase.get(classIndex))).intValue();
      ++classCount[thisClassId];
    }

    // Print counts
    DecimalFormat dc = new DecimalFormat("0.00%");
    int total = cases.size();
    for (int i = 0; i < classNames.length; ++i) {
      double perc = 0;
      if (total > 0) {
        perc = classCount[i] / (double) total;
      }

      System.out.println("  " + classNames[i] + " : " + classCount[i] + " [" + dc.format(perc) + "]");
    }

  }


  private void run(String[] args) {
    try {
      String usageMsg = "\nUSAGE: java DatasetInfo file_stem\n";

      System.out.println("\n" + toolName + "\n");

      if (args.length != 1) {
        System.out.println(usageMsg);
        return;
      }

      String fileStem = args[0];

      System.out.println("Filestem: " + fileStem);

//	  listFilestem(argv[0]);

      // Read names
      AttributeSpecs[] names = infoNames(fileStem + ".names");

      // Read all set
      try {
        infoData(fileStem + ".all", names);
      } catch (Exception e) {
      }

      // Read data set
      try {
        infoData(fileStem + ".data", names);
      } catch (Exception e) {
      }

      // Read test set
      try {
        infoData(fileStem + ".test", names);
      } catch (Exception e) {
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

/**
 *  Generate listing of files in a directory.
 *
 * @author     jsa
 * @since      April 1, 2002
 */
class DirFilter implements FilenameFilter {


  String afn;


  DirFilter(String afn) {
    this.afn = afn;
  }


  /**
   *  Return true when file name matches the filter.
   *
   * @param  dir   file directory.
   * @param  name  file name.
   * @return       Description of the Returned Value
   */
  public boolean accept(File dir, String name) {
    // Strip path information
    String f = new File(name).getName();
    return f.indexOf(afn) != -1;
  }
}
