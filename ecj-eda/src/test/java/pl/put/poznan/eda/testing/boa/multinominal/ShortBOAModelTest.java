package pl.put.poznan.eda.testing.boa.multinominal;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.multinominal.ShortBOAModel;
import pl.put.poznan.eda.testing.ECJTestingBase;
import ec.util.Parameter;
import ec.vector.ShortVectorIndividual;

public class ShortBOAModelTest extends ECJTestingBase {

    private static final double DELTA = 0.000001;
    private ShortBOAModel model;
    private int genomeLength;
    private ShortVectorIndividual individual;
    private int maxGene;
    private int minGene;

    @Before
    public void set_up() {
        this.setUpEvolutionStateBasic();
        this.setUpBase();

        model = new ShortBOAModel();

        // here goes setup for further code
        genomeLength = 3;

        individual = new ShortVectorIndividual();
        individual.setGenome(new short[1]);
        individual.setGenomeLength(genomeLength);

        Parameter individualParameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(individualParameter, individual);

        Parameter genomeSizeParameter = BOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(genomeSizeParameter, genomeLength);

        maxGene = 100;
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MAX_GENE, maxGene);

        minGene = 0;
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MIN_GENE, minGene);
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_needs_type_of_individual_to_work() {
        Parameter parameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(parameter, null);

        model.setup(state, parameter);
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_needs_vector_species_to_work() {
        Parameter parameter = BitBOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(parameter, 0);

        model.setup(state, base);
    }

    @Test
    public void boa_model_defaults_min_max_to_null_if_not_specified() {

        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MAX_GENE, -1);
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MIN_GENE, -1);

        model.setup(state, base);

        // the max gene is null
        Assert.assertNull(model.getMaxGene());

        // the min value is always 0
        Assert.assertEquals(new Integer(0), model.getMinGene());
    }

    @Test
    public void boa_model_reads_min_and_max_gene() {
        // no exception up to this point
        model.setup(state, base);

        Assert.assertEquals(new Integer(maxGene), model.getMaxGene());
        Assert.assertEquals(new Integer(minGene), model.getMinGene());
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_does_not_allow_for_non_zero_min_gene() {
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MAX_GENE, 100);
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MIN_GENE, 2);

        model.setup(state, base);
    }

    @Test
    public void boa_model_after_setup_has_no_linked_genomes_no_min_max() {

        // this code should turn off the max min genes
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MAX_GENE, -1);
        this.generateGetIntForParameterWithDefault(ShortBOAModel.U_MIN_GENE, -1);

        model.setup(state, base);

        // go with assertions of created network
        BayesNet network = model.getNetwork();

        int shortSize = Short.MAX_VALUE + 1;

        // variables part of assertions
        ProbabilityVariable[] variables = network.get_probability_variables();
        Assert.assertEquals(genomeLength, variables.length);
        for (ProbabilityVariable pVariable : variables) {
            String[] values = pVariable.get_values();
            Assert.assertEquals(values.length, shortSize);
        }

        // functions part of assertions
        double expectedProbability = 1.0 / (double) (shortSize);
        ProbabilityFunction[] functions = network.get_probability_functions();
        Assert.assertEquals(genomeLength, functions.length);
        for (ProbabilityFunction pFunction : functions) {
            // each function has only one dependent variable (itself)
            Assert.assertEquals(1, pFunction.get_variables().length);
            Assert.assertEquals(shortSize, pFunction.get_values().length);

            for (double probability : pFunction.get_values()) {
                Assert.assertEquals(expectedProbability, probability, DELTA);
            }
        }

    }

    @Test
    public void boa_model_after_setup_has_no_linked_genomes_with_min_max() {
        model.setup(state, base);

        // go with assertions of created network
        BayesNet network = model.getNetwork();

        // number of values goes like this 1-3 it is 1,2,3 meaning 3-1 = 2 (thats why +1)
        int caridnality = maxGene - minGene + 1;

        // variables part of assertions
        ProbabilityVariable[] variables = network.get_probability_variables();
        Assert.assertEquals(genomeLength, variables.length);
        for (ProbabilityVariable pVariable : variables) {
            String[] values = pVariable.get_values();
            Assert.assertEquals(values.length, caridnality);
        }

        // functions part of assertions
        double expectedProbability = 1.0 / (double) caridnality;
        ProbabilityFunction[] functions = network.get_probability_functions();
        Assert.assertEquals(genomeLength, functions.length);
        for (ProbabilityFunction pFunction : functions) {
            // each function has only one dependent variable (itself)
            Assert.assertEquals(1, pFunction.get_variables().length);
            Assert.assertEquals(caridnality, pFunction.get_values().length);

            for (double probability : pFunction.get_values()) {
                Assert.assertEquals(expectedProbability, probability, DELTA);
            }
        }

    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_supports_only_to_the_short_scope() {
        BayesNet network = new BayesNet("sample", 1, 1);
        ProbabilityVariable[] pvs = new ProbabilityVariable[1];
        ProbabilityVariable pv = new ProbabilityVariable();
        String[] values = new String[2];
        values[0] = "123";
        values[1] = "Not Parsable to Short";
        pv.set_values(values);
        pvs[0] = pv;

        // by now the function has some probability variable with wrong
        network.set_probability_variables(pvs);

        model.setNetwork(network);
        model.validate();
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_validates_the_min_max_genes() {
        BayesNet network = new BayesNet("sample", 1, 1);
        ProbabilityVariable[] pvs = new ProbabilityVariable[1];
        ProbabilityVariable pv = new ProbabilityVariable();
        String[] values = new String[2];
        values[0] = "123";

        // this number is expected to exceed the max gene limit
        values[1] = "10000";

        pv.set_values(values);
        pvs[0] = pv;

        // by now the function has some probability variable with wrong
        network.set_probability_variables(pvs);

        model.setMaxGene(999);
        model.setMinGene(0);

        model.setNetwork(network);

        model.validate();
    }

}
