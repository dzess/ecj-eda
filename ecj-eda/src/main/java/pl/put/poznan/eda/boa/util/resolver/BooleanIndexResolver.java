package pl.put.poznan.eda.boa.util.resolver;

import pl.put.poznan.eda.boa.BOASampler;
import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import pl.put.poznan.eda.boa.util.graph.GraphTuple;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;

/**
 * Boolean version of index resolving in {@linkplain ProbabilityFunction} values table.Does fast power using bit shifting and other bitwise
 * tricks to be faster. Yet has to follow standard interface of {@linkplain IIndexResolver} to work against {@linkplain BOASampler}.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class BooleanIndexResolver implements IIndexResolver<Boolean> {

    /**
     * Get the index of probability from the compound representations of {@linkplain ProbabilityFunction}. It searches for the probability
     * of the TRUE. Note that {@linkplain GraphNetwork} has to have partially evaluated nodes, meaning that this function should be used on
     * topologically sorted graph.
     * 
     * @param usedVariables
     *            variables of the node from {@linkplain ProbabilityFunction}.
     * @return index on {@linkplain ProbabilityFunction} with probability of the TRUE basing on already evaluated nodes and theirs values.
     */
    @SuppressWarnings("unchecked")
    private int getIndexBasingOnEvidence(DiscreteVariable[] usedVariables, GraphNetwork graph) {
        int value = 0;

        // skip the first one variable, this i = 1
        for (int i = 1; i < usedVariables.length; i++) {

            // i is the location in the inner for looping construct
            // normally it goes in a nested structure like this
            // for X1 { for X2 { for X3 ... }}}
            DiscreteVariable var = usedVariables[i];

            // searching in set is quite easy (hope) should be O(1), but it is hashtable so constant time might be large
            int inGraphIndex = graph.searchForIndex(var);
            GraphTuple<Boolean> tuple = graph.searchForGraphTuple(inGraphIndex);

            if (tuple.evaluated == false) {
                throw new IllegalStateException("the ordering of variables is not right");
            }

            // now time to find coordinates of the new probability value in looping for binomial values
            // the coordinate is equal to bit shifting
            if (tuple.value.equals(Boolean.TRUE)) {
                // for X bit value do X1
                value = value * 2 + 1;
            } else {
                // for X bit value do X0
                value = value * 2;
            }
        }

        // we are always looking for index of TRUE so change
        // this is assuming that values constructed are ('0','1') always
        return value;
    }

    @Override
    public double[] getProbabilitesGivenEvidance(GraphTuple<Boolean> tuple, GraphNetwork graph) {

        DiscreteVariable[] usedVariables = tuple.function.get_variables();

        // boolean version always holds out two..
        double[] probabilites = new double[2];
        int properIndex = 0;
        if (usedVariables.length == 1) {
            // this should be based on simple assumption that probability values are created: {'0','1'}
            // and that '0' means FALSE and '1' means TRUE (we are searching for probability of FALSE)
            properIndex = 0;
        } else {
            // this method is also VERY specific (it picks the probability of FALSE)
            properIndex = this.getIndexBasingOnEvidence(usedVariables, graph);
        }

        // pick proper probability from the function (and change from FALSE to TRUE)
        double probability = tuple.function.get_value(properIndex);

        // probability true
        probabilites[1] = 1.0 - probability;

        // probability false
        probabilites[0] = probability;

        return probabilites;
    }

    @Override
    public Boolean getValueGivenEvidance(int sampledIndex) {
        if (sampledIndex == 0) {
            return false;
        } else if (sampledIndex == 1) {
            return true;
        } else {
            throw new IllegalStateException("for boolean variables the index should be 0 or 1");
        }
    }

    @Override
    public void reset() {
        // nothing to be cleaned
    }
}
