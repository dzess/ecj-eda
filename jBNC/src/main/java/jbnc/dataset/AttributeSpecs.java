/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.io.Serializable;

/**
 * Describes a single attribute/feature in a data set.
 *
 * @author Jarek Sacha
 * @see jbnc.dataset.Dataset
 * @see jbnc.dataset.NamesReader
 * @see jbnc.dataset.DatasetReader
 * @since June 1, 1999
 */
final public class AttributeSpecs implements Serializable {
    // TODO Evaluate possibility of nor using DISCRETE_N type.

    /**
     * Represents symbolic names for attribute states. It is used when attribute type==DISCRETE. It is used to convert
     * between internal and external representation. For efficiency discrete states are internally represented as
     * integers, starting with 0.
     */
    private String states[];

    private String name;

    private AttributeType type;


    /**
     * By default created attribute with type IGNORE.
     */
    public AttributeSpecs() {
        type = AttributeType.IGNORE;
        name = null;
        states = null;
    }


    /**
     * Can create attribute with type CONTINUOUS or IGNORE.
     *
     * @param name Name of the attribute.
     * @param type Type of the attribute (CONTINUOUS or IGNORE).
     * @throws AttributeException Attribute type is neither CONTINUOUS nor IGNORE.
     */
    public AttributeSpecs(final String name, final AttributeType type) throws AttributeException {
        if (type != AttributeType.CONTINUOUS && type != AttributeType.IGNORE) {
            throw new AttributeException("Attribute type is neither "
                    + "CONTINUOUS nor IGNORE.");
        }
        setName(name);
        setType(type);
    }


    /**
     * Creates a DISCRETE attribute.
     *
     * @param name   Name of the attribute.
     * @param states States of the discrete attribute.
     */
    public AttributeSpecs(final String name, final String[] states) {
        setName(name);
        setType(AttributeType.DISCRETE);
        try {
            setStates(states);
        } catch (AttributeException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Creates a disctete attribute with <tt>n</tt> states (DISCRETE_N).
     *
     * @param name Name of the attribute.
     * @param n    Number of discrete values represented by the attribute (from 0 to n-1).
     * @throws AttributeException <tt>type</tt> is not equal to DISCRETE_N.
     */
    public AttributeSpecs(final String name, final int n) throws AttributeException {
        setName(name);
        setType(AttributeType.DISCRETE_N, n);
    }


    /**
     * Set attribute name.
     *
     * @param name The new Name value
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * Set attribute type.
     *
     * @param type The new Type value
     */
    public void setType(final AttributeType type) {
        this.type = type;
        states = null;
    }


    /**
     * Set attribute type to DISCRETE_N and specify number of states <code>n</code>.
     *
     * @param type Attriute byte type. Has to be DESCRETE_N.
     * @param n    Number of discrete values represented by the attribute (from 0 to n-1).
     * @throws AttributeException <tt>type</tt> is not equal to DISCRETE_N.
     * @throws AttributeException <tt>n</tt> is less then one.
     */
    public void setType(final AttributeType type, final int n) throws AttributeException {
        if (type != AttributeType.DISCRETE_N) {
            setType(AttributeType.IGNORE);
            throw new AttributeException("Type is not equal to DISCRETE_N.");
        }

        if (n < 1) {
            throw new AttributeException("n is less then one.");
        }

        this.type = AttributeType.DISCRETE_N;
        states = new String[n];
        for (int i = 0; i < n; ++i) {
            states[i] = new String("" + i);
        }
    }


    /**
     * Set states of a discrete attribute. Throws exception of the attribute is not discrete.
     *
     * @param states The new States value
     * @throws AttributeException Description of Exception
     */
    public void setStates(final String[] states) throws AttributeException {
        if (type != AttributeType.DISCRETE) {
            throw new AttributeException("AttributeSpecs.setStates(...) can only be called for DISCRETE attributes.");
        }

        this.states = states;
    }


    /**
     * Get attribute name.
     *
     * @return The Name value
     */
    public String getName() {
        return name;
    }


    /**
     * Get attribute type.
     *
     * @return The Type value
     */
    public AttributeType getType() {
        return type;
    }


    /**
     * Returns state name for a state with the given state index. Used for discrete attributes.
     *
     * @param index Description of Parameter
     * @return The State value
     * @throws AttributeException Description of Exception
     */
    public String getState(final int index) throws AttributeException {
        if (type != AttributeType.DISCRETE) {
            throw new AttributeException("AttributeSpecs.getState(...) can only be called for DISCRETE attributes.");
        }

        if (index < 0 || index >= states.length) {
            throw new AttributeException("AttributeSpecs.getState(...) index out of range.");
        }

        return states[index];
    }


    /**
     * Returns state index for a state with the given state name. Used for discrete attributes.
     *
     * @param name Description of Parameter
     * @return The State value
     * @throws AttributeException Description of Exception
     */
    public int getState(final String name) throws AttributeException {
        if (type != AttributeType.DISCRETE) {
            throw new AttributeException("AttributeSpecs.getState(...) can only be called for DISCRETE attributes.");
        }

        for (int i = 0; i < states.length; ++i) {
            if (name.equals(states[i])) {
                return i;
            }
        }

        throw new AttributeException("AttributeSpecs.getState(...) invalid state name.");
    }


    /**
     * Return states of a discrete attribute.
     *
     * @return The States value
     */
    public String[] getStates() {
        return states;
    }


    /**
     * Description of the attribute type or list of attribute states (in type is DISCRETE).
     *
     * @return Description of the Returned Value
     */
    public String toString() {
        String s = (name != null) ? name : "?";
        s += " : ";
        if (type == AttributeType.DISCRETE) {
            for (int i = 0; i < states.length - 1; ++i) {
                s += (states[i] != null) ? states[i] : "?";
                s += ", ";
            }
            if (states.length > 0 && states[states.length - 1] != null) {
                s += states[states.length - 1];
            } else {
                s += "?";
            }
            s += ".";
        } else {
            s += type.toString() + ".";
        }
        return s;
    }


    /**
     * Exception specific to the AttributeSpecs class.
     */
    public static final class AttributeException extends Exception {
        /**
         * Constructor for the AttributeException object.
         */
        public AttributeException() {
        }


        /**
         * Constructor for the AttributeException object.
         *
         * @param msg Description of Parameter
         */
        public AttributeException(final String msg) {
            super(msg);
        }
    }
}
