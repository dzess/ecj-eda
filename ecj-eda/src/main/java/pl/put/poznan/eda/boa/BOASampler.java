package pl.put.poznan.eda.boa;

import java.lang.reflect.Array;
import java.util.List;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import pl.put.poznan.eda.boa.util.graph.GraphTopologicalSorter;
import pl.put.poznan.eda.boa.util.graph.GraphTuple;
import pl.put.poznan.eda.boa.util.resolver.IIndexResolver;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.EvolutionState;

/**
 * Utility class that encapsulates all the actions done be sampler. Class to be used by various implementations of
 * {@linkplain IModelSampler} using <code>composition</code>.
 * 
 * <p>
 * Generic parameter <b>T</b> is needed for this class to be usable as short as and boolean
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class BOASampler<T> {

    private static final int C_THREAD_NUMBER = 0;
    private boolean cached = false;

    private GraphNetwork graph;
    private List<Integer> ranking;

    private final IDistributionSampler distSampler;
    private final IIndexResolver<T> indexResolver;

    public BOASampler(IDistributionSampler distributionSampler, IIndexResolver<T> indexResolver) {
        this.distSampler = distributionSampler;
        this.indexResolver = indexResolver;
    }

    @SuppressWarnings("unchecked")
    public T[] sampleGenomeFromModel(BOAModel model, Class<? extends T> cls, EvolutionState state) {

        BayesNet network = model.getNetwork();

        if (cached == false) {
            // build the normal traversable graph
            ProbabilityFunction[] functions = network.get_probability_functions();
            graph = new GraphNetwork(network);
            GraphTopologicalSorter sorter = new GraphTopologicalSorter();

            for (int i = 0; i < functions.length; i++) {
                graph.addFunction(functions[i]);
            }

            // sort topological ranking of the possible graph variables
            ranking = sorter.sort(graph);
            cached = true;
        }

        T[] result = (T[]) Array.newInstance(cls, model.getGenomeSize());

        // sample functions from ranking using the Henrion sampling
        for (Integer rank : ranking) {
            // we are using tuples instead of the other ways like
            // BayesNet.get_function(variable) because this implementation
            // in BayesNet uses the O(N) searching for function which will be to slow
            GraphTuple<T> tuple = graph.searchForGraphTuple(rank);

            // each tuple is responsible of generating gene value on one gene and
            // setting the local cached model of already set evidence
            result[rank] = this.generateValue(tuple, graph, state);
        }

        // for new round rest all the tuples
        graph.resetGraphTuples();

        return result;
    }

    private T generateValue(GraphTuple<T> tuple, GraphNetwork graph2, EvolutionState state) {

        double[] probablities = indexResolver.getProbabilitesGivenEvidance(tuple, graph);

        // sample the distribution (for index of values)
        int sampledIndex = distSampler.sampleDistribution(probablities, state, C_THREAD_NUMBER);

        // then basing on this index value from values of variables should be taken
        tuple.value = (T) indexResolver.getValueGivenEvidance(sampledIndex);

        tuple.evaluated = true;

        return (T) tuple.value;
    }

    public void reset() {
        this.cached = false;
    }
}
