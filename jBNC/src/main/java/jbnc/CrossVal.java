/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.dataset.DatasetReaderInt;
import jbnc.inducers.*;
import jbnc.util.BNCTester;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

/**
 *  Cross validation classifier tester. Assumes that cross validation files are
 *  already generated an attempts to read them using given file stem. The file
 *  name format is: "stem-repetition-fold". For instance for stem "monk" the
 *  file names could be "monk-0-0.*", "monk-0-1.*", etc. <pre>
 * USAGE: CrossVal {options}
 *
 * where:
 *  -a {algor_name} Algorithm choices: "naive", "TAN", "FAN", "STAN", "STAND".
 *  -c {class_name} Name of the class variable. The default value is 'class'.
 *  -d              Print debugging information.
 *  -f {file_stem}  Load test data in C4.5 format (.names + .test),
 *                   file_stem-?-?.names - file with specification of attributes,
 *                   file_stem-?-?.data  - file with train cases.
 *                   file_stem-?-?.test  - file with test cases.
 *  -n {file_name}  Save constructed naive-Bayes networks to file_name-?-?.bif.
 *                  File is saved in BIF 0.15 format.
 *  -q {q_measure}  Select quality mesure. This relevant for the inducers that
 *                  perform selection of network candidates, like FAN or STAN.
 *                  Diffrent measure have drasticaly diffrent computational
 *                  complexity.
 *                  Quality measures choices:
 *                   LC     - local criterion: log p(c_l|D) [local]
 *                   SB     - standard Bayesian measure with penalty for size
 *                            [global].
 *                   HGS    - Heckerman-Geiger-Chickering measure [global].
 *                   LOO    - leave-one-out cross validation [local].
 *                   CV10   - ten fold cross validation.[local].
 *                   CV1010 - ten fold cross validation averaged ten times
 *                            [local].
 *  -s              Number of smoothing priors to test. Has to be an integer
 *                  greater or equal zero.
 *  -t              Print execution time.
 *
 * EXAMPLE: CrossVal -a TAN -tf monk1
 * EXAMPLE: CrossVal -dta FAN -q LOO -f monk1
 *</pre>
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class CrossVal extends Classifier {

    protected void run(String[] args) {
        setUsageMsg("CrossVal");

        try {
            jbnc.util.Timer t = new jbnc.util.Timer();

            // Print header
            System.out.println("\nCross validation classifier tester.\n");

            // Process command line arguments
            if (!processCommandLine(args)) {
                System.out.println(usageMsg);
                return;
            }

            // Run the test
            run();

            // Print execution time
            if (useTimer) {
                t.println("\nExecution time = ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *  Main function.
     *
     * @param  args  The command line arguments
     */
    public static void main(String[] args) {
        CrossVal crossVal = new CrossVal();
        crossVal.run(args);
    }


    /**
     *  Bayesian network classifier training wrapper.
     *
     * @param  inducer        algorithm used for classifier learning.
     * @param  fc             Description of Parameter
     * @param  usePriors      Description of Parameter
     * @param  alphaK         Description of Parameter
     * @return                constructed Bayesian classifier network.
     * @exception  Exception  Description of Exception
     */
    protected BayesNet train(BayesianInducer inducer, jbnc.util.FrequencyCalc fc,
                             boolean usePriors,
                             double alphaK) throws Exception {
        // Train inducer; create network
        inducer.reset();
        inducer.train(fc, usePriors, alphaK);
        BayesNet net = inducer.getNetwork();

        if (netFileName != null) {
            // Save to a file
            if (debugMode) {
                System.out.println("\nSaving network...");
            }
            FileOutputStream out = new FileOutputStream(netFileName);
            PrintStream pOut = new PrintStream(out);
            net.save_bif(pOut);
        }

        return net;
    }


    /**
     *  Bayesian network classifier testing wrapper.
     *
     * @param  net            Bayesian network classifier.
     * @param  testSet        test data.
     * @param  report         wether to report results on the screen.
     * @return                object containg results of testing.
     * @exception  Exception  Description of Exception
     */
    protected BNCTester.Result test(BayesNet net,
                                    DatasetInt testSet,
                                    boolean report) throws Exception {
        // Test network
        BNCTester tester = new BNCTester();
        tester.setDebugMode(debugMode);
        BNCTester.Result result = null;
        result = tester.test(net, testSet);
//    result = tester.test_old(net, testSet);
        if (report) {
            tester.reportShort();
        }

        return result;
    }


    /*
     *  Execute the cross-validation algorithm.
     */
    protected void run() {
        try {
            jbnc.measures.QualityMeasure qm;
            if (measureName == null) {
                qm = null;
            } else if (measureName.equalsIgnoreCase("LC")) {
                qm = new jbnc.measures.QualityMeasureLC();
            } else if (measureName.equalsIgnoreCase("SB")) {
                qm = new jbnc.measures.QualityMeasureSB();
            } else if (measureName.equalsIgnoreCase("HGC")) {
                qm = new jbnc.measures.QualityMeasureHGC();
            } else if (measureName.equalsIgnoreCase("LOO")) {
                qm = new jbnc.measures.QualityMeasureLCV_LOO();
            } else if (measureName.equalsIgnoreCase("CV10")) {
                qm = new jbnc.measures.QualityMeasureLCV(10, 1);
            } else if (measureName.equalsIgnoreCase("CV55")) {
                qm = new jbnc.measures.QualityMeasureLCV(5, 5);
            } else if (measureName.equalsIgnoreCase("CV1010")) {
                qm = new jbnc.measures.QualityMeasureLCV(10, 10);
            } else {
                System.out.println("Unrecognized quality measure name: " + measureName + "\n");
                System.out.println(usageMsg);
                return;
            }

            // Select algorithm
            BayesianInducer inducer = null;
            String algCode = null;
            if (algorithmName.equalsIgnoreCase("naive")) {
                inducer = new NaiveBayesInducer();
                qm = null;
                algCode = algorithmName;
            } else if (algorithmName.equalsIgnoreCase("TAN")) {
                inducer = new TANInducer();
                qm = null;
                algCode = algorithmName;
            } else if (algorithmName.equalsIgnoreCase("FAN")) {
                inducer = new FANInducer();
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("STAN")) {
                STANInducer stan = new STANInducer();
                stan.setDiscardNonDependent(false);
                inducer = stan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("STAND")) {
                STANInducer stan = new STANInducer();
                stan.setDiscardNonDependent(true);
                inducer = stan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("SFAN")) {
                SFANInducer sfan = new SFANInducer();
                sfan.setDiscardNonDependent(false);
                inducer = sfan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("SFAND")) {
                SFANInducer sfan = new SFANInducer();
                sfan.setDiscardNonDependent(true);
                inducer = sfan;
                algCode = algorithmName + "-" + measureName;
            } else {
                System.out.println("Unrecognized algorithm name: " + algorithmName + "\n");
                System.out.println(usageMsg);
                return;
            }

            System.out.println("File stem: " + fileNameStem);
            System.out.println("Algorithm: " + algorithmName);
            if (qm != null) {
                System.out.println("Quality measure: " + qm.getName());
            }

            boolean report = false;

            // Do cross validation
            Vector errorRates = new Vector();
            while (true) {
                // Generate new file names from stem
                String thisStem;
                thisStem = fileNameStem + "-0-" + String.valueOf(errorRates.size());
                testFileName = thisStem + ".test";
                trainFileName = thisStem + ".data";
                namesFileName = thisStem + ".names";

                // Load new data
                DatasetInt trainSet;

                // Load new data
                DatasetInt testSet;
                try {
                    trainSet = loadDataset(namesFileName, trainFileName);
                    testSet = loadDataset(namesFileName, testFileName);
                } catch (Exception e) {
                    break;
                }

                if (report) {
                    System.out.print("Fold " + errorRates.size() + ": ");
                } else {
                    System.out.print("+");
                }
                jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(trainSet);
                if (qm != null) {
                    qm.setDataset(trainSet);
                }
                inducer.setQualityMeasure(qm);

                if (debugMode) {
                    System.out.println("\nTraining...");
                }
                BayesNet net = train(inducer, fc, false, 0);
                if (!report) {
                    System.out.print(".");
                }

                if (debugMode) {
                    System.out.println("\nTesting...");
                }
                BNCTester.Result r = test(net, testSet, report);

                // Update statistics
                double[] thisSet = new double[nbAlphas + 1];
                errorRates.add(thisSet);

                double thisError = r.nbFail / (double) (r.nbPass + r.nbFail);
                thisSet[0] = thisError;

                for (int i = 1; i <= nbAlphas; ++i) {
                    net = train(inducer, fc, true, i);
                    if (report) {
                        System.out.print("alpha_k = " + i + " : ");
                    } else {
                        System.out.print(".");
                    }
                    r = test(net, testSet, report);
                    thisError = r.nbFail / (double) (r.nbPass + r.nbFail);
                    thisSet[i] = thisError;
                }
                if (!report) {
                    System.out.println("");
                }
            }

            int nbFolds = errorRates.size();
            if (nbFolds <= 0) {
                System.out.println("No data sets found matching file stem: " + fileNameStem);
                return;
            }

            System.out.println("\nFile stem : " + fileNameStem);
            System.out.print("Algorithm : " + algorithmName);
            if (qm != null) {
                System.out.println(" - " + qm.getName());
            } else {
                System.out.println("");
            }

            System.out.println("Nb Folds  : " + nbFolds);

            NumberFormat f = new DecimalFormat("#0.00%");
            int bestA = -1;
            double bestMean = Double.POSITIVE_INFINITY;
            double bestMeanStdDev = 0;
            double bestMinError = 0;
            double bestMaxError = 0;
            for (int a = 0; a <= nbAlphas; ++a) {
                // Mean
                double sum = 0;
                double minError = Double.POSITIVE_INFINITY;
                double maxError = Double.NEGATIVE_INFINITY;
                for (int i = 0; i < errorRates.size(); i++) {
                    double thisError = ((double[]) errorRates.get(i))[a];
                    sum += thisError;
                    if (thisError < minError) {
                        minError = thisError;
                    }
                    if (thisError > maxError) {
                        maxError = thisError;
                    }
                }
                double mean = sum / nbFolds;

                // Variance
                sum = 0;
                for (int i = 0; i < errorRates.size(); i++) {
                    double thisError = ((double[]) errorRates.get(i))[a];
                    sum += (thisError - mean) * (thisError - mean);
                }
                double variance = sum / (nbFolds - 1);
                double meanStdDev = Math.sqrt(variance / nbFolds);

                // Print statistics
                System.out.println("     alpha_" + a + ": Error: "
                        + f.format(mean)
                        + " +- " + f.format(meanStdDev)
                        + " (" + f.format(minError) + " - "
                        + f.format(maxError) + ")");

                if (bestMean > mean) {
                    bestA = a;
                    bestMean = mean;
                    bestMeanStdDev = meanStdDev;
                    bestMinError = minError;
                    bestMaxError = maxError;
                }
            }

            if (nbAlphas > 1) {
                System.out.print("\nBest ");
                System.out.println("alpha_" + bestA + ": Error: "
                        + f.format(bestMean)
                        + " +- " + f.format(bestMeanStdDev)
                        + " (" + f.format(bestMinError) + " - "
                        + f.format(bestMaxError) + ")");
            }

            // Log results to database
            if (tableName != null) {
                debugMode = true;
                int i = fileNameStem.length() - 1;
                for (; i >= 0; --i) {
                    char c = fileNameStem.charAt(i);
                    if (c == '/' || c == '\\') {
                        break;
                    }
                }
                String datasetName = fileNameStem.substring(i + 1);

                System.out.println("Opening database link '" + databaseURL +
                        "' [" + tableName + ":"
                        + datasetName + ":"
                        + algCode + "].");
                jbnc.database.DBLog dbLog = new jbnc.database.DBLog();
                dbLog.open(databaseURL, "", "");
                dbLog.insert(tableName, datasetName, algCode, bestMean * 100,
                        bestMeanStdDev * 100);
                dbLog.close();
            }
        } catch (Exception e) {
            System.out.println("\nError: " + e);
            if (debugMode) {
                e.printStackTrace();
            }
        }
    }


    /**
     *  Dataset loading wrapper.
     *
     * @param  namesFile      Description of Parameter
     * @param  dataFile       Description of Parameter
     * @return                Description of the Returned Value
     * @exception  Exception  Description of Exception
     */
    protected DatasetInt loadDataset(String namesFile,
                                     String dataFile) throws Exception {
        if (debugMode) {
            System.out.println("\nLoading data file: " + dataFile);
        }

        DatasetInt dataset = new DatasetInt();
        if (namesFileName != null) {
            // Load data in C4.5 format

            dataset.setDiscardIncompleteCases(true);
            dataset.openC45(namesFile, dataFile);

        } else {
            // Load data in from comma delimited file with a header

            DatasetReaderInt datasetReader = new DatasetReaderInt();
            datasetReader.setDiscardIncompleteCases(true);
            dataset = (DatasetInt) datasetReader.open(dataFile, className);
        }

        // Remove attributes of type IGNORE
        int nbOrigVars = dataset.names.length;
        dataset.discardAllOfType(AttributeType.IGNORE);

        int nbVars = dataset.names.length;
        if (debugMode && nbVars != nbOrigVars) {
            System.out.println("" + (nbOrigVars - nbVars) + " attributes of type IGNORE have been discarded.");
        }

        if (dataset.names == null || dataset.names.length == 0) {
            throw new Exception("'.names' is empty");
        }

        int nbAttributes = dataset.names.length - 1;
        int nbClasses = dataset.names[nbAttributes].getStates().length;

        if (debugMode) {
//        System.out.println("Number of classes    = "+nbClasses);
            System.out.println("Number of attributes = " + nbAttributes + " + one class.");
        }

        // Verify that attributes are discrete
        for (int i = 0; i < nbAttributes; ++i) {
            AttributeType type = dataset.names[i].getType();
            if (type == AttributeType.DISCRETE || type == AttributeType.DISCRETE_N) {
                continue;
            } else if (type == AttributeType.CONTINUOUS) {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is continuous.");
            } else if (type == AttributeType.IGNORE) {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is to be ignored (?).");
            } else {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is of unknown type = "
                        + dataset.names[i].getType());
            }
        }

        if (debugMode) {
            System.out.println("Number of cases = " + dataset.cases.size());
        }

        return dataset;
    }

}
