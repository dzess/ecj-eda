package pl.put.poznan.eda.umda;

import pl.put.poznan.eda.IMatchable;
import pl.put.poznan.eda.Model;
import ec.Individual;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.ShortVectorIndividual;

/**
 * Base class for the family of class working on {@linkplain DenseUMDAModel} representation of {@linkplain Model}. Holds the logic responsible
 * for {@linkplain IMatchable} interface implementation.
 * 
 * 
 * <p>
 * Used in classes:
 * <ul>
 * <li>{@linkplain DenseUMDASampler}</li>
 * <li>{@linkplain DenseUMDAUpdater}</li>
 * </ul>
 * </p>
 * 
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public abstract class DenseUMDA {

    protected boolean isBitVector = false;
    protected boolean isByteVector = false;

    public DenseUMDA() {
        super();
    }

    public boolean matchModelType(Class<? extends Model> model) {
        if (model.equals(DenseUMDAModel.class))
            return true;
        return false;
    }

    public boolean matchIndividualType(Class<? extends Individual> individual) {
        if (individual.equals(BitVectorIndividual.class)) {
            return true;
        }

        if (individual.equals(ShortVectorIndividual.class)) {
            return true;
        }

        if (individual.equals(ByteVectorIndividual.class)) {
            return true;
        }

        return false;
    }

    public void markIndividualType(Class<? extends Individual> individual) {
        if (individual.equals(BitVectorIndividual.class)) {
            isBitVector = true;
        } else if (individual.equals(ShortVectorIndividual.class)) {
            isBitVector = false;
            isByteVector = false;
        } else if (individual.equals(ByteVectorIndividual.class)) {
            isByteVector = true;
        } else {
            throw new IllegalArgumentException("Passed individual type is not supported");
        }
    }

}