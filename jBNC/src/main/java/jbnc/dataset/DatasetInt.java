/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

/**
 *  Represents a data set of discreate values.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 * @see        jbnc.dataset.NamesReader
 * @see        jbnc.dataset.DatasetReader
 */
public final class DatasetInt extends Dataset {

  /**  Default constructor. */
  public DatasetInt() {
    super();
  }


  /**
   *  Set names to null. Initialize dataset with the array <tt>data</tt> .
   *
   * @param  data           Array of cases <tt>data[case][variable]</tt>
   * @param  attributeSpecs Specifications for attributes
   * @exception  Exception  Description of Exception
   */
  public DatasetInt(int[][] data, AttributeSpecs[] attributeSpecs) throws Exception {
    // TODO: make deep copy of attributeSpecs
    names = attributeSpecs;
    if (data == null || data.length == 0) {
      cases = null;
      return;
    }

    cases = new Vector();
    int caseSize = data[0].length;
    for (int c = 0; c < data.length; ++c) {
      if (caseSize != data[c].length) {
        throw new Exception("Rows of the array are not of equal size.");
      }

      int[] thisCase = new int[caseSize];
      for (int i = 0; i < caseSize; ++i) {
        thisCase[i] = data[c][i];
      }

      cases.add(thisCase);
    }
  }


  /**
   * @param  namesFile
   * @param  dataFile
   * @exception  Exception
   */
  public void openC45(String namesFile, String dataFile) throws Exception {
    clear();
    names = NamesReader.open(namesFile);

    DatasetReader datasetReader = new DatasetReaderInt();
    datasetReader.setDiscardIncompleteCases(discardIncompleteCases);
    cases = datasetReader.open(dataFile, names);
  }


  /**
   * @param  fileName
   * @exception  Exception
   */
  public void saveCasesC45(String fileName) throws Exception {
    if (cases == null || cases.size() == 0) {
      throw new Exception("No cases to save.");
    }

    if (names == null || names.length == 0) {
      throw new Exception("Attribute description is empty");
    }

    // Open output file
    PrintWriter out = new PrintWriter(
        new BufferedWriter(
            new FileWriter(fileName)));

    // Write cases
    int nbCases = cases.size();
    for (int c = 0; c < nbCases; ++c) {
      int[] thisCase = (int[]) cases.get(c);
      if (thisCase == null) {
        continue;
      }

      for (int i = 0; i < names.length; ++i) {
        out.print(thisCase[i]);
        if (i < (names.length - 1)) {
          out.print(",");
        }
      }
      out.println("");
    }

    // Close output file
    out.close();
  }


  /**
   *  Discards all attributes that match the given type. Removes columns from
   *  the data set and entries from the names vector.
   *
   * @param  type  Type of attributes to be discarded.
   */
  public void discardAllOfType(AttributeType type) {
    if (cases == null || names == null) {
      return;
    }

    // Check if there is anything to remove
    int gotAny = 0;
    for (int i = 0; i < names.length; ++i) {
      if (names[i].getType() == type) {
        ++gotAny;
      }
    }

    if (gotAny < 1) {
      return;
    }

    // Fix cases
    for (int c = 0; c < cases.size(); ++c) {
      int[] thisCase = (int[]) cases.get(c);
      if (thisCase == null) {
        continue;
      }

      int[] newCase = new int[names.length - gotAny];
      int index = 0;
      for (int i = 0; i < names.length; ++i) {
        if (names[i].getType() != type) {
          newCase[index] = thisCase[i];
          ++index;
        }
      }

      cases.set(c, newCase);
    }

    // Fix names
    Vector newNames = new Vector();
    for (int i = 0; i < names.length; ++i) {
      if (names[i].getType() != type) {
        newNames.add(names[i]);
      }
    }

    names = new AttributeSpecs[newNames.size()];
    for (int i = 0; i < names.length; ++i) {
      names[i] = (AttributeSpecs) newNames.get(i);
    }
  }


  /**
   *  Performs a shallow copy of itself. Member variables in the clone dataset
   *  are references to variables in the parent dataset. Only primitive types
   *  members, like boolean, are actually copied.
   *
   * @return                                 a clone of this dataset.
   * @exception  CloneNotSupportedException
   */
  public Object clone() throws CloneNotSupportedException {
    DatasetInt aClone = new DatasetInt();

    aClone.cases = this.cases;
    aClone.names = this.names;
    aClone.discardIncompleteCases = this.discardIncompleteCases;

    return aClone;
  }

}
