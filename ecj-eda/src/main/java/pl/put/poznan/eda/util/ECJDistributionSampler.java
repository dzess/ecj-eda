package pl.put.poznan.eda.util;

import ec.EvolutionState;
import ec.util.RandomChoice;

/**
 * Samples the distribution using the {@linkplain RandomChoice} classes.
 * 
 * <p>
 * Does <b>not</b> change the passed arguments like original {@linkplain RandomChoice} implementation does
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class ECJDistributionSampler implements IDistributionSampler {

    /*
     * (non-Javadoc)
     * 
     * @see pl.put.poznan.eda.util.IDistributionSampler#sampleDistribution(double[], ec.EvolutionState, int)
     */
    @Override
    public int sampleDistribution(double[] probablities, EvolutionState state, int thread) {

        double[] organizedProbabilityDist = probablities.clone();
        RandomChoice.organizeDistribution(organizedProbabilityDist);
        double prob = state.random[thread].nextDouble();
        return RandomChoice.pickFromDistribution(organizedProbabilityDist, prob);
    }

    /*
     * (non-Javadoc)
     * 
     * @see pl.put.poznan.eda.util.IDistributionSampler#sampleDistribution(double[], ec.EvolutionState, int)
     */
    @Override
    public boolean sampleDistribution(double probability, EvolutionState state, int thread) {
        return state.random[thread].nextBoolean(probability);
    }
}
