package pl.put.poznan.eda;

import java.util.List;

import ec.EvolutionState;
import ec.Individual;
import ec.Species;

/**
 * Samples the model from the subpopulation.
 * 
 * <p>
 * Algorithm used should be form of some random sampling. Model samplers are connected with the type of {@linkplain Model} classes. Thus
 * should follow the type safety checking before stating the algorithm run, thus should be compatible.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public interface IModelSampler extends IMatchable {

    /**
     * Sample from the {@linkplain Model} and create the new set of {@linkplain Individual}, actually the sequence - duplicates are allowed.
     * This code is invoked by {@linkplain SimpleEDABreeder} every breed so it operates on generations.
     * 
     * @param model
     *            : the model from which sampling needs to be done
     * @param amount
     *            : number of {@linkplain Individual} which will be created
     * @param species
     *            : used as a breeding factory - the new individuals have to be created somehow
     * @param state
     *            : current state of evolution, which should not be used too much in the {@linkplain IModelSampler} implementations
     * @return the newly allocated list of created {@linkplain Individual} in the process of sampling model
     */
    public List<Individual> sampleModel(Model model, int amount, Species species, EvolutionState state);

}
