package pl.put.poznan.eda;

import ec.Breeder;
import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.util.Parameter;

/**
 * Subpopulation used by EDA algorithms.
 * 
 * <p>
 * This kind of {@linkplain Subpopulation} does hold the list (array) of {@linkplain Individual} objects, like its superclass. Those
 * individuals are just samples of population but the entire population might be stored within probability model represented somehow by the
 * {@linkplain Model} classes. Note that how much of population is represented by model how much by instances of individuals is controlled by
 * appropriate {@linkplain Breeder} class.
 * </p>
 * 
 * <p>
 * When {@linkplain ModelSubpopulation} is asked to provide the list of individuals it returns <b>lastly used</b> samples from the model.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public class ModelSubpopulation extends Subpopulation {

    private static final long serialVersionUID = -3053202402483157484L;

    // class of the model
    public static final String P_MODEL = "model";

    @Override
    public void setup(EvolutionState state, Parameter base) {
        super.setup(state, base);

        // get the prototype class for this
        Parameter modelParam = base.push(P_MODEL);
        model = (Model) state.parameters.getInstanceForParameter(modelParam, null, Model.class);
        model.setup(state, modelParam);

        // safe check
        state.output.exitIfErrors();
    }

    private Model model;

    public Model getModel() {
        return this.model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
