package pl.put.poznan.eda.boa.util.graph;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;

/**
 * Stores in the form of graph {@linkplain BayesNet}.
 * 
 * <p>
 * This representation is helpful for using various algorithms on the {@linkplain BayesNet} like for example
 * {@linkplain GraphTopologicalSorter}. Building this structure takes about O(N) operations, plus some constant handicap from the
 * {@linkplain Hashtable} used for storing mapping from {@linkplain ProbabilityVariable} names and indices in the data structures.
 * </p>
 * 
 * <p>
 * Note that {@linkplain GraphTuple} data structure is used to glue {@linkplain ProbabilityVariable} and {@linkplain ProbabilityFunction}
 * alongside with {@linkplain Hashtable}.
 * </p>
 * 
 * <p>
 * Internal representation of graph structure is array successor nodes and array of predecessor nodes. {@linkplain ArrayList} were used
 * because of the O(1) access time. The internal lists (those in arrays) are {@linkplain LinkedList}.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class GraphNetwork {

    private BayesNet network;

    private List<Integer>[] successorArray;
    private List<Integer>[] predecessorArray;

    private Hashtable<String, Integer> map;
    private Hashtable<Integer, GraphTuple> tupleMap;

    public GraphNetwork(BayesNet network) {

        if (network == null) {
            throw new IllegalArgumentException("passed network cannot be null");
        }

        this.network = network;

        int length = network.number_variables();
        this.successorArray = GraphNetworkUtils.createArrayOfLists(length);
        this.predecessorArray = GraphNetworkUtils.createArrayOfLists(length);

        // create mapping between variables and indices
        this.createMappings();
    }

    public void addFunction(ProbabilityFunction probabilityFunction) {
        // TODO: maybe there is some meaning to put assertion that probability function comes from the network

        int variablesCount = probabilityFunction.get_variables().length;
        if (variablesCount < 1) {
            throw new IllegalStateException("function must describe at least one variable");
        }

        // the model needs to be updated (edges are described)
        DiscreteVariable[] variables = probabilityFunction.get_variables();
        // first variable is the P(X|...)
        DiscreteVariable baseVariable = variables[0];
        int indexOfBaseVariable = this.searchForIndex(baseVariable);

        if (variablesCount == 1) {
            // this is a case when function describes only one node (not an edge)
            // no update on the model of graph is needed
        } else {
            // other variables are done the same way (P(..|X1,X2,X3)).
            for (int i = 1; i < variablesCount; i++) {
                int indexOfVariable = this.searchForIndex(variables[i]);

                // create graph representation in a good way
                GraphNetwork.noDuplicateAdd(successorArray[indexOfVariable], indexOfBaseVariable);
                GraphNetwork.noDuplicateAdd(predecessorArray[indexOfBaseVariable], indexOfVariable);
            }
        }

        // update the graph tuples - create binding between variable and function
        // done for extended functions and normal ones
        GraphTuple tuple = tupleMap.get(indexOfBaseVariable);
        tuple.function = probabilityFunction;
    }

    private static void noDuplicateAdd(List<Integer> list, int value) {
        if (list.contains(value)) {
            // skip
        } else {
            list.add(value);
        }
    }

    private void createMappings() {
        ProbabilityVariable[] variables = network.get_probability_variables();

        // initial capacity helps with memory allocation of JVM
        map = new Hashtable<String, Integer>(variables.length);
        tupleMap = new Hashtable<Integer, GraphTuple>(variables.length);

        for (int i = 0; i < variables.length; i++) {
            ProbabilityVariable variable = variables[i];
            String name = variable.get_name();
            if (name == null) {
                throw new IllegalStateException("variables in network should be named");
            }

            // for index map
            map.put(name, i);

            // for tuple map (see that variable is missing for now probability function)
            GraphTuple tuple = new GraphTuple(i);
            tuple.variable = variable;
            tupleMap.put(i, tuple);
        }
    }

    public int searchForIndex(DiscreteVariable baseVariable) {
        String name = baseVariable.get_name();
        return map.get(name);
    }

    public void resetGraphTuples() {
        for (Entry<Integer, GraphTuple> entry : tupleMap.entrySet()) {
            GraphTuple tuple = entry.getValue();
            tuple.value = null;
            tuple.evaluated = false;
        }
    }

    /**
     * Gets the variable and function correlated with current index in the map.
     * 
     * @param index
     *            : index of variable from {@linkplain GraphNetwork}
     * @return the so called {@linkplain GraphTuple}
     */
    public GraphTuple searchForGraphTuple(int index) {
        return tupleMap.get(index);
    }

    /**
     * Gets the successors list fully copied.
     * 
     * @return the deep clone of successor list.
     */
    public List<Integer>[] getSuccessorArrayDeepCopy() {
        return GraphNetworkUtils.getDeepCopy(successorArray);
    }

    /**
     * Gets the predecessor list fully copied.
     * 
     * @return the deep clone of successor list.
     */
    public List<Integer>[] getPredecessorArrayDeepCopy() {
        return GraphNetworkUtils.getDeepCopy(predecessorArray);
    }

    /**
     * Reference access of the predecessor array.
     * 
     * @return predecessor array
     */
    public List<Integer>[] getPredecessorArray() {
        return predecessorArray;
    }

    /**
     * Reference access of the successor array.
     * 
     * @return successor array
     */
    public List<Integer>[] getSuccessorArray() {
        return successorArray;
    }

}