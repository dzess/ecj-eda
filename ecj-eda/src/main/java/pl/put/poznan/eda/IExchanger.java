package pl.put.poznan.eda;

import java.util.List;

import ec.BreedingSource;
import ec.EvolutionState;
import ec.Individual;
import ec.Prototype;
import ec.Subpopulation;

/**
 * Mixes some {@link Individual} into passed {@linkplain Subpopulation}.
 * 
 * <p>
 * Somehow this class resembles the {@linkplain BreedingSource} but for now it is much easier to write this code such a way than trying to
 * fit ECJ system perfectly, moreover the {@linkplain BreedingSource} classes inherit from {@linkplain Prototype} class. So they are not
 * persisted during all time of the experiment - which might be used by some kind of EDA algorithms (which store population over time).
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public interface IExchanger {

    /**
     * 
     * Mixes the passed individuals into passed subpopulation.
     * 
     * @param individuals
     *            : the individuals which are to be incorporated into {@linkplain Subpopulation} individuals array
     * @param dstSubpopulation
     *            : the subpopulation into which the individuals are to be injected
     * @param srcSubpopoulation
     *            : the subpopulation from which the individuals are taken
     * @param state
     *            : current state of evolution
     */
    public void incorporate(List<Individual> individuals, Subpopulation srcSubpopoulation, Subpopulation dstSubpopulation,
            EvolutionState state);
}
