package pl.put.poznan.eda.umda;

import java.util.Map;

import ec.EvolutionState;
import ec.Individual;
import ec.Initializer;
import ec.Population;
import ec.Subpopulation;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.IntegerVectorSpecies;
import ec.vector.ShortVectorIndividual;
import ec.vector.VectorIndividual;
import ec.vector.VectorSpecies;
import pl.put.poznan.eda.Model;

/**
 * The simplest possible model of probability used for {@linkplain VectorSpecies} and {@linkplain VectorIndividual} classes. The
 * representation is vector of probabilities.
 * 
 * <p>
 * This class does <b>not</b> support {@linkplain IntegerVectorIndividual} nor {@linkplain FloatVectorIndividual},
 * {@linkplain DoubleVectorIndividual}. It is because inner design of this classes uses static structure of the array instead of dynamic
 * structures like {@linkplain Map}.
 * </p>
 * 
 * 
 * <p>
 * Works only with the models based on representation of:
 * <ul>
 * <li>{@linkplain BitVectorIndividual}</li>
 * <li>{@linkplain ByteVectorIndividual}</li>
 * <li>{@linkplain ShortVectorIndividual}</li>
 * </ul>
 * </p>
 * 
 * <p>
 * The first table of <code>genomeProbabilites</code> is the cardinality second one is the gene probability. It can be described more like a
 * <code>genomeProbabilites[value][probability]</code> but the <i>value</i> is in reality index of value and the <i>probability</i> is index
 * of probability.
 * </p>
 * 
 * <p>
 * Validates if UMDA models fits the name of the probabilistic model, by checking the all three asumptions of probability theorem.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 */
public class DenseUMDAModel extends Model {

    private static final long serialVersionUID = -1777144379376953857L;

    private int genomeSize;
    private int genomeCardinality;
    private Individual individual;

    public double[][] genomeProbabilites;

    // other parameter outside this class which are needed to determine the final look of this instance
    public static final Parameter U_GENOME_SIZE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(VectorSpecies.P_GENOMESIZE);

    public static final Parameter U_INDIVIDUAL = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(VectorSpecies.P_INDIVIDUAL);

    public static final Parameter U_MAX_GENE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(IntegerVectorSpecies.P_MAXGENE);

    public static final Parameter U_MIN_GENE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(IntegerVectorSpecies.P_MINGENE);

    private Integer maxGene;
    private Integer minGene;

    public int getGenomeSize() {
        return genomeSize;
    }

    public int getGenomeCardinality() {
        return genomeCardinality;
    }

    private double getMarginalProbability() {
        return 1.0 / (double) genomeCardinality;
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {

        // read the genome size
        genomeSize = state.parameters.getInt(U_GENOME_SIZE, null, 2);

        // read the type of species
        individual = (Individual) state.parameters.getInstanceForParameter(U_INDIVIDUAL, null, Individual.class);

        // validation of the genome size etc
        if (genomeSize < 2) {
            state.output.fatal("The passed genome size cannot be lower than 2");
        }
        if (individual == null) {
            state.output.fatal("The individual is needed for safety checking");
        }

        // value (-1) is to deceive ecj lacking code
        maxGene = state.parameters.getIntWithDefault(U_MAX_GENE, null, -1);
        if (maxGene == -1) {
            maxGene = null;
        }

        minGene = state.parameters.getIntWithDefault(U_MIN_GENE, null, -1);
        if (minGene == -1) {
            // default value for min gene is 0 (once again this is the case of jBNC)
            minGene = 0;
        }

        // initialize the genome probabilities (all with 0.5 - its fair for bit)
        if (individual instanceof BitVectorIndividual) {
            // there are only two versions of gene: true or false
            genomeCardinality = 2;
            generateProbabilites();
        } else if (individual instanceof ByteVectorIndividual) {
            // fast power with base 2 (number of distinct values)
            genomeCardinality = 1 << Byte.SIZE;
            generateProbabilites();
        } else if (individual instanceof ShortVectorIndividual) {

            // for the time being, only the short one has implementation for now
            if (maxGene == null) {
                maxGene = (int) Short.MAX_VALUE;
            }

            if (maxGene <= minGene) {
                state.output.fatal("max-gene cannot be lower than min-gene");
            }

            genomeCardinality = maxGene - minGene + 1;
            state.output.warning("Setting gene span to the " + genomeCardinality);

            generateProbabilites();
        } else {
            state.output.fatal("Not supported type of individual" + individual);
        }

        // check if created model was valid
        validate();

        // safe check
        state.output.exitIfErrors();
    }

    private void generateProbabilites() {
        // this is very disturbing here because this UMDA model cannot be used
        // to pull of with genomeCardinality with long value as an argument table
        genomeProbabilites = new double[genomeSize][genomeCardinality];
        for (int j = 0; j < genomeCardinality; j++) {
            for (int i = 0; i < genomeSize; i++) {
                genomeProbabilites[i][j] = this.getMarginalProbability();
            }
        }
    }

    @Override
    public void validate() {

        for (int i = 0; i < genomeSize; i++) {
            double sum = 0;

            // each gene probability must be probability
            for (int j = 0; j < genomeCardinality; j++) {
                double value = genomeProbabilites[i][j];
                if (value > 1.0) {
                    throw new ArithmeticException("breaks (1.0) the probability model on gene" + i + "with distinct value " + j);
                }

                if (value < 0.0) {
                    throw new ArithmeticException("breaks (0.0) the probability model on gene" + i + "with distinct value " + j);
                }

                sum += value;
            }

            // check the sum of probabilities on each gene
            // FIXME: checking the sum of probability is very fragile to getting the
            // floating point issues around -> it requires some smart concept - this is 
            // not the best of getting this proper, 
            if (sum - 1.0 > 0.001) {
                throw new ArithmeticException("breaks (1.0) the probability model on gene " + i + "with sum " + sum);
            }

            if (sum < 0.0) {
                throw new ArithmeticException("breaks (0.0) the probability model on gene " + i + "with sum " + sum);
            }
        }

    }

    public Integer getMaxGene() {
        return maxGene;
    }

    public void setMaxGene(int maxGene) {
        this.maxGene = maxGene;
    }

    public Integer getMinGene() {
        return minGene;
    }

    public void setMinGene(int minGene) {
        this.minGene = minGene;
    }

}
