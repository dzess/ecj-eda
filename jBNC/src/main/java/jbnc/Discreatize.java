/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc;

import gnu.getopt.Getopt;
import jbnc.dataset.AttributeSpecs;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetReader;
import jbnc.dataset.NamesReader;
import jbnc.discretize.Case;
import jbnc.discretize.EntropyDiscretizer;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.Vector;

public class Discreatize {
    private int classIndex;

    static class Config {
        String inputFilestem = null;
        String outputFilestem = null;
        String discretizerInputFile = null;
        String discretizerOutputFile = null;
        String className = "class";
        boolean debugMode = false;
        boolean useTimer = false;
    }

    private Config config;


    private static final String usageMsg =
            "Discretice continous attributes of the input dataset.\n" +
            "USAGE:  Discretize  <options> input_filestem output_filestem\n" +
            "\n" +
            "options:\n" +
            " -c {class_name} Name of the class variable. The default value is 'class'.\n" +
            " -i {file_name} - Load discretizer from a file 'file_name'.\n" +
            " -o {file_name} - Save discretizer to a file 'file_name'.\n" +
            " -d - Print debugging information.\n" +
            " -t - Print execution time.\n" +
            "\n" +
            "EXAMPLE: Discretize iris iris-disc\n" +
            "  Builds discretizer using 'iris.data'. The discretizer is used to\n" +
            "  discretize contents of files 'iris.data', and 'iris.test'. Results\n" +
            "  are written to files 'iris-disc.data' and 'iris-disc.test', respectively.\n" +
            "EXAMPLE: Discretize -o iris.ed.xml iris iris-disc\n" +
            "EXAMPLE: Discretize -i iris.ed.xml iris iris-disc\n" +
            "";

    private AttributeSpecs[] attributeSpecs = null;
    private AttributeSpecs[] discretizedAttributeSpecs = null;
    private EntropyDiscretizer[] discretizers;

    /*
     *
     */
    public void setConfig(Config config) {
        this.config = config;
    }

    /**
     * @throws Exception
     */
    public void run() throws Exception {

        // Read attribute information
        attributeSpecs = NamesReader.open(config.inputFilestem + ".names");

        // Determine class index
        classIndex = -1;
        for (int i = 0; i < attributeSpecs.length; ++i) {
            if (attributeSpecs[i].getName().equalsIgnoreCase(config.className)) {
                classIndex = i;
                break;
            }
        }
        if (classIndex < -1) {
            throw new Exception("Unable to determine which attribute is class attribute.");
        }

        // Check if there is anything to discretize
        if (!hasContinousAttributes(attributeSpecs)) {
            System.out.println("Nothing to do, no continous attributes for discretization found.");
            return;
        }

        // Prepare discretizers for each attribute
        if (config.discretizerInputFile != null) {
            // Load discretizer definition
            discretizers = loadDiscretizers(config.discretizerInputFile);
        } else {
            //Use .data file to build discretizer
            discretizers = buildDiscretizers(config.inputFilestem + ".data");
        }

        // Save discretizers' definitions
        if (config.discretizerOutputFile != null) {
            saveDiscretizers(config.discretizerOutputFile, discretizers);
        }

        final String[] extensions = {".data", ".test", ".all"};
        for (int i = 0; i < extensions.length; i++) {
            String extension = extensions[i];
            discretize(extension);
        }
    }

    /*
     *
     */
    private void discretize(String extension) throws Exception {

        // Read dataset
        DatasetReader datasetReader = new DatasetReader();
        datasetReader.setDiscardIncompleteCases(true);
        Vector v = datasetReader.open(config.inputFilestem + extension, attributeSpecs);

        // Iterate through attributes and discretice continous ones
        int classIndex = attributeSpecs.length - 1;
        for (int i = 0; i < attributeSpecs.length; i++) {
            AttributeSpecs name = attributeSpecs[i];
            if (name.getType() == AttributeType.CONTINUOUS) {
                // Prepare data for creation of discretize
                Case[] cases = new Case[v.size()];
                for (int j = 0; j < v.size(); j++) {
                    Vector thisCase = (Vector) v.elementAt(j);
                    int classId = ((Integer) thisCase.get(classIndex)).intValue();
                    double value = ((Double) thisCase.get(i)).intValue();
                    cases[j] = new Case(classId, value);

                }

                // Create Discreatizer
                EntropyDiscretizer discretizer = new EntropyDiscretizer();
                discretizer.buildDiscretizer(cases);

                // Modify attribute specs
                attributeSpecs[i] = new AttributeSpecs(name.getName(), discretizer.getPartitionNames());

                //Replace attribute values with their discretized form
                for (int j = 0; j < v.size(); j++) {
                    Vector thisCase = (Vector) v.elementAt(j);
                    double value = ((Double) thisCase.get(i)).intValue();
                    int partitionId = discretizer.discretize(value);
                    thisCase.set(i, new Integer(partitionId));
                }
            }
        }

        //TODO: Save dataset
    }

    /*
     *
     */
    private static boolean hasContinousAttributes(AttributeSpecs[] specses) {

        // Check if there are anu continous attributes
        boolean hasContinousAttributes = false;
        for (int i = 0; i < specses.length; i++) {
            AttributeSpecs name = specses[i];
            if (name.getType() == AttributeType.CONTINUOUS) {
                hasContinousAttributes = true;
                break;
            }
        }

        return hasContinousAttributes;
    }


    /*
     *
     */
    private void saveDiscretizers(String discretizerOutputFile,
                                  EntropyDiscretizer[] discretizers)
            throws FileNotFoundException {

        XMLEncoder e = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(discretizerOutputFile)));
        e.writeObject(discretizers);
        e.close();
    }

    /**
     * Build discretizer from a training dataset.
     *
     * @param dataFileName
     * @return
     * @throws Exception
     */
    private EntropyDiscretizer[] buildDiscretizers(String dataFileName)
            throws Exception {

        // Read dataset
        DatasetReader datasetReader = new DatasetReader();
        datasetReader.setDiscardIncompleteCases(true);
        Vector v = datasetReader.open(dataFileName, attributeSpecs);

        EntropyDiscretizer[] discretizers = new EntropyDiscretizer[attributeSpecs.length];
        discretizedAttributeSpecs = new AttributeSpecs[attributeSpecs.length];
        // Iterate through attributes and discretice continous ones
        for (int i = 0; i < attributeSpecs.length; i++) {
            AttributeSpecs name = attributeSpecs[i];
            if (name.getType() == AttributeType.CONTINUOUS) {
                // Prepare data for creation of discretize
                Case[] cases = new Case[v.size()];
                for (int j = 0; j < v.size(); j++) {
                    Vector thisCase = (Vector) v.elementAt(j);
                    int classId = ((Integer) thisCase.get(classIndex)).intValue();
                    double value = ((Double) thisCase.get(i)).intValue();
                    cases[j] = new Case(classId, value);

                }

                // Create Discreatizer
                EntropyDiscretizer discretizer = new EntropyDiscretizer();
                discretizer.buildDiscretizer(cases);
                discretizers[i] = discretizer;

                // Modify attribute specs
                discretizedAttributeSpecs[i] = new AttributeSpecs(name.getName(), discretizer.getPartitionNames());
            } else {
                discretizedAttributeSpecs[i] = attributeSpecs[i];
            }
        }

        return discretizers;
    }

    /*
     *
     */
    private EntropyDiscretizer[] loadDiscretizers(String discretizerInputFile)
            throws FileNotFoundException {

        XMLDecoder d = new XMLDecoder(new BufferedInputStream(new FileInputStream(discretizerInputFile)));
        Object result = d.readObject();
        d.close();
        return (EntropyDiscretizer[]) result;
    }

    /**
     * Process command line arguments.
     *
     * @param argv Description of Parameter
     * @return 'false' when command line is invalid.
     */
    private static Config processCommandLine(String[] argv) {
        Getopt g = new Getopt("ERROR", argv, "c:i:o:dt");

        int c;
        String alphaString = null;
        Config config = new Config();
        while ((c = g.getopt()) != -1) {
            switch (c) {
                case 'c':
                    config.className = g.getOptarg();
                    break;
                case 'i':
                    config.discretizerInputFile = g.getOptarg();
                    break;
                case 'o':
                    config.discretizerOutputFile = g.getOptarg();
                    break;
                case 'd':
                    config.debugMode = true;
                    break;
                case 't':
                    config.useTimer = true;
                    break;
                case '?':
                    break;
                default:
                    System.out.println("ERROR: getopt() returned: " + c);
                    return null;
            }
        }


        //TODO get input and output file stem
        int index = g.getOptind();
        if (index < argv.length) {
            // Staff left on the command line
            System.out.print("ERROR: Too many arguments in the command line. Extraneous: ");
            for (; index < argv.length; ++index) {
                System.out.print(argv[index] + ", ");
            }
            return null;
        }

        if (config.debugMode) {
            System.out.println("Input file stem    = " + config.inputFilestem);
            System.out.println("Output file stem   = " + config.outputFilestem);
            System.out.println("Class name         = " + config.className);
            System.out.println("Discretizer input  = " + config.discretizerInputFile);
            System.out.println("Discretizer output = " + config.discretizerOutputFile);
            System.out.println("Use timer          = " + config.useTimer);
            System.out.println("Debug mode         = " + config.debugMode);
        }

        return config;
    }


    /*
     *
     */
    public static void main(String[] args) {

        try {
            Config config = processCommandLine(args);

            Discreatize discreatize = new Discreatize();
            discreatize.setConfig(config);
            discreatize.run();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("\n" + usageMsg);
        }
    }
}
