package pl.put.poznan.eda.boa.util.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Implementation of topological sorting, using the followers and the predecessor lists. Based on Depth First Search algorithm.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class GraphTopologicalSorter {

    private List<Integer>[] preds;
    private List<Integer>[] succs;

    private boolean[] visited;
    private Stack<Integer> stack;
    private Set<Integer> nodesOnTrack;

    public List<Integer> sort(GraphNetwork graph) {

        preds = graph.getPredecessorArrayDeepCopy();
        succs = graph.getSuccessorArrayDeepCopy();

        this.validate();

        // depth first search of the graph structure (should be O(N))
        int nodesCount = succs.length;
        visited = new boolean[nodesCount];
        // the amount of nodes can fasten the allocation
        nodesOnTrack = new HashSet<Integer>(nodesCount);

        stack = new Stack<Integer>();

        for (int i = 0; i < nodesCount; i++) {
            if (visited[i] == false) {
                dfs(i);
            }
        }

        ArrayList<Integer> result = new ArrayList<Integer>(stack);
        Collections.reverse(result);
        return result;
    }

    private void dfs(int currentNodeIndex) {
        visited[currentNodeIndex] = true;
        nodesOnTrack.add(currentNodeIndex);

        // for each of the following nodes go down
        List<Integer> nexts = succs[currentNodeIndex];
        for (Integer integer : nexts) {

            // check if we are not in a loop
            if (nodesOnTrack.contains(integer)) {
                throw new IllegalStateException("node " + integer + "already on track: " + nodesOnTrack);
            }

            if (visited[integer] == false) {
                dfs(integer);
            }
        }

        nodesOnTrack.remove(currentNodeIndex);
        stack.push(currentNodeIndex);
    }

    private void validate() {
        if (preds.length != succs.length) {
            throw new IllegalArgumentException("Successors and predecessors representations should be complete");
        }

    }
}
