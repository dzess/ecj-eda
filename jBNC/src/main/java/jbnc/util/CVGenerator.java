/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 *  Generates cross validation data sets. For a given dataset it will generate
 *  the same sequence of cross validation sets. The squence can be modified
 *  using methods setSeed() and randomize().
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class CVGenerator {

  protected Random random = new Random(5392648);
  protected ArrayList caseList = null;


  /**
   *  Sets the Cases attribute of the CVGenerator object
   *
   * @param  cases  The new Cases value
   */
  public void setCases(Vector cases) {
    caseList = new ArrayList();
    caseList.addAll(cases);
  }


  /**
   *  Sets the seed of random number generator used for creation of cross
   *  validation sets.
   *
   * @param  seed  the initial seed.
   */
  public void setSeed(long seed) {
    random.setSeed(seed);
  }


  /**
   * @param  cvFolds
   * @param  trainSets  Description of Parameter
   * @param  testSets   Description of Parameter
   */

  /**
   * @param  cvFolds
   * @param  trainSets
   * @param  testSets
   */
  public void generateSets(int cvFolds, Vector[] trainSets, Vector[] testSets) {
    // Shuffle cases
    Collections.shuffle(caseList, random);

    // Create CV sets
    int nbCases = caseList.size();
    double increment = nbCases / cvFolds;
    int windowBegin = 0;
    for (int i = 0; i < cvFolds; ++i) {
      int windowEnd = (int) ((i + 1) * increment + 0.5);

      int windowSize = windowEnd - windowBegin;

      if (trainSets[i] == null) {
        trainSets[i] = new Vector();
      }
      trainSets[i].setSize(nbCases - windowSize);

      if (testSets[i] == null) {
        testSets[i] = new Vector();
      }
      testSets[i].setSize(windowSize);

      int jTrain = 0;
      int j = 0;
      for (; j < windowBegin; ++j, ++jTrain) {
        trainSets[i].set(jTrain, caseList.get(j));
      }

      int jTest = 0;
      for (; j < windowEnd; ++j, ++jTest) {
        testSets[i].set(jTest, caseList.get(j));
      }

      for (; j < nbCases; ++j, ++jTrain) {
        trainSets[i].set(jTrain, caseList.get(j));
      }

      windowBegin = windowEnd;
    }
  }


  /**
   *  Set the internal random number generator to a "random" seed based on
   *  current time.
   */
  public void randomize() {
    random = new Random();
  }
}
