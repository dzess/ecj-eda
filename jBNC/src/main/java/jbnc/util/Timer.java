/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.util;


/**
 *  Measures time intervals in milliseconds.
 *
 * @author     Jarek Sacha, jarek@computer.org
 * @since      June 1, 1999
 */
public class Timer {

  protected long tStart = -1;
  protected long tStop = -1;


  /**  Creates and start the timer.  */
  public Timer() {
    restart();
  }


  /**
   *  Returns time elapsed in milliseconds
   *
   * @return    The TimeMillis value
   */
  public long getTimeMillis() {
    long t = (tStop == -1) ? System.currentTimeMillis() : tStop;
    return t - tStart;
  }


  /**  Starts the timer.  */
  public void restart() {
    tStart = System.currentTimeMillis();
    tStop = -1;
  }


  /**  Stops the timer.  */
  public void stop() {
    tStop = System.currentTimeMillis();
  }


  /**
   *  Prints the current elapsed time to the standard output. String "ms." is
   *  printed after the time value.
   *
   * @param  prefix  String printed in front of the time message.
   */
  public void println(String prefix) {
    long t = getTimeMillis();
    long ms = t % 1000;
    t /= 1000;
    long s = t % 60;
    t /= 60;
    long m = t % 60;
    long h = t / 60;

    System.out.println(prefix + getTimeMillis()
        + "ms. [" + h + ":" + m + ":" + s + "." + ms + "]");
  }
}
