/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.measures;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.DatasetInt;

/**
 *  Measure the quality of the bayesian network on the dataset using log of
 *  class probabilities. <p>
 *
 *  &nbsp;&nbsp;&nbsp; <i>q</i> = LC(<i>B</i> ,<i>D</i> ) = sum<SUB><i>l</i>
 *  =1...n</SUB> log <i>p</i> (<i>c<SUB>l</SUB> </i> | <i><b>a</b> <SUB>l</SUB>
 *  </i>, <i>D</i> , <i>B</i> ) <br>
 *
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
final class QualityMeasureLogC extends QualityMeasure {
  /**  Constructor for the QualityMeasureLogC object */
  public QualityMeasureLogC() {
    super();
  }


  /**
   *  Constructor for the QualityMeasureLogC object
   *
   * @param  dataset  Description of Parameter
   */
  public QualityMeasureLogC(DatasetInt dataset) {
    super(dataset);
  }


  /**
   *  Gets the Name attribute of the QualityMeasureLogC object
   *
   * @return    The Name value
   */
  public String getName() {
    return "Log C";
  }


  /**
   *  Description of the Method
   *
   * @param  net            Description of Parameter
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public final double evaluate(BayesNet net)
      throws Exception {
    double q = 0;

    int nbVars = dataset.names.length;
    int nbAttrib = nbVars - 1;
    jbnc.graphs.BNCInference inference = new jbnc.graphs.BNCInference(net);

    // Test each case
    int nbCases = dataset.cases.size();
    for (int caseNb = 0; caseNb < nbCases; ++caseNb) {
      // Set node values
      int[] thisCase = (int[]) dataset.cases.get(caseNb);

      // Skip empty cases.
      if (thisCase == null) {
        continue;
      }

      // Do inference
      double[] classProb = inference.getCondClassProb(thisCase);

      // Find dataset index for current cases class
      int trueClassIndex = thisCase[nbAttrib];

      double lq = Math.log(classProb[trueClassIndex]);
      q += lq;
    }

    return q;
  }

}
