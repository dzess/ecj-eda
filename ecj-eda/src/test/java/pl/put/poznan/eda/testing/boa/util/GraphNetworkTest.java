package pl.put.poznan.eda.testing.boa.util;

import java.util.List;
import java.util.Vector;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.put.poznan.eda.boa.util.graph.GraphNetwork;

public class GraphNetworkTest {

    private Vector<String> emptyVector;

    @Before
    public void set_up() {
        emptyVector = new Vector<String>();
    }

    @Test
    public void adding_probablity_function_does_proper_struture_modification_1() {
        // version of graphs structure with:
        // P(A)
        BayesNet network = new BayesNet("name", 1, 1);
        DiscreteVariable[] discreateVariables = new DiscreteVariable[1];
        discreateVariables[0] = new ProbabilityVariable(network, "A", emptyVector);

        double[] values = new double[2];
        values[0] = 0.5;
        values[1] = 0.5;

        ProbabilityFunction function = new ProbabilityFunction(network, discreateVariables, values, emptyVector);
        network.get_probability_functions()[0] = function;
        network.get_probability_variables()[0] = (ProbabilityVariable) discreateVariables[0];

        // act
        GraphNetwork graph = new GraphNetwork(network);
        graph.addFunction(function);

        // assert
        List<Integer>[] succ = graph.getSuccessorArrayDeepCopy();
        List<Integer> aFollowers = succ[0];
        Assert.assertEquals(0, aFollowers.size());

        List<Integer>[] pred = graph.getPredecessorArrayDeepCopy();
        List<Integer> aPred = pred[0];
        Assert.assertEquals(0, aPred.size());
    }

    @Test
    public void adding_probablity_function_does_proper_struture_modification_2() {
        // version of graph structure with P(B|A), and P(A|B)
        BayesNet network = new BayesNet("sample network", 2, 2);

        Vector<String> emptyVector = new Vector<String>();
        ProbabilityVariable[] variables = network.get_probability_variables();
        ProbabilityFunction[] functions = network.get_probability_functions();

        variables[0] = new ProbabilityVariable(network, "A", emptyVector);
        variables[1] = new ProbabilityVariable(network, "B", emptyVector);

        // P(A) = 0.8
        double[] probOfA = new double[2];
        probOfA[0] = 0.8;
        probOfA[1] = 0.2;
        DiscreteVariable[] discreateVarsA = new DiscreteVariable[1];
        discreateVarsA[0] = variables[0];
        functions[0] = new ProbabilityFunction(network, discreateVarsA, probOfA, emptyVector);

        // the conditional function on the P(B) -> P(B|A)
        double[] probOfB = new double[4];
        probOfB[0] = 0.6;
        probOfB[1] = 0.8;
        probOfB[2] = 0.4;
        probOfB[3] = 0.2;

        DiscreteVariable[] discreateVarsB = new DiscreteVariable[2];
        // NOTE: this bug is outcome of fact, that the variable[1] is explained by variable[0]
        discreateVarsB[0] = variables[1];
        discreateVarsB[1] = variables[0];
        functions[1] = new ProbabilityFunction(network, discreateVarsB, probOfB, emptyVector);

        // act
        GraphNetwork graph = new GraphNetwork(network);
        graph.addFunction(functions[0]);
        graph.addFunction(functions[1]);

        // assert
        List<Integer>[] succ = graph.getSuccessorArrayDeepCopy();
        List<Integer>[] pred = graph.getPredecessorArrayDeepCopy();

        // followers of A
        List<Integer> aFollowers = succ[0];
        Assert.assertEquals(1, aFollowers.size());
        Assert.assertEquals(new Integer(1), aFollowers.get(0));

        // predecessor of A
        List<Integer> aPred = pred[0];
        Assert.assertEquals(0, aPred.size());

        // predecessor of B
        List<Integer> bPred = pred[1];
        Assert.assertEquals(1, bPred.size());
        Assert.assertEquals(new Integer(0), bPred.get(0));

        // followers of B
        List<Integer> bFollowers = succ[1];
        Assert.assertEquals(0, bFollowers.size());

    }
}
