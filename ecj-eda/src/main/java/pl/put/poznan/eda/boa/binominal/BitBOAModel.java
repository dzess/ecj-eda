package pl.put.poznan.eda.boa.binominal;

import java.util.Vector;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import ec.vector.BitVectorIndividual;

/**
 * 
 * Bayesian Network model used for store the probabilities. This version of implementation supports only {@linkplain BitVectorIndividual}
 * classes.
 * 
 * <p>
 * Please, note that there is some representation incoherence: {@linkplain Boolean} values are stored as {@linkplain String} in the
 * {@linkplain BayesNet} engine. Reimplementation would require rewriting the whole <code>Java Bayes Network Toolkit</code>.
 * 
 * Assume that:
 * <ul>
 * <li>TRUE is "1" character</li>
 * <li>FALSE is "0" character</li>
 * </ul>
 * </p>
 * 
 * <p>
 * Please note that, any other value in the network will result in validation exception.
 * </p>
 * 
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class BitBOAModel extends BOAModel {

    private static final long serialVersionUID = 7274764705039683453L;

    private static final String C_NETWORK_NAME = "BIT_BOA_INITIALIZED_NETWORK";

    @Override
    protected void generateBayesNet() {
        int newtworkDsitrbutions = genomeSize;
        network = new BayesNet(C_NETWORK_NAME, genomeSize, newtworkDsitrbutions);

        try {
            // set variables
            ProbabilityVariable[] variables = network.get_probability_variables();
            ProbabilityFunction[] functions = network.get_probability_functions();
            for (int i = 0; i < genomeSize; i++) {

                // TODO: the naming schema of variables is hardcoded here should be more pluggable
                String name = "G" + i;
                Vector<String> vector = new Vector<String>();
                variables[i] = JBNCUtil.createBinaryProbabilityVariable(network, name, vector);

                Vector<String> secondVector = new Vector<String>();
                DiscreteVariable[] discreteVars = new DiscreteVariable[1];
                discreteVars[0] = variables[i];

                double[] probablitiesValues = new double[2];
                probablitiesValues[0] = 0.5;
                probablitiesValues[1] = 0.5;

                functions[i] = new ProbabilityFunction(network, discreteVars, probablitiesValues, secondVector);

            }
        } catch (Exception e) {
            throw new IllegalStateException("jBNC toolkit throws expetion during initialization of the net", e);
        }
    }

    @Override
    public void validate() {
        super.validate();

        // the probability variables must be binary
        ProbabilityVariable[] variables = network.get_probability_variables();
        for (int i = 0; i < variables.length; i++) {
            ProbabilityVariable variable = variables[i];

            // validate the number of variables
            if (variable.get_values().length != 2) {
                throw new IllegalStateException("Each probability variable in BOA must be binominal");
            }

            // we assume (for speed) that first value is always = '0'
            if (!variable.get_value(0).equals("0")) {
                throw new IllegalStateException("first value of binary probability value should be False = 0");
            }

            if (!variable.get_value(1).equals("1")) {
                throw new IllegalStateException("first value of binary probability value should be True = 1");
            }
        }

        ProbabilityFunction[] functions = network.get_probability_functions();
        for (int i = 0; i < functions.length; i++) {
            ProbabilityFunction function = functions[i];
            // for now assume that each variable is binomial
            // it's very limiting assumption, yet allows fast power computation
            int variablesCount = function.get_variables().length;
            int numberOfProbabilites = 1 << variablesCount;
            if (function.get_values().length != numberOfProbabilites) {
                throw new ArithmeticException("The number of values stored in the BOA model should be 2^variables but was: "
                        + function.get_values().length);
            }
        }
    }

    @Override
    protected boolean isIndividualValid() {
        return !individual.getClass().equals(BitVectorIndividual.class);
    }
}
