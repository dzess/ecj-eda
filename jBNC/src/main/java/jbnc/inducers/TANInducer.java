/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.inducers;

import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.operators.Augmenter;
import jbnc.operators.TreeAugmenter;
import jbnc.util.BNTools;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 *  Constructs Tree Augmented naive-Bayes classifier (TAN).<br>
 *  <br>
 *  Friedman N, Geiger D, Goldszmidt M. "Bayesian Network Classifiers". <i>
 *  Machine Learning</i> , vol.29, issue 2/3, November 1997, pp.131-163.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class TANInducer extends BayesianInducer {

  /**
   *  Used for testing of the TAN algorithm.
   *
   * @param  arg  The command line arguments
   */
  public static void main(String[] arg) {
    try {
      System.out.println("\nTesting class...\n");

      String fileStem = "..\\db\\vote";
//      String fileStem = "../databases/PhD/Apex_Test/cv10_disc/apex_F_circle_C2-0-2";
      if (arg.length > 0) {
        fileStem = arg[0];
      }

      String testFileName = fileStem + ".test";
      String trainFileName = fileStem + ".data";
      String namesFileName = fileStem + ".names";

      System.out.println("Load train dataset: " + trainFileName);
      DatasetInt trainSet = new DatasetInt();
      trainSet.openC45(namesFileName, trainFileName);
      trainSet.discardAllOfType(AttributeType.IGNORE);
      jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(trainSet);

      TANInducer tan = new TANInducer();
      tan.train(fc);
      jbnc.util.BNCTester tester = new jbnc.util.BNCTester();
      tester.test(tan.getNetwork(), trainSet);
      tester.reportShort();

      System.out.println("\nLoad test dataset: " + testFileName);
      DatasetInt testSet = new DatasetInt();
      testSet.openC45(namesFileName, testFileName);
      testSet.discardAllOfType(AttributeType.IGNORE);
      tester.test(tan.getNetwork(), testSet);
      tester.reportShort();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   *  Construct TAN network from the training data
   *
   * @param  fc             Description of Parameter
   * @param  usePriors      Description of Parameter
   * @param  alpha_ijk      Description of Parameter
   * @exception  Exception  Description of Exception
   */
  public void train(jbnc.util.FrequencyCalc fc,
                    boolean usePriors,
                    double alpha_ijk) throws Exception {
    // Clean storage
    clean();

    // Some sanity checks
//    verifyDataset(dataset);

    int nbAttrib = fc.names.length - 1;

    // Set all attributes as dependent on the class node
    ArrayList gamma = new ArrayList();
    for (int i = 0; i < nbAttrib; ++i) {
      gamma.add(new Integer(i));
    }

    // Create network structure
    TreeAugmenter ta = new TreeAugmenter();
    Augmenter.Result r = ta.train(gamma, null, fc, null, usePriors, alpha_ijk);

    net = r.bayesNet;

    // Learn parameters
    if (usePriors) {
      BNTools.learnParameters(net, fc, usePriors, alpha_ijk);
    } else {
      BNTools.learnParameters(net, fc, true, DEFAULT_APLHA_IJK);
    }

    if (debugMode) {
      FileOutputStream out = new FileOutputStream("TAN_dump.bif");
      PrintStream pOut = new PrintStream(out);
      net.save_bif(pOut);
    }
  }
}
