/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.measures;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.DatasetInt;

/**
 *  LCV_LOO - Local Cross Validation, Leave-One-Out. Measure the quality of the
 *  Bayesian network on the dataset using leave-one-out cross validation (on
 *  class variable).
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class QualityMeasureLCV_LOO extends QualityMeasure {
  /**  Constructor for the QualityMeasureLCV_LOO object */
  public QualityMeasureLCV_LOO() {
    super();
  }


  /**
   *  Constructor for the QualityMeasureLCV_LOO object
   *
   * @param  dataset  Description of Parameter
   */
  public QualityMeasureLCV_LOO(DatasetInt dataset) {
    super(dataset);
  }


  /**
   *  Gets the Name attribute of the QualityMeasureLCV_LOO object
   *
   * @return    The Name value
   */
  public String getName() {
    return "Leave-one-out cross-valiadation";
  }


  /**
   *  Description of the Method
   *
   * @param  net            Description of Parameter
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public final double evaluate(BayesNet net)
      throws Exception {
    double q = 0;
    jbnc.util.FrequencyTable freqTable = new jbnc.util.FrequencyTable(net);
    freqTable.setCases(dataset.cases);
    jbnc.graphs.BNCInference inference = new jbnc.graphs.BNCInference(net);

//    QualityMeasureLogC lc = new QualityMeasureLogC();
    int nbCases = dataset.cases.size();
    int nbAttrib = dataset.names.length - 1;

//    DatasetInt testSet = (DatasetInt)dataset.clone();
//    testSet.cases = new Vector();
//    testSet.cases.setSize(1);


    // Disable cases one at a time and learn net parameters.
    // All the try-catch-finally stuff is to make sure that
    // dataset is put back to its initial state even when an exception
    // is trown by parameter learning.
    for (int i = 0; i < nbCases; ++i) {
      // Learn network parameters
      int[] thisCase = (int[]) dataset.cases.get(i);
      freqTable.removeCase(thisCase);
      freqTable.learnNetParam(net, usePriors, alphaK);
      freqTable.addCase(thisCase);

      // Do inference
      double[] classProb = inference.getCondClassProb(thisCase);
      // Find dataset index for current cases class
      int trueClassIndex = thisCase[nbAttrib];

      double lq = Math.log(classProb[trueClassIndex]);
      q += lq;

      /*
       *  dataset.cases.set(i, null);
       *  try
       *  {
       *  Util.BNTools.learnParameters(net, dataset, usePriors, alphaK);
       *  }
       *  catch(Exception e)
       *  {
       *  throw e;
       *  }
       *  finally
       *  {
       *  dataset.cases.set(i, thisCase);
       *  }
       *  / Measure quality for this case
       *  testSet.cases.set(0, thisCase);
       *  q += lc.evaluate(net, testSet);
       */
    }

    return q;
  }
}
