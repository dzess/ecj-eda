package pl.put.poznan.eda.boa.binominal;

import ec.Individual;
import ec.vector.BitVectorIndividual;
import pl.put.poznan.eda.IMatchable;
import pl.put.poznan.eda.Model;

/**
 * Base class for managing the {@linkplain BitBOAModel} representations. Follows the {@linkplain IMatchable} interface
 * 
 * For now supports only the {@linkplain BitVectorIndividual}
 * 
 * <p>
 * Used in classes:
 * <ul>
 * <li>{@linkplain BitBOASampler}</li>
 * <li>{@linkplain BitBOAUpdater}</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public abstract class BitBOABase {

    public boolean matchModelType(Class<? extends Model> model) {
        if (model.equals(BitBOAModel.class)) {
            return true;
        }

        return false;
    }

    public boolean matchIndividualType(Class<? extends Individual> individual) {
        if (individual.equals(BitVectorIndividual.class)) {
            return true;
        }
        return false;
    }

    public void markIndividualType(Class<? extends Individual> individual) {
        // this method is not needed because only the BitVectorIndividual is going to be used
    }
}