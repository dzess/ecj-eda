/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.inducers;


/**
 *  Abstract Bayesian network clasifier inducer that is able to discard
 *  attribute nodes that are not dependent on the class node.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public abstract class DiscardingBayesianInducer extends BayesianInducer {

  /**  LOCAL  */

  /**
   *  Whether nodes that are not dependent on the class node should be
   *  dsiscarded from augmenting tree creation.
   */
  protected boolean discardNonDependent = false;


  /**
   *  Sets discardNonDependent flag.
   *
   * @param  discard  The new DiscardNonDependent value
   */
  public void setDiscardNonDependent(boolean discard) {
    discardNonDependent = discard;
  }


  /**
   *  Gets discardNonDependent flag.
   *
   * @return    The DiscardNonDependent value
   */
  public boolean getDiscardNonDependent() {
    return discardNonDependent;
  }
}
