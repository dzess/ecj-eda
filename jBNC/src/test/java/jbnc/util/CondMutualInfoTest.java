package jbnc.util;

import jbnc.dataset.AttributeSpecs;
import jbnc.dataset.DatasetInt;
import junit.framework.TestCase;

/**
 * Test CondMutualInfo class using two classic exmples from literature.
 */
public class CondMutualInfoTest extends TestCase {


    public CondMutualInfoTest(String test) {
        super(test);
    }

    /**
     * Example 2.2.1 from Cover&Thomas "Elements of Information Theory", 1991.
     *
     * @throws Exception
     */
    public void testCoverAndThomasExample() throws Exception {
        int[][] data = {
            {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
            // (1,1) = 1/8
            {0, 1, 0}, {0, 1, 0},
            // (1,2) = 1/16
            {0, 2, 0}, {0, 2, 0},
            // (1,3) = 1/16
            {0, 3, 0}, {0, 3, 0}, {0, 3, 0}, {0, 3, 0},
            {0, 3, 0}, {0, 3, 0}, {0, 3, 0}, {0, 3, 0},
            // (1,4) = 1/4
            {1, 0, 0}, {1, 0, 0},
            // (2,1) = 1/16
            {1, 1, 0}, {1, 1, 0}, {1, 1, 0}, {1, 1, 0},
            // (2,2) = 1/8
            {1, 2, 0}, {1, 2, 0},
            // (2,3) = 1/16
            // (2,4) = 0
            {2, 0, 0},
            // (3,1) = 1/32
            {2, 1, 0},
            // (3,2) = 1/32
            {2, 2, 0}, {2, 2, 0},
            // (3,3) = 1/16
            // (3,4) = 0
            {3, 0, 0},
            // (4,1) = 1/32
            {3, 1, 0},
            // (4,2) = 1/32
            {3, 2, 0}, {3, 2, 0},
            // (4,3) = 1/16
            // (4,4) = 0
        };

        // Create attrib decription
        String[] classStates = {"0"};
        AttributeSpecs[] names = new AttributeSpecs[3];
        names[0] = new AttributeSpecs("a0", 4);
        names[1] = new AttributeSpecs("a1", 4);
        names[2] = new AttributeSpecs("c", 1);

        DatasetInt dataset = new DatasetInt(data, names);

        FrequencyCalc fc = new FrequencyCalc(dataset);

        boolean[] cond = new boolean[3];
        cond[0] = false;
        cond[1] = false;
        CondMutualInfo jp = new CondMutualInfo(fc);
        double[][] cmi = jp.get(cond, false, 0);

        for (int i = 0; i < cmi.length; ++i) {
            for (int j = i + 1; j < cmi[i].length; ++j) {
                assertEquals("Cover&Thomas mutual information example",
                        0.375, cmi[i][j], 1e-10);
            }
        }
    }

    /**
     * Example from Chow&Liu "Approximating discrete probability distributions with dependence trees" IEEE Trans.
     * Inform. Theory vol.IT14, no.3, May 1968.
     *
     * @throws Exception
     */
    public void testChowAndLiuExample() throws Exception {

        int[][] data = {
            {0, 0, 0, 0, 0}, {0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0}, {0, 0, 0, 1, 0},
            {0, 0, 1, 0, 0},
            {0, 0, 1, 1, 0},
            //{0,1,0,0,0},
            //{0,1,0,1,0},
            {0, 1, 1, 0, 0}, {0, 1, 1, 0, 0},
            {0, 1, 1, 1, 0},
            {1, 0, 0, 0, 0},
            {1, 0, 0, 1, 0}, {1, 0, 0, 1, 0},
            //{1,0,1,0,0},
            //{1,0,1,1,0},
            {1, 1, 0, 0, 0},
            {1, 1, 0, 1, 0},
            {1, 1, 1, 0, 0}, {1, 1, 1, 0, 0}, {1, 1, 1, 0, 0},
            {1, 1, 1, 1, 0}, {1, 1, 1, 1, 0}, {1, 1, 1, 1, 0},
        };

        // Create attrib decription
        AttributeSpecs[] names = new AttributeSpecs[5];
        names[0] = new AttributeSpecs("a0", 2);
        names[1] = new AttributeSpecs("a1", 2);
        names[2] = new AttributeSpecs("a2", 2);
        names[3] = new AttributeSpecs("a3", 2);
        names[4] = new AttributeSpecs("c", 1);

        DatasetInt dataset = new DatasetInt(data, names);

        FrequencyCalc fc = new FrequencyCalc(dataset);
        CondMutualInfo jp = new CondMutualInfo(fc);
        double[][] cmi = jp.get(new boolean[5], false, 0);

        double c = Math.log(2) / Math.log(Math.E);
        double[][] shoudBe = {
            {0, 0.079, 0.00005, 0.005},
            {0, 0.000, 0.18900, 0.005},
            {0, 0.000, 0.00000, 0.005}};
//        System.out.println("\nMutual information: should be, got: ");
        for (int i = 0; i < cmi.length; ++i) {
            for (int j = i + 1; j < cmi[i].length; ++j) {
                assertEquals("Chow&Liu (" + i + "," + j + ")", shoudBe[i][j], c * cmi[i][j], 0.0005);
//                System.out.println("(" + i + "," + j + ") = "+shoudBe[i][j]+ "  :  " + (c * cmi[i][j]));
            }
        }
    }

    /**
     * The fixture set up called before every test method
     */
    protected void setUp
            () throws Exception {
    }

    /**
     * The fixture clean up called after every test method
     */
    protected void tearDown
            () throws Exception {
    }
}