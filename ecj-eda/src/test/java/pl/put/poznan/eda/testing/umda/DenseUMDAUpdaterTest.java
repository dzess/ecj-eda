package pl.put.poznan.eda.testing.umda;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.ModelSubpopulation;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.umda.DenseUMDAUpdater;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;
import ec.vector.VectorIndividual;

/**
 * Very important tests used to check that the model building is done properly.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class DenseUMDAUpdaterTest extends DenseUMDATestingBase {

    private DenseUMDAUpdater updater;
    private DenseUMDAModel model;

    @Before
    public void set_up() {
        this.setUpBase();
        this.setUpEvolutionStateBasic();

        updater = new DenseUMDAUpdater();

        subpopulation = Mockito.mock(ModelSubpopulation.class);

    }

    private void assertFrequencies(int genomeSize, int individualsSize, int cardinalityExpected) {
        // read the frequency counters to match the things
        int[][] frequency = updater.getFrequencyCount();
        Assert.assertEquals(genomeSize, frequency.length);
        for (int i = 0; i < frequency.length; i++) {
            Assert.assertEquals(individualsSize, frequency[i][cardinalityExpected]);
        }
    }

    @Test
    public void retriving_model_from_boolean() {

        // some important values
        int genomeSize = 5;
        int genomeCardinality = 2;

        // populate the subpopulation with the individuals
        int individualsSize = 3;
        subpopulation.individuals = new VectorIndividual[individualsSize];
        for (int i = 0; i < individualsSize; i++) {
            BitVectorIndividual ind = new BitVectorIndividual();
            ind.genome = new boolean[genomeSize];
            for (int j = 0; j < genomeSize; j++) {
                ind.genome[j] = true;
            }
            subpopulation.individuals[i] = ind;
        }

        // inject proper model into subpopulation
        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);
        Mockito.when(subpopulation.getModel()).thenReturn(model);
        updater.markIndividualType(BitVectorIndividual.class);
        updater.retrieveModel(subpopulation, state);

        assertFrequencies(genomeSize, individualsSize, 0);
    }

    @Test
    public void retriving_model_from_byte() {
        // some important values
        int genomeSize = 5;
        int genomeCardinality = 256;

        // populate the subpopulation with the individuals
        int individualsSize = 3;
        subpopulation.individuals = new VectorIndividual[individualsSize];
        for (int i = 0; i < individualsSize; i++) {
            ByteVectorIndividual ind = new ByteVectorIndividual();
            ind.genome = new byte[genomeSize];
            for (int j = 0; j < genomeSize; j++) {
                ind.genome[j] = 0;
                // this should be in the middle of the representation
                // AKA byte.size / 2 = 128
            }
            subpopulation.individuals[i] = ind;
        }

        // inject proper model into subpopulation
        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);
        Mockito.when(subpopulation.getModel()).thenReturn(model);
        updater.markIndividualType(ByteVectorIndividual.class);
        updater.retrieveModel(subpopulation, state);

        int cardinalityCheck = 128;
        assertFrequencies(genomeSize, individualsSize, cardinalityCheck);
    }

    @Ignore("with introduciton of max and min gene contraint this cannot be used anymore")
    public void retriving_model_from_short() {
        // some important values
        int genomeSize = 7;
        int genomeCardinality = 1 << Short.SIZE;

        // populate the subpopulation with the individuals
        int individualsSize = 3;
        subpopulation.individuals = new VectorIndividual[individualsSize];
        for (int i = 0; i < individualsSize; i++) {
            ShortVectorIndividual ind = new ShortVectorIndividual();
            ind.genome = new short[genomeSize];
            for (int j = 0; j < genomeSize; j++) {
                ind.genome[j] = 0;
                // this should be in the middle of the representation
                // AKA byte.size / 2 = 32768
            }
            subpopulation.individuals[i] = ind;
        }

        // inject proper model into subpopulation
        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);
        Mockito.when(subpopulation.getModel()).thenReturn(model);
        updater.markIndividualType(ShortVectorIndividual.class);
        updater.retrieveModel(subpopulation, state);

        int cardinalityCheck = 32768;
        assertFrequencies(genomeSize, individualsSize, cardinalityCheck);
    }

    private void prepareAndExecuteUpdating(int numberOfIndividuals, int genomeCardinality, int genomeSize) {

        // create the individuals
        subpopulation.individuals = new VectorIndividual[numberOfIndividuals];

        // create the frequency count already set some important values
        int[][] frequencies = new int[genomeSize][genomeCardinality];
        for (int i = 0; i < genomeSize; i++) {
            // all the individuals created the frequency map with boolean true
            // value being set
            frequencies[i][0] = numberOfIndividuals;
            frequencies[i][1] = 0;
        }

        // prepare existing model
        model = this.prepareModel(genomeCardinality, genomeSize);
        for (int i = 0; i < genomeSize; i++) {
            for (int j = 0; j < genomeCardinality; j++) {
                // the model has the probability
                model.genomeProbabilites[i][j] = 0.5;
            }
        }

        Mockito.when(subpopulation.getModel()).thenReturn(model);

        // application of the frequency map to model
        updater.setFrequencyCount(frequencies);
        updater.applyModel(subpopulation, state);

        // model should validate after the updating
        model.validate();

    }

    @Test
    public void applying_model() {
        int genomeSize = 5;
        int genomeCardinality = 2;
        int numberOfIndividuals = 6;
        this.prepareAndExecuteUpdating(numberOfIndividuals, genomeCardinality, genomeSize);

        // returned model has its 1.0 + 0.5 with weight 0.5 results in 0.75
        for (int i = 0; i < genomeSize; i++) {
            Assert.assertEquals(0.75, model.genomeProbabilites[i][0]);
            Assert.assertEquals(0.25, model.genomeProbabilites[i][1]);
        }
    }

    @Test
    public void applying_model_with_modifed_weight() {
        Parameter parameter = base.push(DenseUMDAUpdater.P_WEIGHT);
        this.generateGetDoubleForParameterExtended(parameter, 0.1);
        updater.setup(state, base);

        // go with normal updating
        int genomeSize = 5;
        int genomeCardinality = 2;
        int numberOfIndividuals = 6;
        this.prepareAndExecuteUpdating(numberOfIndividuals, genomeCardinality, genomeSize);

        // but assertions are needed to be done the other way
        for (int i = 0; i < genomeSize; i++) {
            Assert.assertEquals(0.55, model.genomeProbabilites[i][0]);
            Assert.assertEquals(0.45, model.genomeProbabilites[i][1]);
        }

    }

    @Test(expected = IllegalStateException.class)
    public void updater_validates_the_weight_parameter_1() {

        Parameter parameter = base.push(DenseUMDAUpdater.P_WEIGHT);
        this.generateGetDoubleForParameterExtended(parameter, -0.1);
        updater.setup(state, base);
    }

    @Test(expected = IllegalStateException.class)
    public void updater_validates_the_weight_parameter_2() {

        Parameter parameter = base.push(DenseUMDAUpdater.P_WEIGHT);
        this.generateGetDoubleForParameterExtended(parameter, 1.2);
        updater.setup(state, base);
    }

    @Test
    public void updating_works_with_bit_byte_short_vector() {
        Assert.assertTrue(updater.matchIndividualType(BitVectorIndividual.class));
        Assert.assertTrue(updater.matchIndividualType(ShortVectorIndividual.class));
        Assert.assertTrue(updater.matchIndividualType(ByteVectorIndividual.class));

        // some of classes which are not supported (and somebody might think they are)
        Assert.assertFalse(updater.matchIndividualType(IntegerVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(FloatVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(DoubleVectorIndividual.class));

    }

    @Test
    public void updating_works_only_with_umda_model() {
        Assert.assertTrue(updater.matchModelType(DenseUMDAModel.class));
        // other models are not supported
        Assert.assertFalse(updater.matchModelType(BitBOAModel.class));
    }
}
