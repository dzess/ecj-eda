/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.util.StringTokenizer;
import java.util.Vector;

class ParseLine {
  /**  Description of the Field */
  public AttributeSpecs attribute;

  private String line;
  private StringTokenizer st;


  ParseLine(String line) {
    this.line = new String(line);

    // Strip comments
    int i = line.indexOf('|');
    if (i < 0) {
      st = new StringTokenizer(line);
    } else {
      st = new StringTokenizer(line.substring(0, i));
    }
  }


  /**
   *  Description of the Method
   *
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public Vector read() throws Exception {
    return read(',');
  }


  /**
   *  Description of the Method
   *
   * @param  separator      Description of Parameter
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public Vector read(char separator) throws Exception {
    Vector v = new Vector();
    String token;
    while ((token = nextToken(separator)) != null) {
      v.add(token);
    }

    return (v.size() > 1) ? v : null;
  }


  protected String nextToken(char separator_) {
    if (!st.hasMoreTokens()) {
      return null;
    }

    String separator = new Character(separator_).toString();
    String token = st.nextToken(separator).trim();

    // Remove trailing '.'
    if (token.endsWith(".")) {
      return token.substring(0, token.length() - 1);
    }

    return token;
  }
}
