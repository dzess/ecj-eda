package pl.put.poznan.eda;

import ec.Singleton;
import ec.Subpopulation;

/**
 * The probability information about the current population.
 * 
 * <p>
 * The solely purpose of this class existing is only to separate the concerns of representation of probability from
 * {@linkplain Subpopulation} and {@linkplain ModelSubpopulation}.
 * <p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public abstract class Model implements Singleton {

    // TODO: make this inherit from the {@linkplain Prototype} maybe for now singleton makes the day

    private static final long serialVersionUID = -5658146145637446415L;

    /**
     * Validates if the created is proper in the terms of the probability theorem.
     * 
     * <p>
     * Theorem:
     * <ul>
     * <li>all probabilities sum up to 1</li>
     * <li>impossible is 0, certain is 1.0</li>
     * <li>additive sum of probabilities</li>
     * </ul>
     * </p>
     * 
     * <p>
     * The exact level of this checks is implied by implementation of {@linkplain Model} class.
     * </p>
     */
    public abstract void validate();

}
