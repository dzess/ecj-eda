package pl.put.poznan.eda.umda;

import pl.put.poznan.eda.IModelUpdater;
import pl.put.poznan.eda.ModelSubpopulation;
import ec.EvolutionState;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.ShortVectorIndividual;

/**
 * 
 * The basic implementation of the {@linkplain IModelUpdater} interface. Assigns the probability of each gene as a frequency count. The
 * frequencies are normalized per each generation.
 * 
 * <p>
 * Frequency count is array of genome of genome cardinality.
 * </p>
 * 
 * <p>
 * Works only with the models based on representation of:
 * <ul>
 * <li>{@linkplain BitVectorIndividual}</li>
 * <li>{@linkplain ByteVectorIndividual}</li>
 * <li>{@linkplain ShortVectorIndividual}</li>
 * </ul>
 * </p>
 * 
 * TODO: UMDA uses the sample aggregation (which might be pluggable ?)
 * 
 * <p>
 * Does have some other parameters
 * <ul>
 * <li>weight of the linear update: meaning <code>parameter * old + (1 -parameter) * new</code></li>
 * </ul>
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public class DenseUMDAUpdater extends DenseUMDA implements IModelUpdater {

    private static final long serialVersionUID = 8540963750253180488L;

    private int[][] frequenyCount;

    private int genomeSize;
    private int genomeCardinality;

    private Double weight = 0.5;

    public static final String P_WEIGHT = "weight";

    /**
     * Gets the <b>copy</b> of the frequency count field. The returned values are up to the last generation. The frequency array is cleared
     * every time.
     * 
     * @return the copy of frequency count array.
     */
    public int[][] getFrequencyCount() {
        return frequenyCount.clone();
    }

    /**
     * Sets the frequency counting map. This method should not be used in a basic way.
     * 
     * @param frequencyCountMap
     *            the new array of genome and their cardinalities.
     */
    public void setFrequencyCount(int[][] frequencyCountMap) {
        this.frequenyCount = frequencyCountMap;
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {

        // search for parameter, basic implementation of getDouble methods
        // returns minus value - 1 thus making 0.5 default value
        Parameter weightParam = base.push(P_WEIGHT);
        weight = state.parameters.getDouble(weightParam, null, 0.0);
        if (weight == -1.0) {
            weight = 0.5;
        }

        if (weight > 1.0 || weight < 0.0) {
            state.output.fatal("the weight parameter must be between 0.0 and 1.0");
        }

        // safe check
        state.output.exitIfErrors();
    }

    @Override
    public void retrieveModel(ModelSubpopulation modelSubpop, EvolutionState state) {

        DenseUMDAModel model = (DenseUMDAModel) modelSubpop.getModel();
        genomeSize = model.getGenomeSize();
        genomeCardinality = model.getGenomeCardinality();

        // methods are inlined because of the performance usage
        frequenyCount = new int[genomeSize][genomeCardinality];
        if (isBitVector) {
            for (int indIndex = 0; indIndex < modelSubpop.individuals.length; indIndex++) {
                // take the bits of the genome of VectorIndividual and update the model
                BitVectorIndividual ind = null;
                ind = (BitVectorIndividual) modelSubpop.individuals[indIndex];
                boolean[] genome = ind.genome;
                for (int genIndex = 0; genIndex < genome.length; genIndex++) {
                    if (genome[genIndex] == true) {
                        frequenyCount[genIndex][0] = frequenyCount[genIndex][0] + 1;
                    } else {
                        frequenyCount[genIndex][1] = frequenyCount[genIndex][1] + 1;
                    }
                }
            }
        } else if (isByteVector) {
            // byte version of things
            for (int indIndex = 0; indIndex < modelSubpop.individuals.length; indIndex++) {
                // take the bits of the genome of VectorIndividual and update the model
                ByteVectorIndividual ind = null;
                ind = (ByteVectorIndividual) modelSubpop.individuals[indIndex];
                byte[] genome = ind.genome;
                for (int genIndex = 0; genIndex < genome.length; genIndex++) {
                    // cardinality - value mapping
                    int cardinalityIndex = genome[genIndex] - Byte.MIN_VALUE;
                    frequenyCount[genIndex][cardinalityIndex] = frequenyCount[genIndex][cardinalityIndex] + 1;
                }
            }

        } else {
            // it must be the short vector individual
            for (int indIndex = 0; indIndex < modelSubpop.individuals.length; indIndex++) {
                // take the bits of the genome of VectorIndividual and update the model
                ShortVectorIndividual ind = null;
                ind = (ShortVectorIndividual) modelSubpop.individuals[indIndex];
                short[] genome = ind.genome;
                for (int genIndex = 0; genIndex < genome.length; genIndex++) {
                    // cardinality - value mapping
                    int cardinalityIndex = genome[genIndex] - model.getMinGene();
                    frequenyCount[genIndex][cardinalityIndex] = frequenyCount[genIndex][cardinalityIndex] + 1;
                }
            }
        }
    }

    @Override
    public void applyModel(ModelSubpopulation modelSubpopulation, EvolutionState state) {

        DenseUMDAModel model = (DenseUMDAModel) modelSubpopulation.getModel();
        genomeSize = model.getGenomeSize();
        genomeCardinality = model.getGenomeCardinality();

        int numberOfIndividuals = modelSubpopulation.individuals.length;
        for (int i = 0; i < genomeSize; i++) {
            for (int j = 0; j < genomeCardinality; j++) {
                double iterProb = frequenyCount[i][j] / (double) numberOfIndividuals;
                double iterProbWeighted = weight * iterProb;
                // arithmetic average over the probabilities
                model.genomeProbabilites[i][j] = ((1.0 - weight) * model.genomeProbabilites[i][j]) + iterProbWeighted;
            }

        }
    }
}
