/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.measures;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.DatasetInt;

/**
 *  Implements an abstract quality measure used by search algorithms to rank
 *  Bayesian networks.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public abstract class QualityMeasure {

  protected DatasetInt dataset = null;

  /**  Flag indicating use of priors. */
  protected boolean usePriors = false;

  /**  Value of the prior. */
  protected double alphaK = 0;


  /**  Default constructor. */
  public QualityMeasure() {
    setDataset(null);
  }


  /**
   *  Create a quality measure based on a dataset.
   *
   * @param  dataset  Description of Parameter
   */
  public QualityMeasure(DatasetInt dataset) {
    setDataset(dataset);
  }


  /**
   *  Sets dataset used for grading Bayesian networks.
   *
   * @param  dataset  The new Dataset value
   */
  public void setDataset(DatasetInt dataset) {
    this.dataset = dataset;
  }


  /**
   *  Sets usePriors flag.
   *
   * @param  usePriors  The new UsePriors value
   */
  public void setUsePriors(boolean usePriors) {
    this.usePriors = usePriors;
  }


  /**
   *  Sets value of the prior.
   *
   * @param  alphaK  The new AlphaK value
   */
  public void setAlphaK(double alphaK) {
    this.alphaK = alphaK;
  }


  /**
   *  Gets name of the quality measure.
   *
   * @return    The Name value
   */
  public abstract String getName();


  /**
   *  Gets usePriors flag.
   *
   * @return    The UsePriors value
   */
  public boolean getUsePriors() {
    return this.usePriors;
  }


  /**
   *  Gets value of the prior.
   *
   * @return    The AlphaK value
   */
  public double getAlphaK() {
    return this.alphaK;
  }


  /**
   *  Calculate the quality measure for the given network and data set.
   *
   * @param  net            Network to be evaluated.
   * @return                value of the quality measure.
   * @exception  Exception  Description of Exception
   */
  public abstract double evaluate(BayesNet net)
      throws Exception;
}
