package pl.put.poznan.eda.statistics;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.ModelSubpopulation;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.multinominal.ShortBOAModel;
import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import ec.EvolutionState;
import ec.Statistics;
import ec.Subpopulation;

/**
 * Gathers information about the bayesian network during runs.
 * 
 * The data are not dumped into file but are stored in already structured way. The dumping can be done with use of inherited class or some
 * other mechanism.
 * 
 * Note that information about model resides in {@linkplain ModelSubpopulation}. This kind of statistics will work then only with the
 * 
 * @author Piotr Jessa
 * @since 0.0.3
 * 
 */
public class BayesianNetworkStatistics extends Statistics {

    private static final long serialVersionUID = 1L;

    private BayesNet network;
    private GraphNetwork graphNetwork;

    private int statisticLog = 0;

    /**
     * Returns the last network created by the BOA modeling.
     * 
     * @return an instance of {@linkplain BayesNet} from the last generation.
     */
    public BayesNet getNetwork() {
        return network;
    }

    /**
     * Return the last graph representation of the same last network from BOA modeling.
     * 
     * @return the instance of the {@linkplain GraphNetwork} build from the {@linkplain BayesNet} instance.
     */
    public GraphNetwork getGraphNetwork() {
        return graphNetwork;
    }

    @Override
    public void postEvaluationStatistics(EvolutionState state) {
        super.postEvaluationStatistics(state);

        // do some reading of the
        Subpopulation subpop = state.population.subpops[0];
        ModelSubpopulation modelSubpop = (ModelSubpopulation) subpop;

        Model model = modelSubpop.getModel();
        if (model instanceof ShortBOAModel) {
            state.output.println("ShortBOAModel Bayesian NetworkStatistics", statisticLog);
            ShortBOAModel boaModel = (ShortBOAModel) model;
            network = boaModel.getNetwork();

            // dump network into traversable structure
            ProbabilityFunction[] functions = network.get_probability_functions();
            graphNetwork = new GraphNetwork(network);
            for (int i = 0; i < functions.length; i++) {
                graphNetwork.addFunction(functions[i]);
            }

        } else if (model instanceof BitBOAModel) {
            state.output.println("BitBOAModel Bayesian NetworkStatistics", statisticLog);
            // TODO: write the same type of statistics to the BitBOAModel version of code
        } else {
            throw new IllegalStateException("Only BOAModel classes are supported");
        }
    }

}
