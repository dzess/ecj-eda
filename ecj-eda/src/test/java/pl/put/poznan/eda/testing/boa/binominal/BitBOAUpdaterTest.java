package pl.put.poznan.eda.testing.boa.binominal;

import jbnc.inducers.BayesianInducer;
import jbnc.inducers.NaiveBayesInducer;
import jbnc.measures.QualityMeasure;
import jbnc.measures.QualityMeasureHGC;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.binominal.BitBOAUpdater;
import pl.put.poznan.eda.testing.ECJTestingBase;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;

public class BitBOAUpdaterTest extends ECJTestingBase {

    private BitBOAUpdater updater;

    @Before
    public void set_up() {
        this.setUpBase();
        this.setUpEvolutionStateBasic();

        updater = new BitBOAUpdater();

        // set the required parameters here
        Parameter parameter = BitBOAUpdater.U_GENOME_SIZE;
        this.generateGetIntForParameter(parameter, 5);

        // mark the other required parameter as empty
        Parameter measureParameter = base.push(BitBOAUpdater.P_QUALITY_ALGORITHM);
        this.generateGetInstanceForParameterEmpty(measureParameter);

        Parameter inducerParameter = base.push(BitBOAUpdater.P_SEARCH_ALGORITHM);
        this.generateGetInstanceForParameterEmpty(inducerParameter);
    }

    @Test
    public void updater_with_no_setup_fallsback_to_naive_bayes_and_hgc_quality_measure() {

        // no params set, and does not throws exceptions
        updater.setup(state, base);

        QualityMeasure measure = updater.getMeasure();
        BayesianInducer inducer = updater.getInducer();

        Assert.assertEquals(QualityMeasureHGC.class, measure.getClass());
        Assert.assertEquals(NaiveBayesInducer.class, inducer.getClass());
    }

    @Test
    public void updater_can_read_quality_measure_and_will_use_it() {
        Parameter measureParameter = base.push(BitBOAUpdater.P_QUALITY_ALGORITHM);
        QualityMeasure qualityMeasure = Mockito.mock(QualityMeasure.class);
        this.generateGetInstanceForParameters(measureParameter, qualityMeasure);
        updater.setup(state, base);

        Assert.assertSame(qualityMeasure, updater.getMeasure());
    }

    @Test
    public void updater_can_read_inducer_and_will_use_it() {
        Parameter inducerParameter = base.push(BitBOAUpdater.P_SEARCH_ALGORITHM);
        BayesianInducer inducer = Mockito.mock(BayesianInducer.class);
        this.generateGetInstanceForParameters(inducerParameter, inducer);
        updater.setup(state, base);

        Assert.assertSame(inducer, updater.getInducer());
    }

    @Test
    public void updating_works_with_bit_vector() {
        Assert.assertTrue(updater.matchIndividualType(BitVectorIndividual.class));

        // other types are not supported officially yet
        Assert.assertFalse(updater.matchIndividualType(ShortVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(ByteVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(IntegerVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(FloatVectorIndividual.class));
        Assert.assertFalse(updater.matchIndividualType(DoubleVectorIndividual.class));

    }

    @Test
    public void updating_works_only_with_boa_model() {
        Assert.assertTrue(updater.matchModelType(BitBOAModel.class));
        // other models are not supported
        Assert.assertFalse(updater.matchModelType(DenseUMDAModel.class));
    }
}
