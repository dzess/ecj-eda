package pl.put.poznan.eda.util;

import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.IModelUpdater;
import ec.EvolutionState;

/**
 * Samples the distribution using passed probabilities.
 * 
 * <p>
 * Created to ensure testing abilities for classes which heavily relay on randomness such as implementations of {@linkplain IModelSampler}
 * or {@linkplain IModelUpdater}.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public interface IDistributionSampler {

    /**
     * Picks the index of the probabilities from the distribution.
     * 
     * @param probablities
     *            the not summed probabilities which should sum up to 1.0
     * @param thread
     *            identifier of the thread on which execution of distribution sampling is done
     * @param state
     *            the current state of the evolution
     * @return the index of the element
     */
    public abstract int sampleDistribution(double[] probablities, EvolutionState state, int thread);

    /**
     * 
     * Boolean version of the same method. The two modal distribution (true / false) is being sampled here
     * 
     * @param probability
     *            probability of the true
     * @param thread
     *            Identifier of the thread on which execution of distribution sampling is done
     * @param state
     *            the current state of the evolution
     * @return value which was generated
     */
    public abstract boolean sampleDistribution(double probability, EvolutionState state, int thread);

}