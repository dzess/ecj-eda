package pl.put.poznan.eda.testing.boa;

import jb.BayesianNetworks.BayesNet;
import jbnc.Classifier;
import jbnc.dataset.AttributeSpecs;
import jbnc.inducers.BayesianInducer;
import jbnc.inducers.NaiveBayesInducer;
import jbnc.measures.QualityMeasure;
import jbnc.measures.QualityMeasureHGC;
import jbnc.util.FrequencyCalc;

import org.junit.Before;
import org.junit.Test;

import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;

/**
 * Test done during learning of <b>jBNC</b> and the {@linkplain Classifier} analysis. Use the {@linkplain BayesianInducer} and some
 * {@linkplain QualityMeasure} to produce some {@linkplain BayesNet}.
 * 
 * The question of this {@linkplain BayesNet} being the generative model is out of scope of this test yet.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class JavaBayesLearnTest {

    private BayesNet network;
    private QualityMeasureHGC measure;
    private NaiveBayesInducer inducer;

    @Before
    public void set_up() {
        // create the heckerman-geiger-chickering measure for bayesian scoring function
        measure = new QualityMeasureHGC();
        // create the naive inducer
        inducer = new NaiveBayesInducer();
        inducer.setQualityMeasure(measure);
    }

    private void runInducer(int[][] data, AttributeSpecs[] attributes) throws Exception {
        // go with the Classifier like code
        FrequencyCalc fCalc = JBNCUtil.prepareFrequencyCalc(data, attributes);
        inducer.train(fCalc);

        // code up to this point passes without exceptions
        network = inducer.getNetwork();

        // print this network into standard output
        network.print(System.out);
    }

    private void runValidater() {
        // now the network is fully established
        BOAModel model = new BitBOAModel();
        model.setNetwork(network);

        // model should not throw any disturbances of the learned network
        model.validate();
    }

    @Test
    public void run_bayes_network_learning_algorithm_1() throws Exception {

        // describe the meta data
        int variablesLength = 2;
        int casesLength = 3;

        // prepare the attributes
        AttributeSpecs[] attributes = JBNCUtil.prepareBinominalAttributes(variablesLength);

        // prepare the data
        // the data look that:
        // G0 G1
        // 0 1
        // 1 1
        // 1 1
        int[][] data = new int[casesLength][variablesLength];

        data[0][0] = 0;
        data[0][1] = 1;

        data[1][0] = 1;
        data[1][1] = 1;

        data[2][0] = 1;
        data[2][1] = 1;

        this.runInducer(data, attributes);
        this.runValidater();
    }

    @Test
    public void run_bayes_network_learning_algorithm_2() throws Exception {
        // describe the meta data
        int variablesLength = 4;
        int casesLength = 3;

        // prepare the attributes
        AttributeSpecs[] attributes = JBNCUtil.prepareBinominalAttributes(variablesLength);

        // prepare the data
        // the data look that:
        // G0 G1 G2 G3
        // 0 1 1 1
        // 1 1 0 0
        // 1 1 1 1
        int[][] data = new int[casesLength][variablesLength];

        data[0][0] = 0;
        data[0][1] = 1;
        data[0][2] = 1;
        data[0][3] = 1;

        data[1][0] = 1;
        data[1][1] = 1;
        data[1][2] = 0;
        data[1][3] = 0;

        data[2][0] = 1;
        data[2][1] = 1;
        data[2][2] = 1;
        data[2][3] = 1;

        this.runInducer(data, attributes);
        this.runValidater();
    }
}
