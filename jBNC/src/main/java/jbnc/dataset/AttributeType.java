package jbnc.dataset;

import java.io.Serializable;

/**
 * Types of values represented by an attribute.
 */
final public class AttributeType implements Serializable {
    /**
     * Attribute to be ignored.
     */
    public final static AttributeType IGNORE = new AttributeType("IGNORE");

    /**
     * Attribute with continuous values represented as double.
     */
    public final static AttributeType CONTINUOUS = new AttributeType("CONTINUOUS");

    /**
     * Attribute with enumrated discrete values. Values are internally represented as integers.
     *
     * @see jbnc.dataset.AttributeSpecs#states
     */
    public final static AttributeType DISCRETE = new AttributeType("DISCRETE");

    /**
     * Attribute with N discrete values (0 to N-1).
     */
    public final static AttributeType DISCRETE_N = new AttributeType("DISCRETE_N");

    private final String name;

    private AttributeType(final String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
