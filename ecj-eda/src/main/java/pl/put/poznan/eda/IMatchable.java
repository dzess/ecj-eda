package pl.put.poznan.eda;

import ec.Individual;
import ec.Singleton;
import ec.Species;
import ec.gp.GPIndividual;
import ec.vector.BitVectorIndividual;

/**
 * Allows matching of elements during setup phases. Used for checking the {@linkplain Model} compatibility with various {@linkplain Species}
 * or {@linkplain Individual}.
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public interface IMatchable extends Singleton {

    /**
     * 
     * Checks if the current configuration is valid in terms of {@linkplain Model}. Meaning if the current implementations of
     * {@linkplain IMatchable} are valid with passed type of the {@linkplain Model}.
     * 
     * @param model
     *            : the model on which {@linkplain ModelSubpopulation} will work
     * @return true if everything is OK, false otherwise
     */
    public boolean matchModelType(Class<? extends Model> model);

    /**
     * Checks if the current configuration is valid in terms of {@linkplain Species} and their representations.
     * 
     * @param individual
     *            : the species which is used for representation of the problem, like: {@linkplain BitVectorIndividual} or
     *            {@linkplain GPIndividual}
     * 
     * @return true if everything is OK, false otherwise
     */
    public boolean matchIndividualType(Class<? extends Individual> individual);

    /**
     * Marks the {@linkplain IMatchable} implementation that the individual type which will be used will be that of the passed individual
     * type.
     * 
     * @param individual
     *            : the species which is used for representation of the problem, like: {@linkplain BitVectorIndividual} or
     *            {@linkplain GPIndividual}
     */
    public void markIndividualType(Class<? extends Individual> individual);
}
