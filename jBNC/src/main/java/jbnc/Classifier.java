/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc;

import gnu.getopt.Getopt;
import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.dataset.DatasetReaderInt;
import jbnc.inducers.*;
import jbnc.util.BNCTester;
import jbnc.util.Timer;

import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 *  Classifier tester. <pre>
 * USAGE: Classifier {options}
 *
 * where:
 *  -a {algor_name} Algorithm choices: 'naive', 'TAN', 'FAN', 'STAN', 'STAND',
 *                  'SFAN', 'SFAND'.
 *  -c {class_name} Name of the class variable. The default value is 'class'.
 *  -d              Print debugging information.
 *  -f {file_stem}  Load test data in C4.5 format (.names + .test),
 *                  file_stem-?-?.names - file with specification of attributes,
 *                  file_stem-?-?.data  - file with train cases.
 *                  file_stem-?-?.test  - file with test cases.
 * -n {file_name}  Save constructed classifier networks to
 *                 file_name-alg-qm-alpha.bif.
 *                 File is saved in BIF 0.15 format.
 * -q {q_measure}  Select quality measure. This relevant for the inducers that
 *                 perform selection of network candidates, like FAN or STAN.
 *                 Different measure have drastically different computational
 *                 complexity.
 *                 Quality measure choices:
 *                  LC     - local criterion: log p(c_l|D) [local]
 *                  SB     - standard Bayesian measure with penalty for size
 *                           [global].
 *                  HGS    - Heckerman-Geiger-Chickering measure [global].
 *                  LOO    - leave-one-out cross validation [local].
 *                  CV10   - ten fold cross validation.[local].
 *                  CV1010 - ten fold cross validation averaged ten times
 *                           [local].
 * -s              Number of smoothing priors to test. Has to be an integer
 *                 greater or equal zero.
 * -t              Print execution time.
 *
 * EXAMPLE: Classifier -a TAN -tf monk1
 * EXAMPLE: Classifier -dta FAN -q LOO -f monk1
 *</pre>
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class Classifier {

    // Constants
    protected final static String classNameDefault = "class";
    protected final static String databaseURL = "jdbc:odbc:JTest";

    // Other variables
    protected static String usageMsg;

    // Command line configuration parameters
    protected boolean useTimer = false;
    protected boolean debugMode = false;
    protected String algorithmName = null;
    protected String measureName = null;
    protected String fileNameStem = null;
    protected String trainFileName = null;
    protected String testFileName = null;
    protected String namesFileName = null;
    protected String netFileName = null;
    protected String className = null;
    protected int nbAlphas = 0;
    protected String tableName = null;


    /**
     *  Main function.
     *
     * @param  args  Description of Parameter
     */
    public static void main(String[] args) {
        Classifier classifier = new Classifier();
        classifier.run(args);
    }


    /**
     *  Print usage message for the command line interface.
     *
     * @param  toolName  how to call the command line tool.
     */
    protected static void setUsageMsg(String toolName) {
        usageMsg = "\n"
                + "USAGE: " + toolName + " <options>\n"
                + "\n"
                + "where:\n"
                + " -a {algor_name} Algorithm choices: 'naive', 'TAN', 'FAN', 'STAN', 'STAND',\n"
                + "                 'SFAN', 'SFAND'.\n"
                + " -c {class_name} Name of the class variable. The default value is 'class'.\n"
                + " -d              Print debugging information.\n"
                + " -f {file_stem}  Load test data in C4.5 format (.names + .test),\n"
                + "                  file_stem-?-?.names - file with specification of attributes,\n"
                + "                  file_stem-?-?.data  - file with train cases.\n"
                + "                  file_stem-?-?.test  - file with test cases.\n"
                + " -n {file_name}  Save constructed classifier networks to\n"
                + "                 file_name-alg-qm-alpha.bif.\n"
                + "                 File is saved in BIF 0.15 format.\n"
                + " -q {q_measure}  Select quality mesure. This relevant for the inducers that\n"
                + "                 perform selection of network candidates, like FAN or STAN.\n"
                + "                 Diffrent measure have drasticaly diffrent computational \n"
                + "                 complexity.              \n"
                + "                 Quality measure choices:\n"
                + "                  LC     - local criterion: log p(c_l|D) [local]\n"
                + "                  SB     - standard Bayesian measure with penalty for size\n"
                + "                           [global].\n"
                + "                  HGS    - Heckerman-Geiger-Chickering measure [global].\n"
                + "                  LOO    - leave-one-out cross validation [local].\n"
                + "                  CV10   - ten fold cross validation.[local].\n"
                + "                  CV1010 - ten fold cross validation averaged ten times \n"
                + "                           [local].\n"
                + " -s              Number of smoothing priors to test. Has to be an integer\n"
                + "                 greater or equal zero.\n"
                + " -t              Print execution time.\n"
                + "\n"
                + "EXAMPLE: " + toolName + " -a TAN -tf monk1\n"
                + "EXAMPLE: " + toolName + " -dta FAN -q LOO -f monk1\n";
    }


    /**  Create and test a classifier. */
    protected void run() {
        try {
            jbnc.measures.QualityMeasure qm;
            if (measureName == null) {
                qm = null;
            } else if (measureName.equalsIgnoreCase("LC")) {
                qm = new jbnc.measures.QualityMeasureLC();
            } else if (measureName.equalsIgnoreCase("SB")) {
                qm = new jbnc.measures.QualityMeasureSB();
            } else if (measureName.equalsIgnoreCase("HGC")) {
                qm = new jbnc.measures.QualityMeasureHGC();
            } else if (measureName.equalsIgnoreCase("LOO")) {
                qm = new jbnc.measures.QualityMeasureLCV_LOO();
            } else if (measureName.equalsIgnoreCase("CV10")) {
                qm = new jbnc.measures.QualityMeasureLCV(10, 1);
            } else if (measureName.equalsIgnoreCase("CV55")) {
                qm = new jbnc.measures.QualityMeasureLCV(5, 5);
            } else if (measureName.equalsIgnoreCase("CV1010")) {
                qm = new jbnc.measures.QualityMeasureLCV(10, 10);
            } else {
                System.out.println("Unrecognized quality measure name: " + measureName + "\n");
                System.out.println(usageMsg);
                return;
            }

            // Select algorithm
            BayesianInducer inducer = null;
            String algCode = null;
            if (algorithmName.equalsIgnoreCase("naive")) {
                inducer = new NaiveBayesInducer();
                qm = null;
                algCode = algorithmName;
            } else if (algorithmName.equalsIgnoreCase("TAN")) {
                inducer = new TANInducer();
                qm = null;
                algCode = algorithmName;
            } else if (algorithmName.equalsIgnoreCase("FAN")) {
                inducer = new FANInducer();
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("STAN")) {
                STANInducer stan = new STANInducer();
                stan.setDiscardNonDependent(false);
                inducer = stan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("STAND")) {
                STANInducer stan = new STANInducer();
                stan.setDiscardNonDependent(true);
                inducer = stan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("SFAN")) {
                SFANInducer sfan = new SFANInducer();
                sfan.setDiscardNonDependent(false);
                inducer = sfan;
                algCode = algorithmName + "-" + measureName;
            } else if (algorithmName.equalsIgnoreCase("SFAND")) {
                SFANInducer sfan = new SFANInducer();
                sfan.setDiscardNonDependent(true);
                inducer = sfan;
                algCode = algorithmName + "-" + measureName;
            } else {
                System.out.println("Unrecognized algorithm name: " + algorithmName + "\n");
                System.out.println(usageMsg);
                return;
            }

            System.out.println("File stem: " + fileNameStem);
            System.out.println("Algorithm: " + algorithmName);
            if (qm != null) {
                System.out.println("Quality measure: " + qm.getName());
            }
            System.out.println("");

            // Load train set
            if (debugMode) {
                System.out.println("\nTraining...");
            }
            namesFileName = fileNameStem + ".names";
            trainFileName = fileNameStem + ".data";
            testFileName = fileNameStem + ".test";
            DatasetInt trainSet = loadDataset(namesFileName, trainFileName);
            jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(trainSet);
            if (qm != null) {
                qm.setDataset(trainSet);
            }
            inducer.setQualityMeasure(qm);
            System.out.println("+");

            inducer.train(fc);
            BayesNet net = inducer.getNetwork();

            // Load test set
            if (debugMode) {
                System.out.println("\nTesting...");
            }
            DatasetInt testSet = loadDataset(namesFileName, testFileName);

            Stats stats = new Stats();

            // Test network
            BNCTester tester = new BNCTester();
            BNCTester.Result r = tester.test(net, testSet);

            tester.reportShort();
            stats.add(0, r, net);
            for (int a = 1; a < nbAlphas; ++a) {
                inducer.train(fc, true, a);
                net = inducer.getNetwork();
                r = tester.test(net, testSet);
                stats.add(a, r, net);
                System.out.print("     alpha_" + a + ": ");
                tester.reportShort();
            }

            if (nbAlphas > 0) {
                stats.reportBestShort();
            }

            // Log results to database
            if (tableName != null) {
                debugMode = true;
                int i = fileNameStem.length() - 1;
                for (; i >= 0; --i) {
                    char c = fileNameStem.charAt(i);
                    if (c == '/' || c == '\\') {
                        break;
                    }
                }
                String datasetName = fileNameStem.substring(i + 1);

                BNCTester.Result bestR = stats.getBestResult();

                System.out.println("Opening database link '" + databaseURL +
                        "' [" + tableName + ":"
                        + datasetName + ":"
                        + algCode + "].");
                jbnc.database.DBLog dbLog = new jbnc.database.DBLog();
                dbLog.open(databaseURL, "", "");
                dbLog.insert(tableName, datasetName, algCode,
                        bestR.getError() * 100,
                        bestR.getTheoreticalStdDev() * 100);
                dbLog.close();
            }

            if (netFileName != null && stats.getBestNet() != null) {
                // Save to a file
                netFileName += "-" + algCode + "-" + stats.getBestAlpha() + ".bif";
                FileOutputStream out = new FileOutputStream(netFileName);
                PrintStream pOut = new PrintStream(out);
                stats.getBestNet().save_bif(pOut);
            }

            System.out.println("");
        } catch (Exception e) {
            System.out.println("\nError: " + e);
            if (debugMode) {
                e.printStackTrace();
            }
        }
    }


    /**
     *  Wrapper for loading a dataset in C4.5 format.
     *
     * @param  namesFile      Description of Parameter
     * @param  dataFile       Description of Parameter
     * @return                Description of the Returned Value
     * @exception  Exception  Description of Exception
     */
    protected DatasetInt loadDataset(String namesFile, String dataFile) throws Exception {
        if (debugMode) {
            System.out.println("\nLoading data file: " + dataFile);
        }

        DatasetInt dataset = new DatasetInt();
        if (namesFileName != null) {
            // Load data in C4.5 format

            dataset.setDiscardIncompleteCases(true);
            dataset.openC45(namesFile, dataFile);

        } else {
            // Load data in from comma delimited file with a header

            DatasetReaderInt datasetReader = new DatasetReaderInt();
            datasetReader.setDiscardIncompleteCases(true);
            dataset = (DatasetInt) datasetReader.open(dataFile, className);
        }

        // Remove attributes of type IGNORE
        int nbOrigVars = dataset.names.length;
        dataset.discardAllOfType(AttributeType.IGNORE);

        int nbVars = dataset.names.length;
        if (debugMode && nbVars != nbOrigVars) {
            System.out.println("" + (nbOrigVars - nbVars) + " attributes of type IGNORE have been discarded.");
        }

        if (dataset.names == null || dataset.names.length == 0) {
            throw new Exception("'.names' is empty");
        }

        int nbAttributes = dataset.names.length - 1;
        int nbClasses = dataset.names[nbAttributes].getStates().length;

        if (debugMode) {
//        System.out.println("Number of classes    = "+nbClasses);
            System.out.println("Number of attributes = " + nbAttributes + " + one class.");
        }

        // Verify that attributes are discrete
        for (int i = 0; i < nbAttributes; ++i) {
            AttributeType type = dataset.names[i].getType();
            if (type == AttributeType.DISCRETE || type == AttributeType.DISCRETE_N) {
                continue;
            } else if (type == AttributeType.CONTINUOUS) {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is continuous.");
            } else if (type == AttributeType.IGNORE) {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is to be ignored (?).");
            } else {
                throw new Exception("Attribute '" + dataset.names[i].getName()
                        + "' is of unknown type = "
                        + dataset.names[i].getType());
            }
        }

        if (debugMode) {
            System.out.println("Number of cases = " + dataset.cases.size());
        }

        return dataset;
    }


    /**
     *  Process command line arguments.
     *
     * @param  argv  Description of Parameter
     * @return       'false' when command line is invalid.
     */
    protected boolean processCommandLine(String[] argv) {
        try {
            Getopt g = new Getopt("ERROR", argv, "a:c:df:l:n:q:s:t");

            int c;
            fileNameStem = null;
            algorithmName = null;
            String alphaString = null;
            while ((c = g.getopt()) != -1) {
                switch (c) {
                    case 'a':
                        algorithmName = g.getOptarg();
                        break;
                    case 'c':
                        className = g.getOptarg();
                        break;
                    case 'd':
                        debugMode = true;
                        break;
                    case 'f':
                        fileNameStem = g.getOptarg();
                        break;
                    case 'l':
                        tableName = g.getOptarg();
                        break;
                    case 'n':
                        netFileName = g.getOptarg();
                        break;
                    case 'q':
                        measureName = g.getOptarg();
                        break;
                    case 't':
                        useTimer = true;
                        break;
                    case 's':
                        alphaString = g.getOptarg();
                        break;
                    case '?':
                        break;
                    default:
                        System.out.println("ERROR: getopt() returned: " + c);
                        return false;
                }
            }

            if (algorithmName == null) {
                System.out.println("ERROR: Algorithm name, option 'a', has to be specified.");
                return false;
            }

            if (fileNameStem == null) {
                System.out.println("ERROR: File stem, option 'f', has to be specified.");
                return false;
            }

            if (alphaString != null) {
                try {
                    nbAlphas = Integer.parseInt(alphaString, 10);
                } catch (NumberFormatException e) {
                    System.out.println("ERROR: Invalid parameter of option 's'; has to be an integer greater or equal zero.");
                    return false;
                }
            }

// Class name
            int index = g.getOptind();
            if (index < argv.length) {
// Staff left on the command line
                System.out.print("ERROR: Too many arguments in the command line. Extraneous: ");
                for (; index < argv.length; ++index) {
                    System.out.print(argv[index] + ", ");
                }
                return false;
            }

            if (debugMode) {
                System.out.println("algorithm name  = " + algorithmName);
                System.out.println("quality measure = " + measureName);
                System.out.println("class name      = " + className);
                System.out.println("debug mode      = " + debugMode);
                System.out.println("train data      = " + trainFileName);
                System.out.println("test data       = " + testFileName);
                System.out.println("names file      = " + namesFileName);
                System.out.println("network file    = " + netFileName);
                System.out.println("use timer       = " + useTimer);
            }

            return true;
        } catch (Exception e) {
            System.out.println("Error: " + e);
            if (debugMode) {
                e.printStackTrace();
            }
        }

        return false;
    }


    protected void run(String argv[]) {
        setUsageMsg("Classifier");

        try {
            Timer t = new Timer();

// Print header
            System.out.println("\nClassifier tester.\n");

// Process command line arguments
            if (!processCommandLine(argv)) {
                System.out.println(usageMsg);
                return;
            }

// Run the test
            run();

// Print execution time
            if (useTimer) {
                t.println("Execution time = ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *  Utility class for handling results of classifier testing.
     *
     * @author     Jarek Sacha
     * @since      June 1, 1999
     */
    static class Stats {

        int bestAlpha = -1;
        BNCTester.Result bestResult = null;
        BayesNet bestNet = null;


        /**  Constructor for the Stats object */
        public Stats() {
            reset();
        }


        /**
         *  Return best alpha (prior).
         *
         * @return    The BestAlpha value
         */
        public int getBestAlpha() {
            return bestAlpha;
        }


        /**
         *  Return best classification result.
         *
         * @return    The BestResult value
         */
        public BNCTester.Result getBestResult() {
            return bestResult;
        }


        /**
         *  Return constructed Bayesian network
         *
         * @return    The BestNet value
         */
        public BayesNet getBestNet() {
            return bestNet;
        }


        public void add(int alpha, BNCTester.Result r, BayesNet net) throws Exception {
            if (bestResult == null || r.getError() < bestResult.getError()) {
                bestAlpha = alpha;
                bestResult = r;
                bestNet = net;
            }
        }


        /**
         *  Report the best result in a short format.
         *
         * @exception  Exception  Description of Exception
         */
        public void reportBestShort() throws Exception {
            System.out.print("\nBest alpha_" + bestAlpha + ": ");
            bestResult.reportShort();
        }


        /**  Reset member variables to initial values. */
        public void reset() {
            bestAlpha = -1;
            bestResult = null;
        }
    }

}
