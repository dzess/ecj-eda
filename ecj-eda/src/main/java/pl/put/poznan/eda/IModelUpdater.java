package pl.put.poznan.eda;

import ec.EvolutionState;

/**
 * Updates the model stored in the {@linkplain ModelSubpopulation} using some algorithms. The exact type of the {@linkplain IModelUpdater}
 * depends on the type of {@linkplain Model} type and should be compatible.
 * 
 * <p>
 * The internal state of the implementation of {@linkplain IModelUpdater} is crucial. If the stored internally {@linkplain Model} is going
 * to be used more than one generation, for example for a scope of few generations it depends only to the implementation of
 * {@linkplain IModelUpdater}.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public interface IModelUpdater extends IMatchable {

    /**
     * Retrieves all the information from samples passed in {@linkplain ModelSubpopulation}. Called by {@linkplain SimpleEDABreeder} during
     * breeding phase. Invoked per generation before apply method.
     * 
     * @param modelSubpopulation
     *            current individuals contain samples from which (maybe not only) the {@linkplain Model} should be inferred
     * @param state
     *            evolution state which should not be changed too much
     */
    public void retrieveModel(ModelSubpopulation modelSubpopulation, EvolutionState state);

    /**
     * Applies the already stored information into the {@linkplain Model} stored in {@linkplain ModelSubpopulation}.. Called by
     * {@linkplain SimpleEDABreeder} during breeding phase. Invoked per generation after retrieve method.
     * 
     * 
     * @param modelSubpopulation
     *            holds the current {@linkplain Model}
     * @param state
     *            evolution state which should not be used too much
     */
    public void applyModel(ModelSubpopulation modelSubpopulation, EvolutionState state);

}
