/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.util.Vector;

class ParseClass extends ParseLine {

  ParseClass(String line) {
    super(line);
  }


  /**
   *  Description of the Method
   *
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public AttributeSpecs parse() throws Exception {
    Vector classes = new Vector();

    String token;
    while ((token = nextToken(',')) != null) {
      classes.add(token);
    }

    if (classes.size() == 0) {
      return null;
    }
    // line is empty

    if (classes.size() == 1) {
      throw new Exception("ParseClass.parse(): only one class read");
    }

    String[] states = new String[classes.size()];
    for (int i = 0; i < classes.size(); ++i) {
      states[i] = classes.get(i).toString();
    }

    AttributeSpecs specs = new AttributeSpecs();
    specs.setName("class");
    specs.setType(AttributeType.DISCRETE);
    specs.setStates(states);

    return specs;
  }

}
