/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.util.Vector;

class ParseAttrib extends ParseLine {

  ParseAttrib(String line) {
    super(line);
  }


  /**
   *  Description of the Method
   *
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public AttributeSpecs parse() throws Exception {
    // Get attribute name
    String name = nextToken(':');

    if (name == null) {
      return null;
    }

    AttributeSpecs a = new AttributeSpecs();
    a.setName(name);

    // Get get attribute type or enumeration of states.
    Vector states = new Vector();
    String token;
    while ((token = nextToken(',')) != null) {
      if (token.charAt(0) == ':') {
        // go around a bug in java 1.3 (old separator is not discarded)
        token = token.substring(1).trim();
      }
      states.add(token);
    }

    if (states.size() == 0) {
      throw new Exception("ParseAttrib.parse(): attribute type is empty");
    }

    // Attribute type
    if (states.size() == 1) {
      String typeStr = states.get(0).toString();
      if (typeStr.equals("ignore")) {
        a.setType(AttributeType.IGNORE);
        return a;
      }
      if (typeStr.equals("continuous")) {
        a.setType(AttributeType.CONTINUOUS);
        return a;
      }
      if (typeStr.startsWith("discrete")) {
        String s = typeStr.substring(8);
        try {
          a.setType(AttributeType.DISCRETE);
          int n = (new Integer(s)).intValue();
          String[] statesStr = new String[n];
          for (int i = 0; i < n; ++i) {
            statesStr[i] = (new Integer(i)).toString();
          }
          a.setStates(statesStr);

          return a;
        } catch (NumberFormatException e) {
          throw new Exception("ParseAttrib.parse(): unknown attribute type: " + token);
        }
      }

      throw new Exception("ParseAttrib.parse(): unknown attribute type: " + token);
    }

    // Enumerated states

    String[] statesStr = new String[states.size()];
    for (int i = 0; i < states.size(); ++i) {
      statesStr[i] = states.get(i).toString();
    }

    a.setType(AttributeType.DISCRETE);
    a.setStates(statesStr);

    return a;
  }
}
