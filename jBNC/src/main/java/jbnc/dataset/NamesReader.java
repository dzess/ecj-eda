/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.dataset;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;

/**
 *  Read C4.5's '.names' files describing the class and each of the attributes.
 *  <br>
 *  <br>
 *  C4.5 saves data sets in two types of files. The first type contains
 *  description of description of the class and each of the attributes. The
 *  second type contains the actual data in a comma delimited format. File names
 *  have the same stem, different extensions: <blockquote> <tt>.names</tt> -
 *  file containing description of data (first type)<br>
 *  <tt>.test</tt> - file containing test cases (second type) <tt>.data</tt> -
 *  file containing training cases (second type)<br>
 *  <tt>.all</tt> - file containing all available cases (second type)<br>
 *  </blockquote>
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class NamesReader {
  /**
   *  Reads '.names' file containing description of a class and variables.
   *
   * @param  filename       Description of Parameter
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public static AttributeSpecs[] open(String filename) throws Exception {
    int lineCount = 0;

    try {
      // Open file
      BufferedReader in = new BufferedReader(new FileReader(filename));

      // Get class description
      AttributeSpecs classSpecs = null;
      String s = in.readLine();
      while (s != null) {
        ++lineCount;
        ParseClass p = new ParseClass(s);
        if ((classSpecs = p.parse()) != null) {
//          System.out.println("Parsed> "+ classSpecs.toString());
          break;
        }
        s = in.readLine();
      }

      if (classSpecs == null) {
        throw new Exception("NamesReader.open: no class description in file " + filename);
      }

      // Get attributes description
      Vector attribSpecs = new Vector();
      while ((s = in.readLine()) != null) {
        ++lineCount;
        ParseAttrib p = new ParseAttrib(s);
        AttributeSpecs specs = p.parse();
        if (specs != null) {
//          System.out.println("Parsed> "+ specs.toString());
          attribSpecs.add(specs);
        }
      }

      lineCount = -1;
      in.close();

      if (attribSpecs.size() < 1) {
        throw new Exception("NamesReader.open: no attribute descriptions in file " + filename);
      }

      // Convert to array
      AttributeSpecs[] names = new AttributeSpecs[1 + attribSpecs.size()];
      for (int i = 0; i < attribSpecs.size(); ++i) {
        names[i] = (AttributeSpecs) attribSpecs.get(i);
      }
      names[attribSpecs.size()] = classSpecs;

      return names;
    } catch (Exception e) {
      if (lineCount > 0) {
        System.out.println("Error in line " + lineCount);
        System.out.println(e.toString());
      }

      throw e;
    }
  }
}
