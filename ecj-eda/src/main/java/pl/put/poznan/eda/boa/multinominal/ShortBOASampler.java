package pl.put.poznan.eda.boa.multinominal;

import java.util.LinkedList;
import java.util.List;

import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.BOASampler;
import pl.put.poznan.eda.boa.util.resolver.ShortIndexResolver;
import pl.put.poznan.eda.util.ECJDistributionSampler;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.EvolutionState;
import ec.Individual;
import ec.Species;
import ec.util.Parameter;
import ec.vector.ShortVectorIndividual;

/**
 * Basic implementation of sampling of the {@linkplain ShortBOAModel}.
 * 
 * <p>
 * Currently supports all the types supported by {@linkplain ShortBOABase}
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class ShortBOASampler extends ShortBOABase implements IModelSampler {

    private static final long serialVersionUID = -3107853186794629540L;
    private static final int C_THREAD_NUMBER = 0;
    private BOASampler<Short> sampler;

    public ShortBOASampler() {
        this(new ECJDistributionSampler());
    }

    public ShortBOASampler(IDistributionSampler distributionMock) {
        ShortIndexResolver indexResolver = new ShortIndexResolver();
        this.sampler = new BOASampler<Short>(distributionMock, indexResolver);
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {
        state.output.exitIfErrors();
    }

    @Override
    public List<Individual> sampleModel(Model inputModel, int amount, Species species, EvolutionState state) {

        BOAModel model = (BOAModel) inputModel;

        List<Individual> newPopulationInds = new LinkedList<Individual>();

        sampler.reset();

        for (int indIndex = 0; indIndex < amount; indIndex++) {

            // create the new individual
            ShortVectorIndividual ind = (ShortVectorIndividual) species.newIndividual(state, C_THREAD_NUMBER);

            // sampling of the individual from the model
            Short[] sampledGenome = sampler.sampleGenomeFromModel(model, Short.class, state);

            short[] unboxedGenome = new short[sampledGenome.length];
            for (int i = 0; i < sampledGenome.length; i++) {
                unboxedGenome[i] = sampledGenome[i];
            }
            ind.setGenome(unboxedGenome);

            // add individual to the new subpopulation
            newPopulationInds.add(ind);
        }
        return newPopulationInds;
    }

}
