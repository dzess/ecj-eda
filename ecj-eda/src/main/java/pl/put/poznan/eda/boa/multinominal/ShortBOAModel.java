package pl.put.poznan.eda.boa.multinominal;

import java.util.Vector;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import ec.EvolutionState;
import ec.Initializer;
import ec.Population;
import ec.Subpopulation;
import ec.util.Parameter;
import ec.vector.IntegerVectorSpecies;
import ec.vector.ShortVectorIndividual;

/**
 * Bayesian Network model used to store the probabilities and linkage information about the genes. This version supports
 * {@linkplain ShortVectorIndividual}.
 * 
 * <p>
 * {@linkplain ShortVectorIndividual} is a vector of number, exactly of {@linkplain Short}. The internal representation of
 * {@linkplain BayesNet} is {@linkplain String} representation. Thus the {@linkplain Short} values will be coded using the simplest possible
 * approach:
 * <ul>
 * <li><code>toString()</code></li>
 * <li><code>Short.parseString()</code></li>
 * </ul>
 * </p>
 * 
 * <p>
 * This approach is not effective at all but the whole other system would require reimplementation of {@linkplain BayesNet} and
 * <code>Java Bayes Network Toolkit</code>.
 * </p>
 * 
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 */
public class ShortBOAModel extends BOAModel {

    private static final long serialVersionUID = -435514981415527769L;

    private static final String C_NETWORK_NAME = "SHORT_BOA_INITIALIZED_NETWORK";

    public static final Parameter U_MAX_GENE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(IntegerVectorSpecies.P_MAXGENE);

    public static final Parameter U_MIN_GENE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(IntegerVectorSpecies.P_MINGENE);

    private Integer maxGene;
    private Integer minGene;

    @Override
    public void setup(EvolutionState state, Parameter base) {

        // try reading those max-gene / min-gene from EC but defaults to null
        // value (-1) is to deceive ecj lacking code
        maxGene = state.parameters.getIntWithDefault(U_MAX_GENE, null, -1);
        if (maxGene == -1) {
            maxGene = null;
        }

        minGene = state.parameters.getIntWithDefault(U_MIN_GENE, null, -1);
        if (minGene == -1) {
            // default value for min gene is 0 (once again this is the case of jBNC)
            minGene = 0;
        }

        // this restriction comes from jBNC
        if (minGene != 0) {
            state.output.fatal("the min-gene in boa model has to be 0");
        }

        // invoke the super method of this (class)
        super.setup(state, base);
    }

    @Override
    protected void generateBayesNet() {

        int newtworkDsitrbutions = genomeSize;
        network = new BayesNet(C_NETWORK_NAME, genomeSize, newtworkDsitrbutions);

        int cardinality = -1;
        int min;
        int max;
        if (isMaxMinGeneEnabled()) {
            // +1 because 0 is inclusive
            cardinality = maxGene + 1;
            min = 0;
            max = maxGene;
        } else {
            // +1 because 0 is inclusive
            cardinality = Short.MAX_VALUE + 1;
            min = 0;
            max = Short.MAX_VALUE;
        }
        double expectedProbabilty = 1.0 / (double) cardinality;

        try {
            // set variables
            ProbabilityVariable[] variables = network.get_probability_variables();
            ProbabilityFunction[] functions = network.get_probability_functions();
            for (int i = 0; i < genomeSize; i++) {

                // TODO: the naming schema of variables is hardcoded here should be more pluggable
                String name = "G" + i;
                Vector<String> vector = new Vector<String>();

                variables[i] = JBNCUtil.createShortMulitnominalVariable(network, name, vector, min, max);

                Vector<String> secondVector = new Vector<String>();
                DiscreteVariable[] discreteVars = new DiscreteVariable[1];
                discreteVars[0] = variables[i];

                double[] probablitiesValues = new double[cardinality];
                for (int j = 0; j < cardinality; j++) {
                    probablitiesValues[j] = expectedProbabilty;
                }

                functions[i] = new ProbabilityFunction(network, discreteVars, probablitiesValues, secondVector);

            }
        } catch (Exception e) {
            throw new IllegalStateException("jBNC toolkit throws expetion during initialization of the net", e);
        }
    }

    private boolean isMaxMinGeneEnabled() {
        return maxGene != null;
    }

    private void assertMinGene(short numberValue, ProbabilityVariable variable) {
        if (numberValue < minGene) {
            String message = "One of the value in variable '" + variable.get_name() + "' is lower than " + minGene;
            throw new IllegalStateException(message);
        }
    }

    private void assertMaxGene(short numberValue, ProbabilityVariable variable) {
        if (numberValue > maxGene) {
            String message = "One of the value in variable '" + variable.get_name() + "' is greater than " + maxGene;
            throw new IllegalStateException(message);
        }
    }

    @Override
    protected boolean isIndividualValid() {
        return !individual.getClass().equals(ShortVectorIndividual.class);
    }

    @Override
    public void validate() {
        ProbabilityVariable[] variables = network.get_probability_variables();
        long shortSize = 1 << Short.SIZE;
        for (int i = 0; i < variables.length; i++) {
            ProbabilityVariable variable = variables[i];

            // validate the number of variables
            if (variable.get_values().length > shortSize) {
                throw new IllegalStateException("Each probability variable in ShortBOA must have less variables than scope of Short");
            }

            // we assume that all values must be "parsable" to Short
            String[] values = variable.get_values();
            for (int j = 0; j < values.length; j++) {
                String value = values[j];
                try {
                    short numberValue = Short.parseShort(value);

                    // check if value is range between
                    if (isMaxMinGeneEnabled()) {
                        assertMinGene(numberValue, variable);
                        assertMaxGene(numberValue, variable);
                    }
                } catch (NumberFormatException e) {
                    String message = "One of the value in variable '" + variable.get_name() + "has non parsable short value";
                    throw new IllegalStateException(message, e);
                }
            }
        }

        // the other validation is moved to the last point because of the sake
        // of testing validate() method on this class
        super.validate();
    }

    public Integer getMaxGene() {
        return maxGene;
    }

    public void setMaxGene(int maxGene) {
        this.maxGene = maxGene;
    }

    public Integer getMinGene() {
        return minGene;
    }

    public void setMinGene(int minGene) {
        this.minGene = minGene;
    }
}
