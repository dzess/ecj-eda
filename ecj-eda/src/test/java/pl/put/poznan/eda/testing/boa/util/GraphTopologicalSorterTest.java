package pl.put.poznan.eda.testing.boa.util;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import pl.put.poznan.eda.boa.util.graph.GraphNetworkUtils;
import pl.put.poznan.eda.boa.util.graph.GraphTopologicalSorter;

public class GraphTopologicalSorterTest {

    private GraphTopologicalSorter sorter;
    private GraphNetwork graph;
    private List<Integer>[] preds;
    private List<Integer>[] succs;

    @Before
    public void set_up() {
        sorter = new GraphTopologicalSorter();
        graph = Mockito.mock(GraphNetwork.class);
    }

    private void prepareGraph(int length) {
        preds = GraphNetworkUtils.createArrayOfLists(length);
        succs = GraphNetworkUtils.createArrayOfLists(length);

        Mockito.when(graph.getPredecessorArrayDeepCopy()).thenReturn(preds);
        Mockito.when(graph.getSuccessorArrayDeepCopy()).thenReturn(succs);
    }

    @Test
    public void sorting_topologicaly_case_1() {

        // graph structure to be sorted here is
        // A-> B thus 0 -> 1
        this.prepareGraph(2);

        // A
        succs[0].add(1);
        // B
        preds[1].add(0);

        // create some nice cases from the earlier tests
        List<Integer> ranking = sorter.sort(graph);

        // assert
        Assert.assertEquals(2, ranking.size());
        Assert.assertEquals(new Integer(0), ranking.get(0));
        Assert.assertEquals(new Integer(1), ranking.get(1));
    }

    @Test
    public void sorting_topologicaly_case_2() {
        // sorting nontrivial case
        // A->B->C->D this 0->1->2->3
        this.prepareGraph(4);

        // A
        succs[0].add(1);
        // B
        succs[1].add(2);
        preds[1].add(0);
        // C
        succs[2].add(3);
        preds[2].add(1);
        // D
        preds[3].add(2);

        // create some nice cases from the earlier tests
        List<Integer> ranking = sorter.sort(graph);

        // assert
        Assert.assertEquals(4, ranking.size());
        Assert.assertEquals(new Integer(0), ranking.get(0));
        Assert.assertEquals(new Integer(1), ranking.get(1));
        Assert.assertEquals(new Integer(2), ranking.get(2));
        Assert.assertEquals(new Integer(3), ranking.get(3));
    }

    @Ignore
    public void sorting_topologicaly_case_3() {
        // sorting tough case
        // A->B->C->D
        // E->F-> D
        this.prepareGraph(6);

        // A
        succs[0].add(1);
        // B
        succs[1].add(2);
        preds[1].add(0);
        // C
        succs[2].add(3);
        preds[2].add(1);
        // D
        preds[3].add(2);
        preds[3].add(5);
        // E
        succs[4].add(5);
        // F
        succs[5].add(3);
        preds[5].add(4);

        // create some nice cases from the earlier tests
        List<Integer> ranking = sorter.sort(graph);

        // assert
        Assert.assertEquals(6, ranking.size());

        // ABC
        Assert.assertEquals(new Integer(0), ranking.get(0));
        Assert.assertEquals(new Integer(1), ranking.get(1));
        Assert.assertEquals(new Integer(2), ranking.get(2));
        // EF
        Assert.assertEquals(new Integer(4), ranking.get(3));
        Assert.assertEquals(new Integer(5), ranking.get(4));
        // D
        Assert.assertEquals(new Integer(3), ranking.get(5));
    }

    @Test
    public void sorting_topologicaly_bounding_case_1() {
        // empty net
        this.prepareGraph(4);

        // no connections at all (any sequence will do)
        // check if no exception is being thrown

        List<Integer> ranking = sorter.sort(graph);
        Assert.assertEquals(4, ranking.size());
    }

    @Test(expected = IllegalStateException.class)
    public void sorting_topologicaly_bounding_case_2() {
        // not DAG sorting
        // A->B->C-A so there is a cycle
        this.prepareGraph(3);

        // A
        succs[0].add(1);
        // B
        succs[1].add(2);
        preds[1].add(0);
        // C
        succs[2].add(0);
        preds[2].add(1);

        sorter.sort(graph);
    }

    @Test(expected = IllegalStateException.class)
    public void sorting_topologicaly_bounding_case_3() {
        // not DAG sorting in a bigger graph

        // sorting nontrivial case
        // A->B->C->D -> E this 0->1->2->3
        // D-> A
        this.prepareGraph(5);

        // A
        succs[0].add(1);
        // B
        succs[1].add(2);
        preds[1].add(0);
        // C
        succs[2].add(3);
        preds[2].add(1);
        // D
        preds[3].add(2);
        succs[3].add(0);
        succs[3].add(4);
        // E
        preds[4].add(3);

        sorter.sort(graph);
    }

}
