package pl.put.poznan.eda.testing;

import org.hamcrest.Matcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.matchers.GreaterOrEqual;
import org.mockito.internal.matchers.LessThan;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.EvolutionState;
import ec.Individual;
import ec.Species;

/**
 * Helper class for testing {@linkplain IModelSampler} implementations.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class ECJSamplerTestingBase extends ECJTestingBase {

    protected Species species;
    protected IDistributionSampler distributionMock;

    protected void prepareSpecies(int genomeSize, Individual individual, Individual... individuals) {
        species = Mockito.mock(Species.class);

        subpopulation.species = species;

        // prepare the species thing
        Mockito.when(species.newIndividual(Matchers.any(EvolutionState.class), Matchers.anyInt())).thenReturn(individual, individuals);
    }

    protected IDistributionSampler prepareDistributionSampler() {
        IDistributionSampler dist = Mockito.mock(IDistributionSampler.class);
        Mockito.when(dist.sampleDistribution(Matchers.any(double[].class), Matchers.any(EvolutionState.class), Matchers.anyInt()))
                .thenReturn(0);
        Mockito.when(dist.sampleDistribution(Matchers.anyDouble(), Matchers.any(EvolutionState.class), Matchers.anyInt())).thenReturn(true);

        return dist;
    }

    protected void prepareDistribution(final double threshold) {

        // CHANGE THE ISampleDisitrbution to not be returning only ones
        // but to return the true when double is greater than threshold and
        // return false when the threshold is not met
        Matcher<Double> doubleMatcherGrater = new GreaterOrEqual<Double>(threshold);
        Matcher<Double> doubleMatcherLess = new LessThan<Double>(threshold);
        Mockito.when(
                distributionMock.sampleDistribution(Matchers.doubleThat(doubleMatcherGrater), Matchers.any(EvolutionState.class),
                        Matchers.anyInt())).thenReturn(false);

        Mockito.when(
                distributionMock.sampleDistribution(Matchers.doubleThat(doubleMatcherLess), Matchers.any(EvolutionState.class),
                        Matchers.anyInt())).thenReturn(true);

        // the same part of matcher for vector of probabilities
        Mockito.when(
                distributionMock.sampleDistribution(Matchers.any(double[].class), Matchers.any(EvolutionState.class), Matchers.anyInt()))
                .thenAnswer(new Answer<Integer>() {
                    @Override
                    public Integer answer(InvocationOnMock invocation) throws Throwable {
                        Object[] args = invocation.getArguments();

                        // here is logic for behavior of this mock
                        double[] probabilites = (double[]) args[0];
                        double currentSum = 0;

                        // compute partials sums
                        for (int i = 0; i < probabilites.length; i++) {
                            currentSum = currentSum + probabilites[i];

                            // check if threshold is where ?
                            if (currentSum >= threshold) {
                                return new Integer(i);
                            }
                        }

                        return new Integer(probabilites.length);
                    }
                });
    }

}