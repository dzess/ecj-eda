package pl.put.poznan.eda.boa.binominal;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeSpecs;
import jbnc.inducers.BayesianInducer;
import jbnc.inducers.NaiveBayesInducer;
import jbnc.measures.QualityMeasure;
import jbnc.measures.QualityMeasureHGC;
import jbnc.util.FrequencyCalc;
import pl.put.poznan.eda.IModelUpdater;
import pl.put.poznan.eda.ModelSubpopulation;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import ec.EvolutionState;
import ec.Individual;
import ec.Initializer;
import ec.Population;
import ec.Subpopulation;
import ec.util.ParamClassLoadException;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.VectorSpecies;

/**
 * 
 * Basic implementation of the updater class for {@linkplain BitBOAModel} and underlining {@linkplain BayesNet} implementation of Bayesian
 * Networks. <b>The {@linkplain BitBOAModel} is not used between generations in this implementation of Bayesian Optimization Algorithm!</b>
 * 
 * <p>
 * Currently supports all the types supported by {@linkplain BitBOABase}
 * </p>
 * 
 * <p>
 * <b>Parameters: </b>
 * <ul>
 * <li><b>quality measure</b> - the metrics which is used as a scoring function for Bayesian Network assessment</li>
 * <li><b>network search algorithm</b> - the algorithm (heuristic) which is used to find the satisfying Bayesian Network</li>
 * </ul>
 * </p>
 * 
 * <p>
 * Note that two parameters are complex tools which can have its various other tunable parameters.
 * </p>
 * 
 * <p>
 * <b>Default configuration: </b> <br>
 * Because of the fact that BOA algorithms are so tunable the default configuration is provided which falls back to algorithms:
 * 
 * <ul>
 * <li>Naive Search</li>
 * <li>Heckerman-Dirichlet Quality Measure</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class BitBOAUpdater extends BitBOABase implements IModelUpdater {

    private static final long serialVersionUID = 7867448501650524695L;

    public static final String P_QUALITY_ALGORITHM = "quality";
    public static final String P_SEARCH_ALGORITHM = "search";

    private QualityMeasure measure;
    private BayesianInducer inducer;

    private AttributeSpecs[] attributes;
    private int[][] samples;
    private int genomeSize;

    public static final Parameter U_GENOME_SIZE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(VectorSpecies.P_GENOMESIZE);

    @Override
    public void setup(EvolutionState state, Parameter base) {

        genomeSize = state.parameters.getInt(U_GENOME_SIZE, null, 2);

        if (genomeSize < 2) {
            state.output.fatal("The passed genome size cannot be lower than 2");
        }

        Parameter qualityParameter = base.push(P_QUALITY_ALGORITHM);

        try {
            measure = (QualityMeasure) state.parameters.getInstanceForParameter(qualityParameter, null, QualityMeasure.class);
        } catch (ParamClassLoadException e) {
            // this is normal case - fall back to the defaults
            measure = new QualityMeasureHGC();
        }

        Parameter inducerParameter = base.push(P_SEARCH_ALGORITHM);

        try {
            inducer = (BayesianInducer) state.parameters.getInstanceForParameter(inducerParameter, null, BayesianInducer.class);
        } catch (ParamClassLoadException e) {
            // fall back to the default
            inducer = new NaiveBayesInducer();
        }

        inducer.setQualityMeasure(measure);

        // prepare the attributes space this code is exactly the case
        // what makes the differencing the Bit and Short models from themselves
        attributes = JBNCUtil.prepareBinominalAttributes(genomeSize);

        // safe check
        state.output.exitIfErrors();
    }

    @Override
    public void retrieveModel(ModelSubpopulation modelSubpopulation, EvolutionState state) {

        // the data needs to be retrieved from samples and it may take some time
        Individual[] individuals = modelSubpopulation.individuals;
        samples = new int[individuals.length][genomeSize];
        for (int i = 0; i < individuals.length; i++) {
            BitVectorIndividual individual = (BitVectorIndividual) individuals[i];
            boolean[] genome = individual.genome;
            for (int j = 0; j < genomeSize; j++) {
                if (genome[j] == true) {
                    samples[i][j] = 1;
                } else {
                    samples[i][j] = 0;
                }

            }
        }
    }

    @Override
    public void applyModel(ModelSubpopulation modelSubpopulation, EvolutionState state) {

        BOAModel model = (BOAModel) modelSubpopulation.getModel();

        FrequencyCalc fCalc = JBNCUtil.prepareFrequencyCalc(samples, attributes);
        try {
            inducer.train(fCalc);
        } catch (Exception e) {
            throw new IllegalStateException("exception when training Bayesian Inducer in jBNC toolkit", e);
        }

        try {
            // NOTE: this kind of inducer replaces totally the network
            // which might be kind of
            BayesNet network = inducer.getNetwork();
            model.setNetwork(network);
        } catch (Exception e) {
            throw new IllegalStateException("expection when getting network from Bayesian Inducer in jBNC toolkit", e);
        }
    }

    public QualityMeasure getMeasure() {
        return measure;
    }

    public BayesianInducer getInducer() {
        return inducer;
    }

}
