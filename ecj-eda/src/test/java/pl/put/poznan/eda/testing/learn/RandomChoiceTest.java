package pl.put.poznan.eda.testing.learn;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import ec.util.RandomChoice;

/**
 * This is sort of learning test to make out the most of the {@linkplain RandomChoice} class.
 * 
 * @author Piotr Jessa
 * 
 */
public class RandomChoiceTest {

    private double[] probabilities;

    @Before
    public void set_up() {
        // show some probabilities
        probabilities = new double[5];
        probabilities[0] = 0.1;
        probabilities[1] = 0.3; // 0.4
        probabilities[2] = 0.5; // 0.9
        probabilities[3] = 0.05; // 0.95
        probabilities[4] = 0.05; // 1.0

        // this methods suck because it operates
        RandomChoice.organizeDistribution(probabilities);
    }

    @Test
    public void sample_distribution_using_ecj_1() {
        int index = RandomChoice.pickFromDistribution(probabilities, 0.67);
        Assert.assertEquals(2, index);
    }

    @Test
    public void sample_distribution_using_ecj_2() {

        int index = RandomChoice.pickFromDistribution(probabilities, 0.1);
        Assert.assertEquals(1, index);
    }

    @Test
    public void sample_distribution_using_ecj_3() {
        int index = RandomChoice.pickFromDistribution(probabilities, 0.99);
        Assert.assertEquals(4, index);
    }

    @Test
    public void sample_distribution_using_ecj_4() {
        int index = RandomChoice.pickFromDistribution(probabilities, 0.0);
        Assert.assertEquals(0, index);
    }

    @Test
    public void sample_distribution_using_ecj_5() {
        int index = RandomChoice.pickFromDistribution(probabilities, 1.0);
        Assert.assertEquals(4, index);
    }
}
