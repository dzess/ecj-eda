/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.graphs;

/**
 *  Description of the Class
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class Edge {

  //----------------------------------------------------------------------
  //
  // LOCAL
  //
  protected Vertex in = null;
  protected Vertex out = null;


  /** */
  Edge() {
    in = null;
    out = null;
  }


  /**
   * @param  x  Description of Parameter
   * @param  y  Description of Parameter
   */
  Edge(Vertex x, Vertex y) {
    in = x;
    out = y;
  }


  /**
   *  Gets the InVertex attribute of the Edge object
   *
   * @return    The InVertex value
   */
  public Vertex getInVertex() {
    return in;
  }


  /**
   *  Gets the OutVertex attribute of the Edge object
   *
   * @return    The OutVertex value
   */
  public Vertex getOutVertex() {
    return out;
  }


  /**  Description of the Method */
  public void invert() {
    Vertex tmp = in;
    in = out;
    out = tmp;
  }


  /**
   *  Description of the Method
   *
   * @return    Description of the Returned Value
   */
  public String toString() {
    String inS = (in == null) ? "null" : in.toString();
    String outS = (out == null) ? "null" : out.toString();

    String s = "(" + inS + "->" + outS + ")";
    return s;
  }

}
