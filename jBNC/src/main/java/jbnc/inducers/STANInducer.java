/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.inducers;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.util.BNTools;

import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 *  Constructs Selective TAN classifier (STAN).<br>
 *  <br>
 *
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class STANInducer extends DiscardingBayesianInducer {

  /**
   *  Used for testing of the STAN algorithm.
   *
   * @param  arg  The command line arguments
   */
  public static void main(String[] arg) {
    try {
      jbnc.util.Timer t = new jbnc.util.Timer();
      System.out.println("\nTesting class...\n");

      String fileStem = null;
      if (arg.length > 0) {
        fileStem = arg[0];
      } else {
//        fileStem = "..\\db\\vote";
        fileStem = "db_cv\\iris-0-1";
      }
//        fileStem = "..\\run\\F_SHORT_AXIS_AP_SEPT-0-0";

      System.out.println("Load dataset: " + fileStem);
      DatasetInt dataset = new DatasetInt();
      dataset.openC45(fileStem);
      dataset.discardAllOfType(AttributeType.IGNORE);
      jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(dataset);

      STANInducer stan = new STANInducer();
      stan.setQualityMeasure(new jbnc.measures.QualityMeasureLC());
      stan.setDebugMode(true);
//      stan.setDiscardNondependent(true);
      stan.train(fc);
      t.stop();

      BayesNet thisNet = stan.getNetwork();
      jbnc.util.BNCTester tester = new jbnc.util.BNCTester();
      jbnc.util.BNCTester.Result r = tester.test(thisNet, dataset);
      double ratio = r.nbPass / (double) (r.nbPass + r.nbFail);
      System.out.println("Accuracy on training set = " + (ratio * 100) + "%");
      t.println("\nExecution time = ");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   *  Construct STAN network from the training data
   *
   * @param  fc             Description of Parameter
   * @param  usePriors      Description of Parameter
   * @param  alpha_ijk      Description of Parameter
   * @exception  Exception  Description of Exception
   */
  public void train(jbnc.util.FrequencyCalc fc,
                    boolean usePriors,
                    double alpha_ijk) throws Exception {
    // Clean storage
    clean();

    // Some sanity checks
//    verifyDataset(dataset);

    //Make sure that usePriors and alphaK are consistent
    if (!usePriors) {
      alpha_ijk = 0;
    }

    // Check quality measure
    if (qualityMeasure == null) {
      throw new Exception("Network quality measure has not been assigned.");
    }

    qualityMeasure.setUsePriors(usePriors);
    qualityMeasure.setAlphaK(alpha_ijk);

    if (debugMode) {
      System.out.println("Attributes/Cases = "
          + (fc.names.length - 1) + "/"
          + fc.nbCases);
    }
    jbnc.operators.SAN san = new jbnc.operators.SAN();
    jbnc.operators.TreeAugmenter ta = new jbnc.operators.TreeAugmenter();

    san.setDiscardNonDependent(discardNonDependent);
    san.setDebugMode(debugMode);

    net = san.train(fc, qualityMeasure, ta, usePriors, alpha_ijk);
    BNTools.learnParameters(net, fc, usePriors, alpha_ijk);

    if (debugMode) {
      FileOutputStream out = new FileOutputStream("STAN_dump.bif");
      PrintStream pOut = new PrintStream(out);
      net.save_bif(pOut);
    }
  }
}
