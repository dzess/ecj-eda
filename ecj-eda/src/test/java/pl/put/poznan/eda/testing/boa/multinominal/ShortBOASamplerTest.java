package pl.put.poznan.eda.testing.boa.multinominal;

import java.util.List;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeSpecs;
import jbnc.inducers.NaiveBayesInducer;
import jbnc.measures.QualityMeasureHGC;
import jbnc.util.FrequencyCalc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.multinominal.ShortBOAModel;
import pl.put.poznan.eda.boa.multinominal.ShortBOASampler;
import pl.put.poznan.eda.boa.multinominal.ShortBOAUpdater;
import pl.put.poznan.eda.testing.ECJSamplerTestingBase;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import ec.Individual;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;

public class ShortBOASamplerTest extends ECJSamplerTestingBase {

    private ShortBOASampler sampler;
    private int genomeSize;
    private int individualsSize;

    @Before
    public void set_up() {

        this.setUpBase();
        this.setUpEvolutionStateBasic();

        distributionMock = this.prepareDistributionSampler();
        sampler = new ShortBOASampler(distributionMock);

    }

    private void prepareForSampling(Model model) throws Exception {

        // needed for sampling testing
        individualsSize = 2;

        this.prepareSubpopulation(individualsSize, model, ShortVectorIndividual.class);

        ShortVectorIndividual ind1 = new ShortVectorIndividual();
        ShortVectorIndividual ind2 = new ShortVectorIndividual();

        ind1.genome = new short[genomeSize];
        ind2.genome = new short[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(ShortVectorIndividual.class);
    }

    private BOAModel prepareModel(BayesNet net) {
        BOAModel model = Mockito.mock(BitBOAModel.class);
        Mockito.when(model.getGenomeSize()).thenReturn(genomeSize);
        Mockito.when(model.getNetwork()).thenReturn(net);
        return model;
    }

    @Test
    public void given_bayes_net_sampling_is_done_properly_1() throws Exception {

        genomeSize = 3;

        // code needed by the setup method of the BOAModel
        ShortVectorIndividual individual = new ShortVectorIndividual();
        individual.setGenome(new short[1]);
        individual.setGenomeLength(genomeSize);

        Parameter individualParameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(individualParameter, individual);

        Parameter genomeSizeParameter = BOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(genomeSizeParameter, genomeSize);

        // mark the one with max and min genes ;)
        int maxGene = 1; // true
        int minGene = 0; // false
        this.generateGetIntForParameterWithDefault(ShortBOAUpdater.U_MAX_GENE, maxGene);
        this.generateGetIntForParameterWithDefault(ShortBOAUpdater.U_MIN_GENE, minGene);

        // create the BayesNet easy model (which will be easy to sample)
        // preferable with no linkage at all - such network is created by the BOAModel at first
        // each probability of each value should be 0.5
        BOAModel model = new ShortBOAModel();
        model.setup(state, base);

        this.prepareDistribution(0.4);
        this.prepareForSampling(model);

        // sampler.sampleModel(newSubpopulation, oldSubpopulation, state);
        List<Individual> newInds = sampler.sampleModel(model, individualsSize, species, state);

        // write the assertion which are needed
        Assert.assertEquals(individualsSize, newInds.size());
        for (int i = 0; i < individualsSize; i++) {
            ShortVectorIndividual ind = (ShortVectorIndividual) newInds.get(i);

            // given the distribution returns 0.4 and binary variable has exceeding point (to FALSE)
            for (int j = 0; j < genomeSize; j++) {
                // assertion should return the same as were bit boa modeling done
                Assert.assertEquals(0, ind.genome[j]);
            }
        }
    }

    @Ignore(value = "not yet implemented")
    public void given_bayes_net_sampling_is_done_properly_2() {
        Assert.fail("not yet implemented");
        // FIXME: write maybe some more elaborate integration test of ShortBOASampler
        // now i think after though this i crucial to make this working
    }

    @Test
    public void sampler_works_fine_on_the_data_from_real_learning() throws Exception {
        // this is integration level testing using some inducers from the jBNC
        // note that this example is copier from learn tests in the basic
        // jBNC testing suite
        QualityMeasureHGC measure = new QualityMeasureHGC();
        NaiveBayesInducer inducer = new NaiveBayesInducer();
        inducer.setQualityMeasure(measure);

        // use bigger cases to show some better works
        genomeSize = 3;

        int casesLength = 3;

        int min = 0;
        int max = 3;

        // prepare the attributes
        // (from 1 to 4: {1,2,3,4} for example instruction set of 4 elements)
        AttributeSpecs[] attributes = JBNCUtil.prepareMultinominalAttributesSpace(genomeSize, min, max);

        // prepare the data
        int[][] data = new int[casesLength][genomeSize];

        data[0][0] = 0;
        data[0][1] = 2;
        data[0][2] = 2;

        data[1][0] = 1;
        data[1][1] = 3;
        data[1][2] = 3;

        data[2][0] = 1;
        data[2][1] = 0;
        data[2][2] = 0;

        FrequencyCalc fCalc = JBNCUtil.prepareFrequencyCalc(data, attributes);
        inducer.train(fCalc);
        BayesNet network = inducer.getNetwork();

        // print this network into standard output
        network.print(System.out);

        BOAModel model = this.prepareModel(network);
        this.prepareDistribution(0.25);

        // needed for sampling testing
        individualsSize = 2;

        this.prepareSubpopulation(individualsSize, model, ShortVectorIndividual.class);

        ShortVectorIndividual ind1 = new ShortVectorIndividual();
        ShortVectorIndividual ind2 = new ShortVectorIndividual();

        ind1.genome = new short[genomeSize];
        ind2.genome = new short[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(ShortVectorIndividual.class);

        // act
        List<Individual> newInds = sampler.sampleModel(model, individualsSize, species, state);

        // assert
        Assert.assertEquals(individualsSize, newInds.size());
        for (int i = 0; i < individualsSize; i++) {
            ShortVectorIndividual ind = (ShortVectorIndividual) newInds.get(i);
            // expected genome with probing of 0.25
            // should be the ['1','0','0'] sequence
            Assert.assertEquals(1, ind.genome[0]);
            Assert.assertEquals(0, ind.genome[1]);
            Assert.assertEquals(0, ind.genome[2]);
        }
    }

    @Test
    public void sampling_works_with_short_vector() {
        Assert.assertTrue(sampler.matchIndividualType(ShortVectorIndividual.class));

        // other types are not supported officially yet
        Assert.assertFalse(sampler.matchIndividualType(BitVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(ByteVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(IntegerVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(FloatVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(DoubleVectorIndividual.class));

    }

    @Test
    public void sampling_works_only_with_boa_model() {
        Assert.assertTrue(sampler.matchModelType(ShortBOAModel.class));
        // other models are not supported
        Assert.assertFalse(sampler.matchModelType(DenseUMDAModel.class));
        Assert.assertFalse(sampler.matchModelType(BitBOAModel.class));
    }
}
