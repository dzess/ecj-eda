package pl.put.poznan.eda.boa;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.multinominal.ShortBOAModel;
import ec.EvolutionState;
import ec.Individual;
import ec.Initializer;
import ec.Population;
import ec.Subpopulation;
import ec.util.Parameter;
import ec.vector.VectorSpecies;

/**
 * Base class for the BOA implementation algorithms:
 * 
 * <ul>
 * <li>{@linkplain BitBOAModel}</li>
 * <li>{@linkplain ShortBOAModel}</li>
 * </ul>
 * 
 * Holds most of the logic except creating of sample networks and validation.
 * 
 * <p>
 * Parameters starting with <i>U</i> are {@linkplain Parameter} used by the setup method of the {@linkplain BitBOAModel}.
 * </p>
 * 
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public abstract class BOAModel extends Model {

    private static final long serialVersionUID = 3729915410204571389L;

    protected BayesNet network;
    protected int genomeSize;
    protected Individual individual;

    public static final Parameter U_INDIVIDUAL = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(VectorSpecies.P_INDIVIDUAL);

    public static final Parameter U_GENOME_SIZE = new Parameter(Initializer.P_POP).push(Population.P_SUBPOP).push("0")
            .push(Subpopulation.P_SPECIES).push(VectorSpecies.P_GENOMESIZE);

    public BayesNet getNetwork() {
        return network;
    }

    public void setNetwork(BayesNet network) {
        this.network = network;
    }

    public int getGenomeSize() {
        return genomeSize;
    }

    public Individual getIndividual() {
        return individual;
    }

    public BOAModel() {
        super();
    }

    /**
     * This method is to be overridden in child class to differentiate between binomial and multinomial models.
     */
    protected abstract void generateBayesNet();

    /**
     * Validates the individual if it is of correct type
     */
    protected abstract boolean isIndividualValid();

    @Override
    public void setup(EvolutionState state, Parameter base) {

        genomeSize = state.parameters.getInt(U_GENOME_SIZE, null, 2);
        individual = (Individual) state.parameters.getInstanceForParameter(U_INDIVIDUAL, null, Individual.class);

        // validation of the genome size etc
        if (genomeSize < 2) {
            state.output.fatal("The passed genome size cannot be lower than 2");
        }
        if (individual == null) {
            state.output.fatal("The individual is needed for safety checking");
        }

        // validate that only the bit vector individual should be used
        if (isIndividualValid()) {
            state.output.fatal("Passed Vector Individual is not the bit vector one");
        }

        // create the network which is empty for now
        generateBayesNet();

        // check after creation if model is valid
        validate();

        // safe check
        state.output.exitIfErrors();
    }

    @Override
    public void validate() {

        // the probabilities on the functions must comply with probability theorem
        ProbabilityFunction[] functions = network.get_probability_functions();
        for (int i = 0; i < functions.length; i++) {
            ProbabilityFunction function = functions[i];
            int variablesCount = function.get_variables().length;

            // the at least one variable is needed
            if (variablesCount < 1) {
                throw new ArithmeticException("Each node in Bayes Net needs the at least one variable");
            }

            // check each value being 0<=x<=1
            for (int j = 0; j < function.get_values().length; j++) {
                if (function.get_value(j) < 0.0) {
                    throw new ArithmeticException("The probability cannot be lower than 0");
                }

                if (function.get_value(j) > 1.0) {
                    throw new ArithmeticException("The probability cannot be higher than 1");
                }
            }

            // TODO: check summing up to the 1
            // this is quite the though question because of the weird format used in BayesNets
        }
    }

}