package pl.put.poznan.eda.boa.util.resolver;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;
import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import pl.put.poznan.eda.boa.util.graph.GraphTuple;

/**
 * Interface to make getting value from the {@linkplain BayesNet} for common case to more efficient cases using specialized algorithms.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public interface IIndexResolver<T> {

    /**
     * Gets vector of probabilities for current tuple, namely the {@linkplain ProbabilityFunction} basing on already provided evidence.
     * 
     * <p>
     * Evidence is provided <b>inside</b> {@linkplain GraphNetwork} with some node with already evaluated state and their value.
     * </p>
     * 
     * <p>
     * The variable about which there is a query is the first variable of {@linkplain ProbabilityFunction}.
     * </p>
     * 
     * <p>
     * Example: <br/>
     * <code>
     *      node A: P(A|B,C). Nodes B,C <b>has to</b> be already evaluated. This method will extract from {@linkplain BayesNet} probabilities
     *      which describe all possible states of <b>A</b> basing on values from B and C.
     * </code>
     * </p>
     * 
     * @param tuple
     *            : tuple with node (variable) about which there is a query
     * @param graph
     *            : the structure of the {@linkplain BayesNet} in a form of the {@linkplain GraphNetwork}
     * @return probabilities for current base variable basing on passed evidence.
     */
    public double[] getProbabilitesGivenEvidance(GraphTuple<T> tuple, GraphNetwork graph);

    /**
     * Retries the value of {@linkplain ProbabilityVariable} from node passed in a last method.
     * 
     * @param sampledIndex
     *            : the index of the sampled value
     * @return the sampled valued
     */
    public T getValueGivenEvidance(int sampledIndex);

    /**
     * Clear any internal state of the {@linkplain IIndexResolver}, to avoid reallocation of memory.
     */
    public void reset();
}
