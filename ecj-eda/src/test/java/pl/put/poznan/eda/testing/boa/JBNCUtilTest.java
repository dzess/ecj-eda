package pl.put.poznan.eda.testing.boa;

import jbnc.dataset.AttributeSpecs;

import org.junit.Assert;
import org.junit.Test;

import pl.put.poznan.eda.boa.JBNCUtil;

// FIXME: this class NEEDs testing etremly
public class JBNCUtilTest {

    private int variables;
    private int min;
    private int max;
    private AttributeSpecs[] attrs;

    @Test
    public void creating_mulitnominal_attributes_creates_properly_1() {

        // testing case one
        variables = 3;
        min = 1;
        max = 4;
        // states {1,2,3,4}

        attrs = JBNCUtil.prepareMultinominalAttributesSpace(variables, min, max);

        Assert.assertEquals(variables, attrs.length);
        for (AttributeSpecs spec : attrs) {
            String[] states = spec.getStates();
            Assert.assertEquals(4, states.length);
            for (String state : states) {
                // this code should not throw an exception
                Short.parseShort(state);
            }
        }
    }

    @Test
    public void creating_mulitnominal_attributes_creates_properly_2() {

        // testing case one
        variables = 2;
        min = -2;
        max = 3;
        // states: {-2,-1,0,1,2,3}

        attrs = JBNCUtil.prepareMultinominalAttributesSpace(variables, min, max);

        Assert.assertEquals(variables, attrs.length);
        for (AttributeSpecs spec : attrs) {
            String[] states = spec.getStates();
            Assert.assertEquals(6, states.length);
            for (String state : states) {
                // this code should not throw an exception
                Short.parseShort(state);
            }
        }
    }

    @Test
    public void intergation_between_preprare_specs_and_calculate_freq_1() {
        int genomeSize = 4;
        int cases = 3;

        // prepare the attributes
        AttributeSpecs[] attributes = JBNCUtil.prepareBinominalAttributes(genomeSize);

        // prepare the data
        int[][] data = new int[cases][genomeSize];

        data[0][0] = 0;
        data[0][1] = 1;
        data[0][2] = 1;
        data[0][3] = 1;

        data[1][0] = 1;
        data[1][1] = 1;
        data[1][2] = 1;
        data[1][3] = 1;

        data[2][0] = 1;
        data[2][1] = 1;
        data[2][2] = 1;
        data[2][3] = 1;

        JBNCUtil.prepareFrequencyCalc(data, attributes);
    }

    @Test
    public void integration_between_prepare_specs_and_calculate_freq_2() {
        variables = 3;

        // min and max describe possible variables
        // NOTE: the jBNC toolkit is so stupid it works only with the
        // integers indexed from the 0 !!!! up to some number
        min = 0;
        max = 3;
        attrs = JBNCUtil.prepareMultinominalAttributesSpace(variables, min, max);

        int[][] data = new int[2][variables];

        data[0][0] = 0;
        data[0][1] = 2;
        data[0][2] = 2;

        data[1][0] = 1;
        data[1][1] = 3;
        data[1][2] = 2;

        // should not throw an exception
        JBNCUtil.prepareFrequencyCalc(data, attrs);
    }
}
