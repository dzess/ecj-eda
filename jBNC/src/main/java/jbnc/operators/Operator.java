/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.operators;

/**
 *  Abstract BNC Augment operator.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class Operator {
  protected boolean debugMode = false;


  /**
   *  Sets the DebugMode attribute of the Operator object
   *
   * @param  debugMode  The new DebugMode value
   */
  public void setDebugMode(boolean debugMode) {
    this.debugMode = debugMode;
  }


  /**
   *  Gets the DebugMode attribute of the Operator object
   *
   * @return    The DebugMode value
   */
  public boolean getDebugMode() {
    return this.debugMode;
  }
}
