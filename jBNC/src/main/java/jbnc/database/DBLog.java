/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *  Enables loging of testing results to a database.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class DBLog {

  private String dbURL = null;
  private String user = null;
  private String password = null;
  private Connection dbConnection = null;
  private Statement dbStatement = null;


  /**
   *  The main program for the DBLog class
   *
   * @param  args  The command line arguments
   */
  public static void main(String[] args) {

    try {
      DBLog dbLog = new DBLog();

      dbLog.open("jdbc:odbc:JTest", "", "");

      dbLog.insert("APEX", "apex_F_circle_C2", "FAN-HGC", 0.9, 0.12);
      dbLog.insert("APEX", "apex_M_circle_C2", "FAN-SB", 0.87, 0.32);

      dbLog.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   *  Attempts to establish a connection to the given database URL.
   *
   * @param  dbURL          a database url of the form jdbc:<i>subprotocol</i> :
   *      <i>subname</i>
   * @param  user           the database user on whose behalf the Connection is
   *      being made
   * @param  password       the user's password
   * @exception  Exception  if a database access error occurs
   */
  public void open(String dbURL, String user, String password) throws Exception {
    // Load the driver (registers itself)
    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    dbConnection = DriverManager.getConnection(dbURL, user, password);
    dbStatement = dbConnection.createStatement();
  }


  /**
   *  Closes connection to a database.
   *
   * @exception  Exception  if a database access error occurs
   */
  public void close() throws Exception {
    dbStatement.close();
    dbStatement = null;
    dbConnection.close();
    dbConnection = null;
  }


  /**
   *  Inserts a results into a two result tables. The first table name is <i>
   *  table</i> _ERROR, the second is <i>table</i> _STDMEAN.
   *
   * @param  table          table prefix.
   * @param  dataset        name of the dataset for which the results are
   *      logged.
   * @param  algorithm      name of the algorithm used for tests.
   * @param  error          value of the error.
   * @param  stdMean        value of the standard deviation of mean of error.
   * @exception  Exception  if a database access error occurs
   */
  public void insert(String table, String dataset, String algorithm,
                     double error, double stdMean) throws Exception {
    insertExact(table + "_Error", dataset, algorithm, error);
    insertExact(table + "_StdMean", dataset, algorithm, stdMean);

  }


  /**
   *  Inserts a value into a names table.
   *
   * @param  table          exact table name.
   * @param  dataset        name of the dataset for which the results are
   *      logged.
   * @param  algorithm      name of the algorithm used for tests.
   * @param  val            value.
   * @exception  Exception  if a database access error occurs
   */
  private void insertExact(String table, String dataset, String algorithm,
                           double val) throws Exception {
    String anSql = "update [" + table + "] " +
        "set [" + algorithm + "]=" + val + " " +
        "where [DATASET]" + "='" + dataset + "'";
//    System.out.println("SQL: "+anSql);
    int r = dbStatement.executeUpdate(anSql);
    if (r == 0) {
      throw new Exception("Insert into " + table + " did not update anything.");
    }
  }

}
