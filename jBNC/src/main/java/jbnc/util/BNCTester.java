/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.util;

import gnu.getopt.Getopt;
import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.InferenceGraphs.InferenceGraph;
import jb.InferenceGraphs.InferenceGraphNode;
import jb.QuasiBayesianInferences.QBInference;
import jbnc.dataset.AttributeType;
import jbnc.dataset.Dataset;
import jbnc.dataset.DatasetReader;
import jbnc.dataset.NamesReader;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Vector;

/**
 *  Description of the Class
 *
 * @author     Jarek Sacha
 * @since
 */
public class BNCTester {

    /*
     *  Private and protected constants
     */
    protected final static String classNameDefault = "class";

    /*
     *  Private and protected member variables
     */
    private final static String programName = "BNCTester";
    private final static String programVersion = "v.0.02";

    // Command line configuration parameters
    protected boolean useTimer = false;
    protected boolean debugMode = false;
    protected String dataFileName = null;
    protected String namesFileName = null;
    protected String netFileName = null;
    protected String className = null;

    // Test data set
    protected Dataset dataset;

    // Network representation
    protected InferenceGraph graph;
    // Graph representing the network
    protected InferenceGraphNode[] attribNodes;
    // References to graph nodes, order as in cases
    protected InferenceGraphNode classNode;
    // Reference to class node in the graph

    // Classification statistics
    protected int nbPass;
    protected int nbFail;


    /**
     * @param  argv  Description of Parameter
     */
    public static void main(String argv[]) {
        String usageMsg = "\n"
                + "USAGE: BNCTester <options> network_file [class_name]\n"
                + "\n"
                + "where:\n"
                + "  -d              Print debugging information.\n"
                + "  -f <file_stem>  Load test data in C4.5 format (.names + .test),\n"
                + "                  file_stem.names - file with specification of attributes,\n"
                + "                  file_stem.test  - file with test cases.\n"
                + "  -h <test_data>  Load test data in comma delimited format. First row contains\n"
                + "                  attribute names.\n"
                + "  -t              Print execution time."
                + "\n"
                + "  network_file    Classifier network definition file.\n"
                + "  class_name      Name of the class variable. The default value is 'class'.\n"
                + "\n"
                + "EXAMPLE: BNCTester -tf monk1 monk1.bif";

        try {
            Timer t = new Timer();

            // Print header
            System.out.println("\nTester for Bayesian network classifiers.\n");

            BNCTester tester = new BNCTester();

            // Process command line arguments
            if (!tester.processCommandLine(argv)) {
                System.out.println(usageMsg);
                return;
            }

            // Run the test
            tester.run();

            // Print execution time
            if (tester.useTimer) {
                t.println("\nExecution time = ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     *  Sets the DebugMode attribute of the BNCTester object
     *
     * @param  mode  The new DebugMode value
     */
    public void setDebugMode(boolean mode) {
        debugMode = mode;
    }


    /**
     * @param  net            Description of Parameter
     * @param  dataset        Description of Parameter
     * @return                Confusion matrix.
     * @exception  Exception  Description of Exception
     */
    public Result test(BayesNet net, Dataset dataset) throws Exception {
        clear();

        this.graph = null;
        this.dataset = dataset;
        this.className = null;

//    verifyDatasetVsNetwork();

        testClassifier(net);

        if (debugMode) {
            printClassificationReport();
        }

        return new Result(nbPass, nbFail);
    }


    /**
     * @param  net            Description of Parameter
     * @param  dataset        Description of Parameter
     * @return                Confusion matrix.
     * @exception  Exception  Description of Exception
     */
    public Result test_old(BayesNet net, Dataset dataset) throws Exception {
        return test_old(new InferenceGraph(net), dataset);
    }


    /**
     * @param  graph          Description of Parameter
     * @param  dataset        Description of Parameter
     * @return                Confusion matrix.
     * @exception  Exception  Description of Exception
     */
    public Result test_old(InferenceGraph graph, Dataset dataset) throws Exception {
        clear();

        this.graph = graph;
        this.dataset = dataset;
        this.className = dataset.names[dataset.names.length - 1].getName();

        verifyDatasetVsNetwork();

        testClassifier_old();

        if (debugMode) {
            printClassificationReport();
        }

        return new Result(nbPass, nbFail);
    }


    /**  Print test results to the standard output. */
    public void report() {
        printClassificationReport();
    }


    /**
     *  Print test results to the standard output in the short form (single line).
     *
     * @exception  Exception  Description of Exception
     */
    public void reportShort() throws Exception {
        Result r = new Result(nbPass, nbFail);
        r.reportShort();
    }


    /**
     *  Process command line arguments.
     *
     * @param  argv  Description of Parameter
     * @return       'false' when command line is invalid.
     */
    protected boolean processCommandLine(String[] argv) {

        try {
            Getopt g = new Getopt("ERROR", argv, "df:h:t");

            int c;
            String arg = null;
            boolean is_f = false;
            boolean is_h = false;
            while ((c = g.getopt()) != -1) {
                switch (c) {
                    case 'd':
                        debugMode = true;
                        break;
                    case 'f':
                        is_f = true;
                        arg = g.getOptarg();
                        dataFileName = arg + ".test";
                        namesFileName = arg + ".names";
                        break;
                    case 'h':
                        is_h = true;
                        dataFileName = g.getOptarg();
                        break;
                    case 't':
                        useTimer = true;
                        break;
                    case '?':
                        break;
                    default:
                        System.out.println("\nERROR: getopt() returned: " + c);
                        return false;
                }
            }

            if (is_f && is_h) {
                System.out.println("\nERROR: Options 'f' and 'h' cannot appear together.");
                return false;
            }

            if (!is_f && !is_h) {
                System.out.println("\nERROR: Either option 'f' or 'h' has to be specified.");
                return false;
            }

            // Classifier network file name
            int index = g.getOptind();
            if (index >= argv.length) {
                System.out.println("\nERROR: Classifier network file not specified.");
                return false;
            }
            netFileName = argv[index];

            // Class name
            ++index;
            if (index < argv.length) {
                className = argv[index];
            } else {
                className = classNameDefault;
            }

            if (debugMode) {
                System.out.println("debug mode   = " + debugMode);
                System.out.println("use timer    = " + useTimer);
                System.out.println("data file    = " + dataFileName);
                System.out.println("names file   = " + namesFileName);
                System.out.println("network file = " + netFileName);
                System.out.println("class name   = " + className);
            }

            ++index;
            if (index >= argv.length) {
                return true;
            }

            // Staff left on the command line
            System.out.print("\nERROR: Too many arguments in the command line. Extraneous: ");
            for (; index < argv.length; ++index) {
                System.out.print(argv[index] + ", ");
            }

            System.out.println("");
        } catch (Exception e) {
            System.out.println("\nError: " + e);
            if (debugMode) {
                e.printStackTrace();
            }
        }

        return false;
    }


    /*
     *
     */
    protected void run() {
        try {
            Timer tLoadNet = new Timer();
            loadNetwork();
            tLoadNet.stop();

            Timer tLoadData = new Timer();
            loadDataset();
            tLoadData.stop();

//      Timer tTest = new Timer();
//      test(graph, dataset);
//      tTest.stop();

            Timer tVerify = new Timer();
            verifyDatasetVsNetwork();
            tVerify.stop();

            Timer tClassify = new Timer();
            testClassifier_old();
//      testClassifier();
            tClassify.stop();

            Timer tReport = new Timer();
            printClassificationReport();
            tReport.stop();

            if (useTimer) {
                tLoadNet.println("\nLoad net  = ");
                tLoadData.println("Load data = ");
//        tTest.println(     "Test      = ");
                tVerify.println("Verify    = ");
                tClassify.println("Classify  = ");
                tReport.println("Report    = ");
            }
        } catch (Exception e) {
            System.out.println("\nError: " + e);
            if (debugMode) {
                e.printStackTrace();
            }
        }
    }


    /*
     *
     */
    protected void loadNetwork() throws Exception {
        if (debugMode) {
            System.out.println("\nLoading network: " + netFileName);
        }

        if (netFileName == null) {
            throw new Exception("Name of the network file is not set.");
        }

        // Load network
        graph = new InferenceGraph(netFileName);

        if (debugMode) {
            Vector graphNodes = graph.get_nodes();
            System.out.println("Number of nodes = " + graphNodes.size());
            System.out.print("Node names: ");
            for (int i = 0; i < graphNodes.size(); ++i) {
                System.out.print(((InferenceGraphNode) graphNodes.get(i)).get_name() + " ");
            }
            System.out.println("");
        }
    }


    /*
     *
     */
    protected void loadDataset() throws Exception {
        if (namesFileName != null) {
            // Load data in C4.5 format

            dataset = new Dataset();

            // Load names file
            if (debugMode) {
                System.out.println("\nLoading names file: " + namesFileName);
            }
            dataset.names = NamesReader.open(namesFileName);

            if (dataset.names == null || dataset.names.length == 0) {
                throw new Exception("'.names' is empty");
            }

            int nbAttributes = dataset.names.length - 1;
            int nbClasses = dataset.names[nbAttributes].getStates().length;

            if (debugMode) {
//        System.out.println("Number of classes    = "+nbClasses);
                System.out.println("Number of attributes = " + nbAttributes + " + one class.");
            }

            // Verify that least one attribute is present and that it is discrete
            int nbIgnored = 0;
            for (int i = 0; i < nbAttributes; ++i) {
                AttributeType type = dataset.names[i].getType();
                if (type == AttributeType.CONTINUOUS) {
                    throw new Exception("Attribute '"
                            + dataset.names[i].getName()
                            + "' is continuous.");
                } else if (type == AttributeType.IGNORE) {
                    ++nbIgnored;
                }
            }

            if (debugMode) {
                System.out.println("Number of ignored attributes = " + nbIgnored + ".");
            }

            if (nbIgnored == nbAttributes) {
                throw new Exception("All attributes are specified to be ignored by the '.names' file.");
            }

            // Read test data file
            if (debugMode) {
                System.out.println("\nLoading test data file: " + dataFileName);
            }
            DatasetReader datasetReader = new DatasetReader();
            datasetReader.setDiscardIncompleteCases(true);
            dataset.cases = datasetReader.open(dataFileName, dataset.names);

            if (debugMode) {
                System.out.println("Number of cases = " + dataset.cases.size());
            }
        } else {
            // Load data in from comma delimited file with a header

            DatasetReader datasetReader = new DatasetReader();
            datasetReader.setDiscardIncompleteCases(true);
            dataset = datasetReader.open(dataFileName, className);

            if (dataset == null) {
                throw new Exception("Data set is empty.");
            }
        }
    }


    /*
     *
     */
    protected void verifyDatasetVsNetwork() throws Exception {
        // Create reverse lookup for nodes and find the class node
        classNode = null;
        HashMap nodesHashMap = new HashMap();
        Vector graphNodes = graph.get_nodes();
        for (int i = 0; i < graphNodes.size(); ++i) {
            InferenceGraphNode node = (InferenceGraphNode) graphNodes.get(i);
            String nodeName = node.get_name();
            if (classNode == null && className.equals(nodeName)) {
                classNode = node;
            } else {
                nodesHashMap.put(nodeName, node);
            }
        }

        if (classNode == null) {
            throw new Exception("The network does not have node with class name '"
                    + className + "'.");
        }

        // Check names vs. network
        int nbAttributes = dataset.names.length - 1;
        attribNodes = new InferenceGraphNode[nbAttributes];
        for (int i = 0; i < nbAttributes; i++) {
            if (dataset.names[i].getType() != AttributeType.IGNORE) {
                InferenceGraphNode n = (InferenceGraphNode) nodesHashMap.get(
                        dataset.names[i].getName());
                if (n == null) {
                    throw new Exception("The network does not have node with attribute name '"
                            + dataset.names[i].getName() + "'");
                }
                attribNodes[i] = n;
            } else {
                attribNodes[i] = null;
            }
        }
    }


    /**
     * @param  net            Description of Parameter
     * @exception  Exception  Description of Exception
     */
    protected void testClassifier(BayesNet net) throws Exception {
        int nbVars = dataset.names.length;
        int nbAttrib = nbVars - 1;
        int[] caseIndexes = new int[nbVars];
        jbnc.graphs.BNCInference inference = new jbnc.graphs.BNCInference(net);

        // Test each case
        nbPass = 0;
        nbFail = 0;
        for (int caseNb = 0; caseNb < dataset.cases.size(); ++caseNb) {
            // Set node values
            int[] thisCase = (int[]) dataset.cases.get(caseNb);
            for (int j = 0; j < nbAttrib; ++j) {
                caseIndexes[j] = thisCase[j];
            }

            // Do inference
            double[] classProb = inference.getCondClassProb(caseIndexes);

            // Find the most probable classification index
            int maxIndex = -1;
            double maxPr = -1;
            for (int j = 0; j < classProb.length; ++j) {
                if (classProb[j] > maxPr) {
                    maxPr = classProb[j];
                    maxIndex = j;
                }
            }

            // Record result
            int trueClassIndex = thisCase[nbAttrib];
            if (maxIndex == trueClassIndex) {
                ++nbPass;
            } else {
                ++nbFail;
            }
            /*
             *  System.out.print(caseNb+": ( ");
             *  for(int i=0; i<classProb.length; ++i)
             *  System.out.print(classProb[i]+" ");
             *  System.out.println(") " + (maxIndex == trueClassIndex) );
             */
        }
    }


    /*
     *
     */
    protected void testClassifier_old() throws Exception {
        int nbAttributes = dataset.names.length - 1;

        boolean do_produce_clusters = false;
        QBInference inference = new QBInference(graph.get_bayes_net(), do_produce_clusters);

        // Test each case
        nbPass = 0;
        nbFail = 0;
        for (int caseNb = 0; caseNb < dataset.cases.size(); ++caseNb) {
            // Find index for current cases class

            // Set node values
            Vector thisCase = (Vector) dataset.cases.get(caseNb);
            for (int j = 0; j < nbAttributes; ++j) {
                InferenceGraphNode thisNode = attribNodes[j];
                if (thisNode != null) {
                    // set value
                    int observedIndex = ((Integer) thisCase.get(j)).intValue();
                    String observedValue = dataset.names[j].getState(observedIndex);
                    thisNode.set_observation_value(observedValue);

                    // verify that the value was actually set
                    int v = thisNode.get_observed_value();
                    if (v >= 0) {
                        String[] setValues = thisNode.get_values();
                        if (!observedValue.equals(setValues[v])) {
                            throw new Exception("Mishmash in setting observation value: "
                                    + "case #" + (caseNb + 1)
                                    + ", attribute #" + (j + 1)
                                    + "\n       Got '" + setValues[v]
                                    + "' instead of requested " + observedValue + ".");
                        }
                    } else {
                        throw new Exception("Failed to set observation value '"
                                + observedValue
                                + "', case #" + (caseNb + 1)
                                + ", attribute #" + (j + 1));
                    }
                }
            }

            // Do inference
            inference.inference(className);
            ProbabilityFunction prFunction = inference.get_result();

            // Find the most probable classification index
            int maxIndex = -1;
            double maxPr = -1;
            double[] pr = prFunction.get_values();
            for (int j = 0; j < pr.length; ++j) {
                if (pr[j] > maxPr) {
                    maxPr = pr[j];
                    maxIndex = j;
                }
            }

            // Get name of most probable class
            String classValue = prFunction.get_variable(0).get_value(maxIndex);

            // Record result
            int trueClassIndex = ((Integer) thisCase.get(nbAttributes)).intValue();
            String trueClassValue = dataset.names[nbAttributes].getState(trueClassIndex);
            boolean pass = trueClassValue.equals(classValue);
            if (pass) {
                ++nbPass;
            } else {
                ++nbFail;
            }

            if (debugMode) {
                System.out.print(caseNb + ": ( ");
                for (int i = 0; i < pr.length; ++i) {
                    System.out.print(pr[i] + " ");
                }
                System.out.println(") " + pass);
            }

        }
    }


    /*
     *
     */
    protected void printClassificationReport() {
        // Print report
        System.out.println("");
        System.out.println("Pass  = " + nbPass);
        System.out.println("Fail  = " + nbFail);
        int total = nbPass + nbFail;
        System.out.println("Total = " + total);
        if (total != 0) {
            NumberFormat f = new DecimalFormat("0.###%");

            double err = (double) (nbFail) / total;
            System.out.println("Error    = " + f.format(err));

            double acc = (double) (nbPass) / total;
            System.out.println("Accuracy = " + f.format(acc));
        }
    }


    /** */
    protected void clear() {
        useTimer = false;
        debugMode = false;
        dataFileName = null;
        namesFileName = null;
        netFileName = null;
        className = null;

        dataset = null;

        graph = null;
        attribNodes = null;
        classNode = null;

        nbPass = 0;
        nbFail = 0;
    }


    public class Result {
        public int nbPass;
        public int nbFail;


        /**
         *  Constructor for the Result object
         *
         * @param  pass  Description of Parameter
         * @param  fail  Description of Parameter
         */
        public Result(int pass, int fail) {
            nbPass = pass;
            nbFail = fail;
        }


        public double getError() throws Exception {
            int tot = nbPass + nbFail;
            if (tot == 0) {
                throw new Exception("pass + fail == zero (division by zero)");
            }

            return nbFail / (double) tot;
        }


        public double getAccuracy() throws Exception {
            int tot = nbPass + nbFail;
            if (tot == 0) {
                throw new Exception("pass + fail == zero (division by zero)");
            }

            return nbPass / (double) tot;
        }


        /*
         *  Compute the estimated standard deviation according to
         *  the binomial model, which assumes every test instance is
         *  a Bernoulli trial, thus std-dev=sqrt(error*(1-error)/(n-1))
         */
        public double getTheoreticalStdDev() throws Exception {
            int tot = nbPass + nbFail;
            if (tot < 2) {
                throw new Exception("pass + fail < 2");
            }

            double err = nbFail / tot;

            return Math.sqrt((double) nbFail * (double) nbPass
                    / ((double) tot * (double) tot * (double) (tot - 1)));
        }


        /**
         *  Print test results to the standard output in the short form (single
         *  line).
         *
         * @exception  Exception  Description of Exception
         */
        public void reportShort() throws Exception {
            // Print report
            int total = nbPass + nbFail;
            if (total != 0) {
                NumberFormat f = new DecimalFormat("0.###%");

                System.out.println("Error = " + f.format(getError())
                        + " +- " + f.format(getTheoreticalStdDev())
                        + " (" + f.format(getAccuracy()) + ")  ["
                        + nbPass + "/" + nbFail + "/"
                        + total + "]");
            } else {
                System.out.println("Error = ? (?)  [0/0/0]");
            }

        }
    }
}
