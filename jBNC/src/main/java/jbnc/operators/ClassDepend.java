/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.operators;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityVariable;
import jbnc.dataset.AttributeSpecs;
import jbnc.graphs.Edge;
import jbnc.graphs.Graph;

import java.util.Collection;
import java.util.Vector;

/**
 * Constructs a selective naive Bayes classifier.
 *
 * @author Jarek Sacha
 * @since June 1, 1999
 */
public final class ClassDepend extends Operator {
    /**
     * @param graphStruct
     * @param gamma
     * @param names
     * @return Bayesian network created from the graph.
     * @throws Exception
     */
    public static BayesNet create(Graph graphStruct,
                                  Collection gamma,
                                  AttributeSpecs[] names) throws Exception {
        //
        int nbAttrib = names.length - 1;
        int nbVars = nbAttrib + 1;

        // Create empty network
        BayesNet thisNet = new BayesNet("BNC", names.length, names.length);

        // Create class probability variable
        Vector classProp = new Vector();
        thisNet.set_probability_variable(nbAttrib,
                names[nbAttrib].getName(),
                names[nbAttrib].getStates(),
                classProp);

        // Create class probability function, p(C)
        int nbClasses = names[nbAttrib].getStates().length;
        double[] probZ = new double[nbClasses];
        ProbabilityVariable classPV = thisNet.get_probability_variable(nbAttrib);
        ProbabilityVariable[] classPVArray = new ProbabilityVariable[1];
        classPVArray[0] = classPV;
        thisNet.set_probability_function(nbAttrib, classPVArray, probZ, null);

        // Create attribute probability variables
        for (int node = 0; node < nbAttrib; ++node) {
            Vector nodeProp = new Vector();
            thisNet.set_probability_variable(node,
                    names[node].getName(),
                    names[node].getStates(),
                    nodeProp);
        }

        // invert graph to find a partent easier
        Graph invGraph = invertEdges(graphStruct);

        // Create probability functions for each attribute node
        for (int node = 0; node < nbAttrib; ++node) {
            int[] parents = invGraph == null
                    ? null
                    : invGraph.getChildrenOf(node);

            if (parents != null && parents.length > 1) {
                throw new Exception("Attribute node #" + node
                        + " has more then a single attribute parent"
                        + " (Augmenting tree is not a tree).");
            }

            int nbStates = names[node].getStates().length;
            double[] nodePFVals = null;
            ProbabilityVariable[] nodePV = null;

            Integer nodeInteger = new Integer(node);
            if (parents == null) {
                if (gamma.contains(nodeInteger)) {
                    // p(x|z)
                    nodePV = new ProbabilityVariable[2];
                    nodePV[0] = thisNet.get_probability_variable(node);
                    nodePV[1] = classPV;

                    nodePFVals = new double[nbClasses * nbStates];
                } else {
                    // p(x) - root node
                    nodePV = new ProbabilityVariable[1];
                    nodePV[0] = thisNet.get_probability_variable(node);

                    nodePFVals = new double[nbStates];
                }
            } else {
                int parentIndex = parents[0];

                // Sanity check
                if (parentIndex == node) {
                    throw new Exception("Vertex cannot be parent of itself.");
                }

                int nbParentStates = names[parentIndex].getStates().length;

                if (gamma.contains(nodeInteger)) {
                    // calculate p(x|y,z) = p(x,y|z)/p(y|z)
                    nodePFVals = new double[nbClasses * nbStates * nbParentStates];

                    nodePV = new ProbabilityVariable[3];
                    nodePV[0] = thisNet.get_probability_variable(node);
                    nodePV[1] = classPV;
                    nodePV[2] = thisNet.get_probability_variable(parentIndex);
                } else {
                    // calculate p(x|y) = p(x,y)/p(y)
                    nodePFVals = new double[nbStates * nbParentStates];

                    nodePV = new ProbabilityVariable[2];
                    nodePV[0] = thisNet.get_probability_variable(node);
                    nodePV[1] = thisNet.get_probability_variable(parentIndex);
                }
            }
            //if( parents == null )

            // Create probability varaible
            thisNet.set_probability_function(node, nodePV, nodePFVals, null);

        }
        //for(int node=0;...)

        return thisNet;
    }


    /**
     * Create a new graph with edges invereted in relation to the argument
     * <code>grapg</code>.
     *
     * @param graph
     * @return new graph with inverted edges.
     * @throws Exception
     */
    protected static Graph invertEdges(Graph graph) throws Exception {
        if (graph == null) {
            return null;
        }

        Graph invGraph = new Graph();
        Edge[] edges = graph.getEdges();
        for (int e = 0; e < edges.length; ++e) {
            invGraph.addEdge(edges[e].getOutVertex(), edges[e].getInVertex());
        }

        return invGraph;
    }

}
