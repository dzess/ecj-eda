/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.measures;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.DatasetInt;
import jbnc.util.BNTools;

/**
 *  Measure the quality of the bayesian network on the dataset using asymptotic
 *  estimate of the standard bayes measure. <p>
 *
 *  &nbsp;&nbsp;&nbsp; <i>q</i> = sum<sub><i>i</i> =1...<i>n</i> </sub> sum<sub>
 *  <i>j</i> =0...<i>r<sub>i</sub> </i> </sub> sum<sub><i>k</i> =1...<i>s<sub>i
 *  </sub></i> </sub> <i>N<sub>ijk</sub> </i> log <i>N<sub>ijk</sub> </i> / <i>N
 *  <sub>ij</sub> </i> - 1/2 <i>Dim</i> (<i>B</i> ) log <i>N</i> <p>
 *
 *  where <i>N<sub>ijk</sub> </i> means that variable <i>X<sub>i</sub> </i> is
 *  in configuration <i>j</i> and parents of variable <i>X<sub>i</sub> </i> are
 *  in configuration <i>k</i> , <i>Dim</i> (<i>B</i> ) is the dimension of the
 *  network, <i>N</i> is the number of cases in the training data set. <p>
 *
 *  E. Castillo, J. M. Gutierrez and A. S. Hadi, <i>Expert Systems and
 *  Probabilistic Network Models</i> , Springer, 1997. p.494, eq.(11.28).
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class QualityMeasureSB extends QualityMeasure {
  /**  Constructor for the QualityMeasureSB object */
  public QualityMeasureSB() {
    super();
  }


  /**
   *  Constructor for the QualityMeasureSB object
   *
   * @param  dataset  Description of Parameter
   */
  public QualityMeasureSB(DatasetInt dataset) {
    super(dataset);
  }


  /**
   *  Gets the Name attribute of the QualityMeasureSB object
   *
   * @return    The Name value
   */
  public String getName() {
    return "Standard Bayesian";
  }


  /**
   *  Description of the Method
   *
   * @param  net            Description of Parameter
   * @return                Description of the Returned Value
   * @exception  Exception  Description of Exception
   */
  public final double evaluate(BayesNet net)
      throws Exception {
    int dim = BNTools.getNetworkDimension(net);
    double qSize = 0.5 * dim * Math.log(dataset.cases.size());
    double qParam = BNTools.getASBMParamComponent(net, dataset, usePriors, alphaK);

    double q = qParam - qSize;

    return q;
  }

}
