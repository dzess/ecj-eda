/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.discretize;

import junit.framework.TestCase;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Test EntropyDiscretizer class.
 *
 * @author Jarek Sacha
 * @version $ Revision: $
 */

public final class EntropyDiscretizerTest extends TestCase {
    private Case[][] casesByAttribute;

    /**
     * Constructor.
     *
     * @param test Test case name.
     */
    public EntropyDiscretizerTest(final String test) {
        super(test);
    }

    public void testDiscretize() throws Exception {
        final Case[] cases = {
            new Case(0, 1),
            new Case(0, 2),
            new Case(0, 3),
            new Case(0, 4),
            new Case(1, 5),
            new Case(1, 6),
            new Case(1, 7),
            new Case(2, 8),
            new Case(2, 9),
            new Case(2, 10)
        };

//        double[] expectedResult = {4.5, 7.5};
        final double[] expectedResult = {4.0, 7.0};

        final EntropyDiscretizer discretizer = new EntropyDiscretizer();
        discretizer.buildDiscretizer(cases);

        final double[] part = discretizer.getPartitionBoundaries();
        assertEquals("Number of partitions", expectedResult.length, part.length);

        System.out.println("Found " + part.length + " partitions:");
        for (int i = 0; i < part.length; i++) {
//            final double v = part[i];
//            System.out.println("Part["+i+"]: "+v);
            assertEquals("Partition[" + i + "]", expectedResult[i], part[i], 0.001);
        }
    }


    /**
     * Test discretization on the IRIS dataset.
     */
    public void testIris() {
        final double[][] irisBoundaries = {
            {5.5, 6.1},
            {2.9, 3.3},
            {1.9, 4.7},
            {0.6, 1.7}};

        for (int i = 0; i < casesByAttribute.length; i++) {
            final Case[] cases = casesByAttribute[i];
            final EntropyDiscretizer discretizer = new EntropyDiscretizer();
            discretizer.buildDiscretizer(cases);
            final double[] boundaries = discretizer.getPartitionBoundaries();

//            for (int j = 0; j < boundaries.length; j++) {
//                System.out.println("Boundary " + j + " for attribute " + i + "; " +
//                        boundaries[j]);
//            }

            assertEquals("Number of interval boundaties for attribute " + i,
                    irisBoundaries[i].length, boundaries.length);

            for (int j = 0; j < boundaries.length; j++) {
                assertEquals("Boundary " + j + " for attribute " + i,
                        irisBoundaries[i][j], boundaries[j], 0.00001);
            }
        }
    }

    /**
     * The fixture set up called before every test method
     */
    protected void setUp() throws Exception {
        System.out.println("" + new File(".").getAbsolutePath());
        // Read test data
        final String testDataDir = System.getProperty("test.data.dir", "src/test/resources");
        final String fileName = testDataDir + File.separator + "iris.csv";
        final String[][] data = loadCSVData(fileName);
        final int nbArributes = data[0].length - 1;
        final int ClassIndex = data[0].length - 1;
        casesByAttribute = new Case[nbArributes][data.length];
        final ArrayList classes = new ArrayList();
        for (int i = 0; i < data.length; i++) {
            final String[] strings = data[i];

            final String classString = strings[ClassIndex];
            int classId = classes.indexOf(classString);
            if (classId < 0) {
                classes.add(classString);
                classId = classes.indexOf(classString);
            }

            for (int j = 0; j < nbArributes; j++) {
                final String string = strings[j];
                final double v = Double.parseDouble(string);
                casesByAttribute[j][i] = new Case(classId, v);
            }
        }
    }

    /**
     * The fixture clean up called after every test method
     */
    protected void tearDown() throws Exception {
    }

    /**
     * Read data from a comma delimited file, skip first line as a header.
     * Number of tokens in each line must be the same as number of tokens in the
     * header.
     *
     * @param fileName
     * @return Content of a comma delimited string a an String array. First
     *         index corresponds to lines in the input file. Second index to
     *         tokens in a line.
     * @throws IOException when there are problems reading or parsing the file.
     */
    private static String[][] loadCSVData(final String fileName) throws IOException {
        final String delimiter = ",";
        final LineNumberReader reader = new LineNumberReader(new FileReader(fileName));

        // Skip header
        final int nbColumns;
        {
            final String line = reader.readLine();
            final StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
            nbColumns = tokenizer.countTokens();
        }

        // Read remaining lines
        final ArrayList data = new ArrayList();
        String line = reader.readLine();
        while (line != null) {
            final StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
            if (nbColumns != tokenizer.countTokens()) {
                throw new IOException("Number of tokens in line "
                        + reader.getLineNumber() +
                        " is not equal number of tokens in the header ("
                        + nbColumns + "!=" + tokenizer.countTokens() + ").");
            }
            final String[] r = new String[tokenizer.countTokens()];
            for (int i = 0; i < r.length; ++i) {
                r[i] = tokenizer.nextToken();
            }
            data.add(r);
            line = reader.readLine();
        }
        return (String[][])data.toArray(new String[data.size()][]);
    }
}