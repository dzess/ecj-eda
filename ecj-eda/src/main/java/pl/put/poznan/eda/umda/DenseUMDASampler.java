package pl.put.poznan.eda.umda;

import java.util.LinkedList;
import java.util.List;

import pl.put.poznan.eda.IModelSampler;
import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.util.ECJDistributionSampler;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.EvolutionState;
import ec.Individual;
import ec.Species;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.ShortVectorIndividual;

/**
 * 
 * The basic implementation of UMDA sampler.
 * 
 * Does random sampling going alongside the representation (genome) and choosing values for gene according to the {@linkplain DenseUMDAModel}
 * 
 * <p>
 * Works only with the models based on representation of:
 * <ul>
 * <li>{@linkplain BitVectorIndividual}</li>
 * <li>{@linkplain ByteVectorIndividual}</li>
 * <li>{@linkplain ShortVectorIndividual}</li>
 * </ul>
 * </p>
 * 
 * <p>
 * Does <b>not</b> have any other parameters.
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.1
 * 
 */
public class DenseUMDASampler extends DenseUMDA implements IModelSampler {

    private static final long serialVersionUID = 4592456873010286472L;

    private static final int C_THREAD_NUMBER = 0;

    private IDistributionSampler distSampler;

    /**
     * Base constructor done for testing purposes and better configuration.
     * 
     * @param distributionSampler
     *            : the sampler of distribution which is to be used
     */
    public DenseUMDASampler(IDistributionSampler distributionSampler) {
        distSampler = distributionSampler;

    }

    /**
     * Constructor for the ECJ callback framework.
     */
    public DenseUMDASampler() {
        this(new ECJDistributionSampler());
    }

    @Override
    public void setup(EvolutionState state, Parameter base) {
        
        // safe check
        state.output.exitIfErrors();
    }

    @Override
    public List<Individual> sampleModel(Model inputModel, int amount, Species species, EvolutionState state) {

        // casting model on the one specific for UMDA
        DenseUMDAModel model = (DenseUMDAModel) inputModel;

        // take individuals from the NEW SUBPOPOULATION
        List<Individual> newPopInd = new LinkedList<Individual>();
        if (isBitVector) {

            for (int indIndex = 0; indIndex < amount; indIndex++) {

                // create new individual (must have the evaluated flag to false)
                // another question how the new individual is to be created
                BitVectorIndividual ind = (BitVectorIndividual) species.newIndividual(state, C_THREAD_NUMBER);
                for (int geneIndex = 0; geneIndex < ind.genome.length; geneIndex++) {

                    // sample the genes using the provided model
                    double probability = model.genomeProbabilites[geneIndex][0];
                    ind.genome[geneIndex] = distSampler.sampleDistribution(probability, state, C_THREAD_NUMBER);
                }
                // add the individual to the new population
                newPopInd.add(ind);
            }

        } else {
            if (!isByteVector) {
                for (int indIndex = 0; indIndex < amount; indIndex++) {
                    ShortVectorIndividual ind = (ShortVectorIndividual) species.newIndividual(state, C_THREAD_NUMBER);
                    for (int geneIndex = 0; geneIndex < ind.genome.length; geneIndex++) {

                        double[] geneProbabilityDist = model.genomeProbabilites[geneIndex];

                        // use the sampler wrapped up in some classes this will allow the unit testing of this code
                        int newGeneValue = distSampler.sampleDistribution(geneProbabilityDist, state, C_THREAD_NUMBER);
                        ind.genome[geneIndex] = (short) (newGeneValue + model.getMinGene());

                    }
                    // add the individual to the new population
                    newPopInd.add(ind);
                }
            } else {
                // it is the byte vector
                for (int indIndex = 0; indIndex < amount; indIndex++) {
                    ByteVectorIndividual ind = (ByteVectorIndividual) species.newIndividual(state, C_THREAD_NUMBER);
                    for (int geneIndex = 0; geneIndex < ind.genome.length; geneIndex++) {

                        double[] geneProbabilityDist = model.genomeProbabilites[geneIndex];

                        // use the sampler wrapped up in some classes this will allow the unit testing of this code
                        int newGeneValue = distSampler.sampleDistribution(geneProbabilityDist, state, C_THREAD_NUMBER);
                        ind.genome[geneIndex] = (byte) (newGeneValue + Byte.MIN_VALUE);

                    }
                    // add the individual to the new population
                    newPopInd.add(ind);
                }
            }
        }
        return newPopInd;
    }
}
