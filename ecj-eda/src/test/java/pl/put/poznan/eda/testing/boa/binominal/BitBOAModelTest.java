package pl.put.poznan.eda.testing.boa.binominal;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityFunction;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.VectorIndividual;

import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.testing.ECJTestingBase;

public class BitBOAModelTest extends ECJTestingBase {

    private static final double DOUBLE_DELTA = 0.00001;
    private BOAModel model;
    private VectorIndividual individual;

    @Before
    public void set_up() {
        this.setUpEvolutionStateBasic();
        this.setUpBase();

        model = new BitBOAModel();
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_needs_type_of_individual_to_work() {
        Parameter parameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(parameter, null);

        model.setup(state, parameter);
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_needs_vector_species_to_work() {
        Parameter parameter = BitBOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(parameter, 0);

        model.setup(state, base);
    }

    @Test
    public void boa_model_after_setup_has_no_linked_genomes() {

        int genomeLength = 3;

        individual = new BitVectorIndividual();
        individual.setGenome(new boolean[1]);
        individual.setGenomeLength(genomeLength);

        Parameter individualParameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(individualParameter, individual);

        Parameter genomeSizeParameter = BitBOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(genomeSizeParameter, genomeLength);

        model.setup(state, base);

        // assert that all the other things match
        Assert.assertEquals(genomeLength, model.getGenomeSize());
        Assert.assertEquals(individual.getClass(), model.getIndividual().getClass());

        BayesNet net = model.getNetwork();

        // the number of variables match
        Assert.assertEquals(genomeLength, net.get_probability_variables().length);

        // all variables are conditionally independent
        for (int i = 0; i < genomeLength; i++) {
            ProbabilityFunction func = net.get_probability_functions()[i];
            // no parental relationship to no other variable in the network
            Assert.assertEquals(1, func.get_variables().length);
            Assert.assertEquals(2, func.get_values().length);

            Assert.assertEquals(0.5, func.get_value(0), DOUBLE_DELTA);
            Assert.assertEquals(0.5, func.get_value(1), DOUBLE_DELTA);
        }
    }

    @Test(expected = IllegalStateException.class)
    public void boa_model_has_no_suport_for_other_than_bit_vectors() {

        int genomeLength = 3;
        individual = new ByteVectorIndividual();
        individual.setGenome(new byte[1]);
        individual.setGenomeLength(genomeLength);

        Parameter individualParameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(individualParameter, individual);

        Parameter genomeSizeParameter = BitBOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(genomeSizeParameter, genomeLength);

        model.setup(state, base);

    }

}
