package pl.put.poznan.eda.testing.boa.binominal;

import java.util.List;
import java.util.Vector;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;
import jbnc.dataset.AttributeSpecs;
import jbnc.inducers.NaiveBayesInducer;
import jbnc.measures.QualityMeasureHGC;
import jbnc.util.FrequencyCalc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import pl.put.poznan.eda.Model;
import pl.put.poznan.eda.boa.BOAModel;
import pl.put.poznan.eda.boa.JBNCUtil;
import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.boa.binominal.BitBOASampler;
import pl.put.poznan.eda.testing.ECJSamplerTestingBase;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import ec.Individual;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;

public class BitBOASamplerTest extends ECJSamplerTestingBase {

    private BitBOASampler sampler;
    private int genomeSize;
    private int individualsSize;

    @Before
    public void set_up() {

        this.setUpBase();
        this.setUpEvolutionStateBasic();

        distributionMock = this.prepareDistributionSampler();
        sampler = new BitBOASampler(distributionMock);

    }

    private void prepareForSampling(Model model) throws Exception {

        // needed for sampling testing
        individualsSize = 2;

        this.prepareSubpopulation(individualsSize, model, BitVectorIndividual.class);

        BitVectorIndividual ind1 = new BitVectorIndividual();
        BitVectorIndividual ind2 = new BitVectorIndividual();

        ind1.genome = new boolean[genomeSize];
        ind2.genome = new boolean[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(BitVectorIndividual.class);
    }

    private BOAModel prepareModel(BayesNet net) {
        BOAModel model = Mockito.mock(BitBOAModel.class);
        Mockito.when(model.getGenomeSize()).thenReturn(genomeSize);
        Mockito.when(model.getNetwork()).thenReturn(net);
        return model;
    }

    @Test
    public void given_bayes_net_sampling_is_done_properly_1() throws Exception {

        genomeSize = 3;

        // code needed by the setup method of the BOAModel
        BitVectorIndividual individual = new BitVectorIndividual();
        individual.setGenome(new boolean[1]);
        individual.setGenomeLength(genomeSize);

        Parameter individualParameter = BOAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(individualParameter, individual);

        Parameter genomeSizeParameter = BitBOAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(genomeSizeParameter, genomeSize);

        // create the BayesNet easy model (which will be easy to sample)
        // preferable with no linkage at all - such network is created by the BOAModel at first
        // each probability of each value should be 0.5
        BOAModel model = new BitBOAModel();
        model.setup(state, base);

        this.prepareDistribution(0.4);
        this.prepareForSampling(model);

        // sampler.sampleModel(newSubpopulation, oldSubpopulation, state);
        List<Individual> newInds = sampler.sampleModel(model, individualsSize, species, state);

        // write the assertion which are needed
        Assert.assertEquals(individualsSize, newInds.size());
        for (int i = 0; i < individualsSize; i++) {
            BitVectorIndividual ind = (BitVectorIndividual) newInds.get(i);

            // given the distribution returns 0.4 and binary variable has exceeding point (to FALSE)
            for (int j = 0; j < genomeSize; j++) {
                Assert.assertFalse(ind.genome[j]);
            }
        }
    }

    @Test
    public void given_bayes_net_sampling_is_done_properly_2() throws Exception {

        // the medicore Bayesian Network
        // A -> B
        // P(A = true) = 0.8
        // P(B = true | A = true ) = 0.6
        // P(B = true | A = false) = 0.9
        // not that change of the B basing on the A results that
        // variables A and B are dependent
        genomeSize = 2;
        int networkFunctions = 2;
        BayesNet net = new BayesNet("sample network", genomeSize, networkFunctions);

        Vector<String> emptyVector = new Vector<String>();
        ProbabilityVariable[] variables = net.get_probability_variables();
        ProbabilityFunction[] functions = net.get_probability_functions();

        variables[0] = JBNCUtil.createBinaryProbabilityVariable(net, "A", emptyVector);
        variables[1] = JBNCUtil.createBinaryProbabilityVariable(net, "B", emptyVector);

        // P(A) = 0.8
        double[] probOfA = new double[2];
        probOfA[0] = 0.2; // for false on the genome
        probOfA[1] = 0.8; // for true on the genome
        DiscreteVariable[] discreateVarsA = new DiscreteVariable[1];
        discreateVarsA[0] = variables[0];
        functions[0] = new ProbabilityFunction(net, discreateVarsA, probOfA, emptyVector);

        // the conditional function on the P(B) -> P(B|A)
        double[] probOfB = new double[4];
        probOfB[0] = 0.1; // P(F|F)
        probOfB[1] = 0.4; // P(F|T)
        probOfB[2] = 0.9; // P(T|F)
        probOfB[3] = 0.6; // P(T|T)

        DiscreteVariable[] discreateVarsB = new DiscreteVariable[2];
        // NOTE: that for function P(B|A) the first variable is B not A ;)
        discreateVarsB[0] = variables[1];
        discreateVarsB[1] = variables[0];
        functions[1] = new ProbabilityFunction(net, discreateVarsB, probOfB, emptyVector);

        BOAModel model = this.prepareModel(net);

        this.prepareDistribution(0.3);

        this.prepareForSampling(model);
        List<Individual> newInds = sampler.sampleModel(model, individualsSize, species, state);

        // assertions about the sampled individuals
        Assert.assertEquals(individualsSize, newInds.size());
        for (int i = 0; i < individualsSize; i++) {
            BitVectorIndividual ind = (BitVectorIndividual) newInds.get(i);
            // basing on some analysis on sheet of paper the result should be 11
            Assert.assertTrue(ind.genome[0]);
            Assert.assertFalse(ind.genome[1]);
        }
    }

    @Test
    public void sampler_works_fine_on_the_data_from_real_learning() throws Exception {

        // this is integration level testing using some inducers from the jBNC
        // note that this example is copier from learn tests in the basic
        // jBNC testing suite
        QualityMeasureHGC measure = new QualityMeasureHGC();
        NaiveBayesInducer inducer = new NaiveBayesInducer();
        inducer.setQualityMeasure(measure);

        genomeSize = 2;

        int casesLength = 3;

        // prepare the attributes
        AttributeSpecs[] attributes = JBNCUtil.prepareBinominalAttributes(genomeSize);

        // prepare the data
        // the data look that:
        // G0 G1
        // 0 1
        // 1 1
        // 1 1
        int[][] data = new int[casesLength][genomeSize];

        data[0][0] = 0;
        data[0][1] = 1;

        data[1][0] = 1;
        data[1][1] = 1;

        data[2][0] = 1;
        data[2][1] = 1;

        FrequencyCalc fCalc = JBNCUtil.prepareFrequencyCalc(data, attributes);
        inducer.train(fCalc);
        BayesNet network = inducer.getNetwork();

        // print this network into standard output
        network.print(System.out);

        BOAModel model = this.prepareModel(network);
        this.prepareDistribution(0.6);

        // needed for sampling testing
        individualsSize = 3;

        this.prepareSubpopulation(individualsSize, model, BitVectorIndividual.class);

        BitVectorIndividual ind1 = new BitVectorIndividual();
        BitVectorIndividual ind2 = new BitVectorIndividual();
        BitVectorIndividual ind3 = new BitVectorIndividual();

        ind1.genome = new boolean[genomeSize];
        ind2.genome = new boolean[genomeSize];
        ind3.genome = new boolean[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2, ind3);

        sampler.markIndividualType(BitVectorIndividual.class);

        // act
        List<Individual> newInds = sampler.sampleModel(model, individualsSize, species, state);

        // assert
        Assert.assertEquals(individualsSize, newInds.size());
        for (int i = 0; i < individualsSize; i++) {
            BitVectorIndividual ind = (BitVectorIndividual) newInds.get(i);
            // 11 is the correct sample - rest of the math will be explained in some XLS files in resources
            Assert.assertTrue(ind.genome[0]);
            Assert.assertTrue(ind.genome[1]);
        }
    }

    @Ignore(value = "not yet implemented")
    public void given_bayes_net_sampling_is_done_properly_3() {
        // full linkage between genes (quite unimplementable yet use the second example from the net)
        Assert.fail("not yet implemented");
    }

    @Test
    public void sampling_works_with_bit_byte_short_vector() {
        Assert.assertTrue(sampler.matchIndividualType(BitVectorIndividual.class));

        // other types are not supported officially yet
        Assert.assertFalse(sampler.matchIndividualType(ShortVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(ByteVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(IntegerVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(FloatVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(DoubleVectorIndividual.class));

    }

    @Test
    public void sampling_works_only_with_boa_model() {
        Assert.assertTrue(sampler.matchModelType(BitBOAModel.class));
        // other models are not supported
        Assert.assertFalse(sampler.matchModelType(DenseUMDAModel.class));
    }
}
