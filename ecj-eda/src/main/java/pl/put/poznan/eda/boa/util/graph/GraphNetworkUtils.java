package pl.put.poznan.eda.boa.util.graph;

import java.util.LinkedList;
import java.util.List;

/**
 * Holds the statics for manipulation of data structures used in {@linkplain GraphNetwork} and by {@linkplain GraphTopologicalSorter}.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class GraphNetworkUtils {

    @SuppressWarnings("unchecked")
    public static List<Integer>[] createArrayOfLists(int length) {
        if (length < 1) {
            throw new IllegalArgumentException("created array cannot be smaller than 1");
        }
        // this code is kind weird by does the job
        List<Integer>[] result = (List<Integer>[]) new LinkedList[length];
        for (int i = 0; i < length; i++) {
            result[i] = new LinkedList<Integer>();
        }

        return result;
    }

    public static List<Integer>[] getDeepCopy(List<Integer>[] input) {

        // TODO: there might be some performance impact with such a creation method
        List<Integer>[] result = createArrayOfLists(input.length);

        for (int i = 0; i < input.length; i++) {
            // here goes cloning (the integers are only boxed)
            List<Integer> innerList = input[i];
            List<Integer> clonedList = new LinkedList<Integer>(innerList);
            result[i] = clonedList;
        }
        return result;
    }

}
