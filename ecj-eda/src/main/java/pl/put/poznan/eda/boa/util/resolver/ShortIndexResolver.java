package pl.put.poznan.eda.boa.util.resolver;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.DiscreteVariable;
import pl.put.poznan.eda.boa.multinominal.ShortBOAModel;
import pl.put.poznan.eda.boa.util.graph.GraphNetwork;
import pl.put.poznan.eda.boa.util.graph.GraphTuple;

/**
 * Utility class that helps getting proper values from the {@linkplain BayesNet} structure. It bases on the idea that values stored within
 * {@linkplain GraphTuple} are {@linkplain Short} values and follow the {@linkplain ShortBOAModel}.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class ShortIndexResolver implements IIndexResolver<Short> {

    private GraphTuple<Short> currentTuple;
    private int location;
    private int mainStatesCount;

    @Override
    public Short getValueGivenEvidance(int sampledIndex) {
        String value = currentTuple.variable.get_value(sampledIndex);
        Short numberValue = Short.parseShort(value);
        return numberValue;
    }

    @Override
    public double[] getProbabilitesGivenEvidance(GraphTuple<Short> tuple, GraphNetwork graph) {

        currentTuple = tuple;
        DiscreteVariable[] usedVariables = tuple.function.get_variables();

        if (usedVariables.length == 1) {
            return tuple.function.get_values();
        } else {
            location = 0;

            // iterate through dependents
            for (int i = 1; i < usedVariables.length; i++) {
                DiscreteVariable var = usedVariables[i];
                int dependentStatesCount = var.get_values().length;

                int inGraphIndex = graph.searchForIndex(var);
                @SuppressWarnings("unchecked")
                GraphTuple<Short> dependentTuple = graph.searchForGraphTuple(inGraphIndex);

                if (dependentTuple.evaluated == false) {
                    throw new IllegalStateException("the ordering of variables is not right");
                }

                location = location * dependentStatesCount + dependentTuple.value;
            }

            // we have shift for all the dependents now it is easy to start mangling with the output variable
            DiscreteVariable outputVariable = usedVariables[0];
            mainStatesCount = outputVariable.get_values().length;
            double[] probabilites = new double[mainStatesCount];
            for (int i = 0; i < mainStatesCount; i++) {
                int probabilityIndex = i * mainStatesCount + location;
                double probability = tuple.function.get_value(probabilityIndex);
                probabilites[i] = probability;
            }
            return probabilites;
        }
    }

    @Override
    public void reset() {
        currentTuple = null;
        location = 0;
    }

}
