/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.util;


import jbnc.dataset.AttributeSpecs;
import jbnc.dataset.Dataset;
import jbnc.dataset.DatasetInt;

import java.util.Iterator;

/**
 *  Calculate variable frequencies given a dataset.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class FrequencyCalc {

  /**  Number of cases in the dataset the dataset. */
  public int nbCases = 0;

  /**
   *  Specs for variables in the dataset from which the frequecies were
   *  calculated.
   */
  public AttributeSpecs[] names = null;
  /**
   *  Joint frequency of variable values X, Y and Z. X corresponds any attribute
   *  with index [0,nbAttrib-1]. Y corresponds any attribute with index
   *  [0,nbAttrib-1]. Index of X is always less or iqual to Y, since relation is
   *  symetric. Z is the last variable, so its index is implicit (equal to
   *  nbAttrib). <br>
   *  <br>
   *  Note that elements on the diagonal are used only for calculation of
   *  p(x,z), they are not used guring computation of p(x,y,z). <pre>
   * freqXYZ[index_x][index_y][state_x][state_y][state_z]
   * </pre>
   */
  protected int[][][][][] freqXYZ = null;

  /**
   *  Joint frequency of variable values X and Y. X corresponds variable with
   *  index [0,nbAttrib]. Y corresponds variable with index [0,nbAttrib]. <pre>
   * freqXY[index_x][index_y][state_x][state_y]
   * </pre>
   */
  protected int[][][][] freqXY = null;

  /**
   *  Frequency of variable values X. X corresponds any variable, with index
   *  [0,nbAttrib]. <pre>
   * freqX[index_x][state_x]
   * </pre>
   */
  protected int[][] freqX = null;
  protected Dataset dataset = null;


  /**
   *  Constructor for the FrequencyCalc object
   *
   * @param  dataset  Description of Parameter
   */
  public FrequencyCalc(DatasetInt dataset) {
    init(dataset);

    // Calculate frequencies
    calcFreqXYZ();

    calcFreqXY_X();

  }



  //----------------------------------------------------------------------
  //
  // LOCAL
  //



  /**
   * @param  dataset  Description of Parameter
   */
  protected void init(Dataset dataset) {
    this.nbCases = dataset.cases.size();
    this.names = dataset.names;
    this.dataset = dataset;

    int nbAttrib = names.length - 1;

    freqXYZ = new int[nbAttrib][nbAttrib][][][];
    freqXY = new int[nbAttrib + 1][nbAttrib + 1][][];
    freqX = new int[nbAttrib + 1][];

    int size_z = names[nbAttrib].getStates().length;

    for (int xi = 0; xi < nbAttrib; ++xi) {
      int size_x = names[xi].getStates().length;
      freqXYZ[xi][xi] = new int[size_x][size_x][size_z];
      // diagonal
      for (int yi = xi + 1; yi < nbAttrib; ++yi) {
        int size_y = names[yi].getStates().length;
        freqXYZ[xi][yi] = new int[size_x][size_y][size_z];
        freqXYZ[yi][xi] = new int[size_y][size_x][size_z];
      }
    }

    for (int xi = 0; xi < nbAttrib + 1; ++xi) {
      int size_x = names[xi].getStates().length;
      freqXY[xi][xi] = new int[size_x][size_x];
      freqX[xi] = new int[size_x];
      for (int yi = xi + 1; yi < nbAttrib + 1; ++yi) {
        int size_y = names[yi].getStates().length;
        freqXY[xi][yi] = new int[size_x][size_y];
        freqXY[yi][xi] = new int[size_y][size_x];
      }
    }
  }


  /**  Calculate values of freqXYZ */

  protected void calcFreqXYZ() {
    int nbAttrib = names.length - 1;

    for (int c = 0; c < nbCases; ++c) {
      int[] thisCase = (int[]) dataset.cases.get(c);
      int vz = thisCase[nbAttrib];

      for (int xi = 0; xi < nbAttrib; ++xi) {
        int vx = thisCase[xi];
        int[][][][] freqXYZxi = freqXYZ[xi];
//        ++freqXYZ[xi][xi][vx][vx][vz]; // update diagonal
        ++freqXYZxi[xi][vx][vx][vz];
        for (int yi = xi + 1; yi < nbAttrib; ++yi) {
          int vy = thisCase[yi];
//          ++freqXYZ[xi][yi][vx][vy][vz];
          ++freqXYZxi[yi][vx][vy][vz];
          ++freqXYZ[yi][xi][vy][vx][vz];
        }
      }
    }
  }


  /**  Calculate values of freqX and freqXY. */
  protected void calcFreqXY_X() {
    int nbVars = names.length;

    Iterator c = dataset.cases.iterator();
    while (c.hasNext()) {
      int[] thisCase = (int[]) (c.next());

      for (int xi = 0; xi < nbVars; ++xi) {
        int vx = thisCase[xi];

        ++freqX[xi][vx];
//        ++freqXY[xi][xi][vx][vx];
        int[][][] freqXYxi = freqXY[xi];
        ++freqXYxi[xi][vx][vx];

        for (int yi = xi + 1; yi < nbVars; ++yi) {
          int vy = thisCase[yi];

//          ++freqXY[xi][yi][vx][vy];
          ++freqXYxi[yi][vx][vy];
          ++freqXY[yi][xi][vy][vx];
        }
      }
    }
  }
}
