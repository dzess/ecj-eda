package pl.put.poznan.eda.boa.util.graph;

import pl.put.poznan.eda.IModelSampler;
import jb.BayesianNetworks.ProbabilityFunction;
import jb.BayesianNetworks.ProbabilityVariable;

/**
 * Tuple of {@linkplain ProbabilityFunction} and {@linkplain ProbabilityVariable} which represents the node in a graph. Holds also the
 * value of the current node and information if it was already evaluated.
 * 
 * This is helper structure used by {@linkplain IModelSampler} for BOA variations algorithms.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class GraphTuple<T> {

    public ProbabilityFunction function;
    public ProbabilityVariable variable;

    public int index;
    public boolean evaluated = false;
    public T value;

    public GraphTuple(int indexInMap) {
        this.index = indexInMap;
    }

    public void reset() {
        this.evaluated = false;
        this.value = null;
    }
}
