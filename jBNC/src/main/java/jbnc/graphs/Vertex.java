/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.graphs;

/**
 *  Description of the Class
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class Vertex {

  //----------------------------------------------------------------------
  //
  // LOCAL
  //
  protected String name = null;

  protected int index = -1;


  /**
   *  Constructor for the Vertex object
   *
   * @param  name   Description of Parameter
   * @param  index  Description of Parameter
   */
  public Vertex(String name, int index) {
    this.name = name;
    this.index = index;
  }


  /**
   *  Gets the Name attribute of the Vertex object
   *
   * @return    The Name value
   */
  public String getName() {
    return name;
  }


  /**
   *  Gets the Index attribute of the Vertex object
   *
   * @return    The Index value
   */
  public int getIndex() {
    return this.index;
  }


  /*
   *  public void setName(String name)
   *  {
   *  this.name = name;
   *  }
   */
  /**
   *  Description of the Method
   *
   * @return    Description of the Returned Value
   */
  public String toString() {
    return name + " [" + index + "]";
  }

}
