/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.graphs;

import java.util.Comparator;

/**
 *  A comparison function, which imposes a total ordering on a collection of
 *  EdgeWithWeights objects.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
class EdgeWeightComparator implements Comparator {
  /**
   * @param  x  Description of Parameter
   * @param  y  Description of Parameter
   * @return    Description of the Returned Value
   */
  public int compare(Object x, Object y) {
    double wx = ((EdgeWithWeight) x).getWeight();
    double wy = ((EdgeWithWeight) y).getWeight();

    if (wx < wy) {
      return -1;
    }

    if (wx == wy) {
      return 0;
    }

    return 1;
  }


  /**
   * @param  o  Description of Parameter
   * @return    Description of the Returned Value
   */
  public boolean equals(Object o) {
    return true;
  }
}
