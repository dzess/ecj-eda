package pl.put.poznan.eda.testing.umda;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import pl.put.poznan.eda.testing.ECJTestingBase;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import ec.Individual;
import ec.util.Parameter;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;
import ec.vector.VectorIndividual;

public class DenseUMDAModelTest extends ECJTestingBase {

    private DenseUMDAModel model;

    private VectorIndividual individual;

    public static final int GENOME_LENGTH = 16;

    @Before
    public void set_up() {
        this.setUpEvolutionStateBasic();
        this.setUpBase();

        Individual ind = new BitVectorIndividual();

        // set up typical setting for all the tests
        this.generateGetInstanceForParameters(DenseUMDAModel.U_INDIVIDUAL, ind);
        this.generateGetIntForParameter(DenseUMDAModel.U_GENOME_SIZE, GENOME_LENGTH);

        // create SUT
        model = new DenseUMDAModel();
    }

    @Test(expected = IllegalStateException.class)
    public void umda_model_needs_type_of_indivdual_to_work() {
        Parameter parameter = DenseUMDAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(parameter, null);

        model.setup(state, base);
    }

    @Test(expected = IllegalStateException.class)
    public void umda_model_needs_vector_species_genome_size_to_work() {
        Parameter parameter = DenseUMDAModel.U_GENOME_SIZE;
        this.generateGetIntForParameter(parameter, 0);

        model.setup(state, base);
    }

    private void preapreForValidation() {
        individual = new BitVectorIndividual();
        individual.setGenome(new boolean[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        Parameter parameter = DenseUMDAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(parameter, individual);

        model.setup(state, base);

        // up to this point throws no exception
    }

    @Test(expected = ArithmeticException.class)
    public void umda_validates_properly_1() {
        preapreForValidation();

        // insert some disturbance -> the models sums not into 1.0 again.
        model.genomeProbabilites[0][0] = 0.8;
        model.validate();
    }

    @Test(expected = ArithmeticException.class)
    public void umda_validates_properly_2() {
        preapreForValidation();

        // insert some disturbance -> the values should be 0..1
        model.genomeProbabilites[0][0] = -1;
        model.validate();
    }

    @Test(expected = ArithmeticException.class)
    public void umda_validates_properly_3() {
        preapreForValidation();

        // insert some disturbance -> the values should be 0..1
        model.genomeProbabilites[0][0] = 2.0;
        model.validate();
    }

    private void executeProbabilityCheck(long genomeCardinality, double expectedProbability, VectorIndividual individual) {
        Parameter parameter = DenseUMDAModel.U_INDIVIDUAL;
        this.generateGetInstanceForParameters(parameter, individual);

        model.setup(state, base);

        // cardinality is two
        Assert.assertEquals(genomeCardinality, model.getGenomeCardinality());

        // each of the cardinality has the genome size
        for (int i = 0; i < GENOME_LENGTH; i++) {
            Assert.assertEquals(genomeCardinality, model.genomeProbabilites[i].length);
        }

        // assert that probability is 0.5 on each gene
        for (int i = 0; i < genomeCardinality; i++) {
            for (int j = 0; j < GENOME_LENGTH; j++) {
                Assert.assertEquals(expectedProbability, model.genomeProbabilites[j][i]);
            }
        }
    }

    @Test
    public void umda_model_has_equall_probability_on_each_gene_bit_vector() {

        // presets of the UMDA model
        int genomeCardinality = 2;
        double expectedProbability = 0.5;
        individual = new BitVectorIndividual();
        individual.setGenome(new boolean[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }

    @Test
    public void umda_model_has_equal_probability_on_each_gene_byte_vector() {
        // presets of the UMDA model
        int genomeCardinality = Byte.MAX_VALUE + Math.abs(Byte.MIN_VALUE) + 1;
        double expectedProbability = 1.0 / (double) genomeCardinality;
        individual = new ByteVectorIndividual();
        individual.setGenome(new byte[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }

    @Ignore("with introduciton of max and min gene contraint this cannot be used anymore")
    public void umda_model_has_equal_probability_on_each_gene_short_vector() {

        // presets of the UMDA model
        int genomeCardinality = Short.MAX_VALUE + Math.abs(Short.MIN_VALUE) + 1;
        double expectedProbability = 1.0 / (double) genomeCardinality;
        individual = new ShortVectorIndividual();
        individual.setGenome(new short[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }

    @Test(expected = IllegalStateException.class)
    public void umda_model_has_equal_probability_on_each_gene_int_version() {

        // count zero ;)
        long genomeCardinality = Integer.MAX_VALUE + Math.abs(Integer.MIN_VALUE) + 1;
        double expectedProbability = 1 / (double) genomeCardinality;

        individual = new IntegerVectorIndividual();
        individual.setGenome(new int[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        // integer is not supported by the Java language limits in
        // creating arrays with new double[long_value] construct
        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }

    @Test(expected = IllegalStateException.class)
    public void umda_model_has_no_support_for_float() {

        // the cardinality is not important
        int genomeCardinality = 1;
        double expectedProbability = 1 / (double) genomeCardinality;

        individual = new FloatVectorIndividual();
        individual.setGenome(new float[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }

    @Test(expected = IllegalStateException.class)
    public void umda_model_has_no_support_for_double() {
        // the cardinality is not important
        int genomeCardinality = 1;
        double expectedProbability = 1 / (double) genomeCardinality;

        individual = new DoubleVectorIndividual();
        individual.setGenome(new double[1]);
        individual.setGenomeLength(GENOME_LENGTH);

        executeProbabilityCheck(genomeCardinality, expectedProbability, individual);
    }
}
