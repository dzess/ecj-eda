package pl.put.poznan.eda.testing.umda;

import org.mockito.Mockito;

import pl.put.poznan.eda.testing.ECJSamplerTestingBase;
import pl.put.poznan.eda.umda.DenseUMDASampler;
import pl.put.poznan.eda.umda.DenseUMDAUpdater;
import pl.put.poznan.eda.umda.DenseUMDAModel;

/**
 * 
 * Base utility class for {@linkplain DenseUMDAModel} related codes testing:
 * 
 * <p>
 * Used for testing classes:
 * <ul>
 * <li>{@linkplain DenseUMDASampler}</li>
 * <li>{@linkplain DenseUMDAUpdater}</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class DenseUMDATestingBase extends ECJSamplerTestingBase {

    protected DenseUMDAModel prepareModel(int genomeCardinality, int genomeSize) {
        DenseUMDAModel model = Mockito.mock(DenseUMDAModel.class);
        model.genomeProbabilites = new double[genomeSize][genomeCardinality];
        Mockito.when(model.getGenomeSize()).thenReturn(genomeSize);
        Mockito.when(model.getGenomeCardinality()).thenReturn(genomeCardinality);

        return model;
    }

}