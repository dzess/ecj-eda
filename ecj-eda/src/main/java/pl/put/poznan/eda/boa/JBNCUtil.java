package pl.put.poznan.eda.boa;

import java.util.Vector;

import jb.BayesianNetworks.BayesNet;
import jb.BayesianNetworks.ProbabilityVariable;
import jbnc.dataset.AttributeSpecs;
import jbnc.dataset.AttributeSpecs.AttributeException;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.inducers.BayesianInducer;
import jbnc.util.FrequencyCalc;

/**
 * Utility class that allows the mundane {@linkplain BayesNet} manipulation alongside with <b>jBNC Toolkit</b>.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class JBNCUtil {

    /**
     * Prepares the array of {@linkplain AttributeSpecs} with binomial values so with {@linkplain AttributeType} equal to <b>DISCRETE_N</b>.
     * 
     * @param numberOfVariables
     *            : number of attributes
     * @return the newly allocated array of {@linkplain AttributeSpecs}
     */
    public static AttributeSpecs[] prepareBinominalAttributes(int numberOfVariables) {

        AttributeSpecs[] attributes = new AttributeSpecs[numberOfVariables];
        for (int i = 0; i < numberOfVariables; i++) {
            final String attrName = "G" + i;
            // attribute type must be provided with the official states for this are the 0,1 (binary)
            try {
                attributes[i] = new AttributeSpecs(attrName, 2);
            } catch (AttributeException e) {
                throw new IllegalStateException("jBNC toolkit invoked quite bad operation", e);
            }
        }
        return attributes;
    }

    /**
     * Prepares the array of {@linkplain AttributeSpecs} with multinomial values with regard to <b>min</b> and <b>max</b> values.
     * 
     * @param numberOfVariables
     *            : number of attributes in the provided data
     * @param min
     *            : starting point for the range of multinomial attributes
     * @param max
     *            : ending point for the range of multinomial attributes
     * 
     * @return the newly allocated array of {@linkplain AttributeSpecs}
     */
    public static AttributeSpecs[] prepareMultinominalAttributesSpace(int numberOfVariables, int min, int max) {

        int cardinality = max - min + 1;
        int current = min;
        String[] states = new String[cardinality];
        for (int i = 0; i < states.length; i++) {
            states[i] = new Integer(current).toString();
            current += 1;
        }

        AttributeSpecs[] attributes = new AttributeSpecs[numberOfVariables];
        for (int i = 0; i < attributes.length; i++) {
            final String attrName = "G" + i;
            try {
                attributes[i] = new AttributeSpecs(attrName, states);
            } catch (Exception e) {
                throw new IllegalStateException("jBNC toolkit invoked quite bad operation", e);
            }
        }
        return attributes;
    }

    /**
     * Prepares the {@linkplain FrequencyCalc} which is needed by {@linkplain BayesianInducer}
     * 
     * @param data
     *            the data on which the {@linkplain BayesianInducer} will be trained.
     * @param attributes
     *            the attributes of the data
     * 
     * @return properly created data set in form of {@linkplain FrequencyCalc}
     */
    public static FrequencyCalc prepareFrequencyCalc(int[][] data, AttributeSpecs[] attributes) {
        DatasetInt dataset;
        try {
            dataset = new DatasetInt(data, attributes);
        } catch (Exception e) {
            throw new IllegalStateException("jBNC toolkit invoked quite bad operation", e);
        }
        FrequencyCalc frequencyCalculation = new FrequencyCalc(dataset);
        return frequencyCalculation;
    }

    /**
     * Creates the new {@linkplain ProbabilityVariable} with already set values. Values are set to <code>{'0', '1'}</code>. Other parameters
     * are just forwarded parameters of the {@linkplain ProbabilityVariable} constructor.
     * 
     * @param network
     *            : the {@linkplain BayesNet} in which this variable resides
     * @param name
     *            : name of the variable
     * @param vector
     *            : vector of properties
     * @return the newly allocated instance of {@linkplain ProbabilityVariable}
     */
    public static ProbabilityVariable createBinaryProbabilityVariable(BayesNet network, String name, Vector<String> vector) {
        ProbabilityVariable variable = new ProbabilityVariable(network, name, vector);

        // those values have to be allocated during creation, to not being shared - using static final is not possible
        String[] values = new String[2];
        values[0] = "0";
        values[1] = "1";
        variable.set_values(values);
        return variable;

    }

    /**
     * Creates the new {@linkplain ProbabilityVariable} with already set values. Values range from <b>min</b> to <b>max</b> inclusively.
     * Other parameters are just forwarded parameters of the {@linkplain ProbabilityVariable} constructor.
     * 
     * @param network
     *            : the {@linkplain BayesNet} in which this variable resides
     * @param name
     *            : name of the variable
     * @param vector
     *            : vector of properties
     * @param max
     *            : maximal number to which values in {@linkplain ProbabilityVariable} are to be created
     * @param min
     *            : minimal number to which values in {@linkplain ProbabilityVariable} are to be created
     * @return the newly allocated instance of {@linkplain ProbabilityVariable}
     */
    public static ProbabilityVariable createShortMulitnominalVariable(BayesNet network, String name, Vector<String> vector, int min, int max) {
        ProbabilityVariable variable = new ProbabilityVariable(network, name, vector);

        if (min > max) {
            throw new IllegalArgumentException("min value cannot be greater than max");
        }

        // those values have to be allocated during creation, to not being shared - using static final is not possible
        int cardinality = max - min + 1;
        int current = min;
        String[] values = new String[cardinality];
        for (int i = 0; i < cardinality; i++) {
            values[i] = new Integer(current).toString();
            current += 1;
        }

        assert current == max + 1;

        variable.set_values(values);
        return variable;
    }
}
