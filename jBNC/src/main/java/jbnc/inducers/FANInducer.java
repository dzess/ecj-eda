/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.inducers;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.AttributeType;
import jbnc.dataset.DatasetInt;
import jbnc.operators.Augmenter;
import jbnc.operators.ForestAugmenter;
import jbnc.util.BNTools;

import java.util.ArrayList;

/**
 *  Constructs Forest Augmented Naive Bayes classifier (FAN).
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class FANInducer extends BayesianInducer {

  /**
   *  Used for testing of the FAN algorithm.
   *
   * @param  arg  The command line arguments
   */
  public static void main(String[] arg) {
    try {
      jbnc.util.Timer t = new jbnc.util.Timer();
      System.out.println("\nTesting class...\n");

      String fileStem = null;
      if (arg.length > 0) {
        fileStem = arg[0];
      } else {
        fileStem = "..\\db\\vote";
      }
//        fileStem = "..\\db_cv\\cleve-0-0";

      System.out.println("Load dataset: " + fileStem);
      DatasetInt dataset = new DatasetInt();
      dataset.setDiscardIncompleteCases(true);
      dataset.openC45(fileStem);
      dataset.discardAllOfType(AttributeType.IGNORE);
      jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(dataset);

      FANInducer fan = new FANInducer();
//      fan.setQualityMeasure(new QualityMeasures.QualityMeasureLC(dataset));
      fan.setQualityMeasure(new jbnc.measures.QualityMeasureLCV_LOO(dataset));
//      fan.setQualityMeasure(new QualityMeasures.QualityMeasureLCV(dataset,10,1));
      fan.setDebugMode(true);
      fan.train(fc);
      t.stop();

      BayesNet thisNet = fan.getNetwork();
      jbnc.util.BNCTester tester = new jbnc.util.BNCTester();
      jbnc.util.BNCTester.Result r = tester.test(thisNet, dataset);
      double ratio = r.nbPass / (double) (r.nbPass + r.nbFail);
      System.out.println("Accuracy on training set = " + (ratio * 100) + "%");
      t.println("\nExecution time = ");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   *  Construct FAN network from the training data.
   *
   * @param  fc             Description of Parameter
   * @param  usePriors      Description of Parameter
   * @param  alpha_ijk      Description of Parameter
   * @exception  Exception  Description of Exception
   */
  public void train(jbnc.util.FrequencyCalc fc,
                    boolean usePriors,
                    double alpha_ijk) throws Exception {
    // Clean storage
    clean();

    // Some sanity checks
//    verifyDataset(fc.dataset);

    //Make sure that usePriors and alphaK are consistent
    if (!usePriors) {
      alpha_ijk = 0;
    }

    // Check quality measure
    if (qualityMeasure == null) {
      throw new Exception("Network quality measure has not been assigned.");
    }

    qualityMeasure.setUsePriors(usePriors);
    qualityMeasure.setAlphaK(alpha_ijk);

    int nbAttrib = fc.names.length - 1;

    // Set all attributes as dependent on the class node
    ArrayList gamma = new ArrayList();
    for (int i = 0; i < fc.names.length - 1; ++i) {
      gamma.add(new Integer(i));
    }

    // Create network structure
    ForestAugmenter fa = new ForestAugmenter();
    Augmenter.Result r = fa.train(gamma, null, fc, qualityMeasure,
        usePriors, alpha_ijk);

    net = r.bayesNet;
    BNTools.learnParameters(net, fc, usePriors, alpha_ijk);

//    FileOutputStream out  = new FileOutputStream("dump_STAN.bif");
//    PrintStream      pOut = new PrintStream(out);
//    bestNet.save_bif(pOut);
  }
}
