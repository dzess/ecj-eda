package pl.put.poznan.eda.testing.umda;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import pl.put.poznan.eda.boa.binominal.BitBOAModel;
import pl.put.poznan.eda.umda.DenseUMDASampler;
import pl.put.poznan.eda.umda.DenseUMDAModel;
import pl.put.poznan.eda.util.IDistributionSampler;
import ec.Individual;
import ec.vector.BitVectorIndividual;
import ec.vector.ByteVectorIndividual;
import ec.vector.DoubleVectorIndividual;
import ec.vector.FloatVectorIndividual;
import ec.vector.IntegerVectorIndividual;
import ec.vector.ShortVectorIndividual;
import ec.vector.VectorIndividual;

/**
 * This tests are very important because they assure that sampling algorithm works as it is planned.
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public class DenseUMDASamplerTest extends DenseUMDATestingBase {

    private DenseUMDASampler sampler;
    private IDistributionSampler dist;

    @Before
    public void set_up() {
        this.setUpBase();
        this.setUpEvolutionStateBasic();

        // inject the sampler
        dist = this.prepareDistributionSampler();
        sampler = new DenseUMDASampler(dist);
    }

    @Test
    public void sampling_boolean_version_generates_good_subpopulation() throws Exception {

        // use 3 genes and booleans
        int genomeCardinality = 2;
        int genomeSize = 3;
        // use only two individuals
        int indSize = 2;

        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);

        this.prepareSubpopulation(indSize, model, BitVectorIndividual.class);

        BitVectorIndividual ind1 = new BitVectorIndividual();
        BitVectorIndividual ind2 = new BitVectorIndividual();

        ind1.genome = new boolean[genomeSize];
        ind2.genome = new boolean[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(BitVectorIndividual.class);

        // sampler.sampleModel(newSubpopulation, oldSubpopulation, state);
        List<Individual> newInds = sampler.sampleModel(model, indSize, species, state);

        // assert that all values in the new subpopulation are true
        for (int i = 0; i < newInds.size(); i++) {
            VectorIndividual ind = (VectorIndividual) newInds.get(i);
            boolean[] genome = (boolean[]) ind.getGenome();
            for (int j = 0; j < genome.length; j++) {
                Assert.assertEquals(true, genome[j]);
            }
        }
    }

    @Test
    public void sampling_byte_version_generates_good_subpopulation() throws Exception {
        // use 3 genes and byte values
        int genomeCardinality = 1 << Byte.SIZE;
        int genomeSize = 3;

        // use only two individuals
        int indSize = 2;

        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);
        this.prepareSubpopulation(indSize, model, ByteVectorIndividual.class);

        ByteVectorIndividual ind1 = new ByteVectorIndividual();
        ByteVectorIndividual ind2 = new ByteVectorIndividual();

        // array with 0
        ind1.genome = new byte[genomeSize];
        ind2.genome = new byte[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(ByteVectorIndividual.class);
        List<Individual> newInds = sampler.sampleModel(model, indSize, species, state);

        // go with assertions
        // assert that all values in the new subpopulation are true
        for (int i = 0; i < newInds.size(); i++) {
            VectorIndividual ind = (VectorIndividual) newInds.get(i);
            byte[] genome = (byte[]) ind.getGenome();
            for (int j = 0; j < genome.length; j++) {

                // cause the injected distribution sampler returns always index 0 of the
                // value the resulted thing will be first of byte values meaning -127
                Assert.assertEquals(Byte.MIN_VALUE, genome[j]);
            }
        }
    }

    @Ignore("with introduciton of max and min gene contraint this cannot be used anymore")
    public void sampling_short_version_generates_good_subpopulation() throws Exception {
        // use 3 genes and byte values
        int genomeCardinality = 1 << Short.SIZE;
        int genomeSize = 3;

        // use only two individuals
        int indSize = 2;

        DenseUMDAModel model = this.prepareModel(genomeCardinality, genomeSize);
        this.prepareSubpopulation(indSize, model, ShortVectorIndividual.class);

        ShortVectorIndividual ind1 = new ShortVectorIndividual();
        ShortVectorIndividual ind2 = new ShortVectorIndividual();

        // array with 0
        ind1.genome = new short[genomeSize];
        ind2.genome = new short[genomeSize];

        this.prepareSpecies(genomeSize, ind1, ind2);

        sampler.markIndividualType(ShortVectorIndividual.class);
        List<Individual> newInds = sampler.sampleModel(model, indSize, species, state);

        // go with assertions
        // assert that all values in the new subpopulation are true
        for (int i = 0; i < newInds.size(); i++) {
            VectorIndividual ind = (VectorIndividual) newInds.get(i);
            short[] genome = (short[]) ind.getGenome();
            for (int j = 0; j < genome.length; j++) {

                // cause the injected distribution sampler returns always index 0 of the
                // value the resulted thing will be first of byte values meaning -127
                Assert.assertEquals(Short.MIN_VALUE, genome[j]);
            }
        }
    }

    @Test
    public void sampling_works_with_bit_byte_short_vector() {

        Assert.assertTrue(sampler.matchIndividualType(BitVectorIndividual.class));
        Assert.assertTrue(sampler.matchIndividualType(ShortVectorIndividual.class));
        Assert.assertTrue(sampler.matchIndividualType(ByteVectorIndividual.class));

        // some of classes which are not supported (and somebody might think they are)
        Assert.assertFalse(sampler.matchIndividualType(IntegerVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(FloatVectorIndividual.class));
        Assert.assertFalse(sampler.matchIndividualType(DoubleVectorIndividual.class));
    }

    @Test
    public void sampling_works_only_with_umda_model() {
        Assert.assertTrue(sampler.matchModelType(DenseUMDAModel.class));
        // other models are not supported
        Assert.assertFalse(sampler.matchModelType(BitBOAModel.class));
    }

}
