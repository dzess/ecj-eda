/**
 *  JBNC - Bayesian Network Classifiers Toolbox <p>
 *
 *  Latest release available at http://sourceforge.net/projects/jbnc/ <p>
 *
 *  Copyright (C) 1999-2003 Jarek Sacha <p>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation; either version 2 of the License, or (at your option)
 *  any later version. <p>
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 *  more details. <p>
 *
 *  You should have received a copy of the GNU General Public License along with
 *  this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *  Place - Suite 330, Boston, MA 02111-1307, USA. <br>
 *  http://www.fsf.org/licenses/gpl.txt
 */
package jbnc.inducers;

import jb.BayesianNetworks.BayesNet;
import jbnc.dataset.DatasetInt;
import jbnc.operators.ClassDepend;
import jbnc.util.BNTools;

import java.util.ArrayList;

/**
 *  Constructs discrete naive-Bayes classifier. <br>
 *  <br>
 *  Duda R, Hart P, <i>Pattern Recognition and Scene Analysis</i> , John Wiley &
 *  Sons, 1973.
 *
 * @author     Jarek Sacha
 * @since      June 1, 1999
 */
public class NaiveBayesInducer extends BayesianInducer {

  /**
   *  Used for testing of the naive-Bayes algorithm.
   *
   * @param  arg  The command line arguments
   */
  public static void main(String[] arg) {
    try {
      jbnc.util.Timer t = new jbnc.util.Timer();
      System.out.println("\nTesting class...\n");

      String fileStem = null;
      if (arg.length > 0) {
        fileStem = arg[0];
      } else {
        fileStem = "..\\db\\vote";
      }
//        fileStem = "db_cv\\iris-0-1";

      System.out.println("Load dataset: " + fileStem);
      DatasetInt dataset = new DatasetInt();
      dataset.openC45(fileStem);
      jbnc.util.FrequencyCalc fc = new jbnc.util.FrequencyCalc(dataset);

      NaiveBayesInducer naive = new NaiveBayesInducer();
      naive.setDebugMode(true);
      naive.train(fc);
      t.stop();

      BayesNet thisNet = naive.getNetwork();
      jbnc.util.BNCTester tester = new jbnc.util.BNCTester();
      jbnc.util.BNCTester.Result r = tester.test(thisNet, dataset);
      double ratio = r.nbPass / (double) (r.nbPass + r.nbFail);
      System.out.println("Accuracy on training set = " + (ratio * 100) + "%");
      t.println("\nExecution time = ");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   *  Construct a naive-Bayes network from the training data.
   *
   * @param  fc             Description of Parameter
   * @param  usePriors      Description of Parameter
   * @param  alpha_ijk      Description of Parameter
   * @exception  Exception  Description of Exception
   */
//  public void train(DatasetInt dataset,
  public void train(jbnc.util.FrequencyCalc fc,
                    boolean usePriors,
                    double alpha_ijk) throws Exception {
    // Clean storage
    clean();

    // Some sanity checks
//    verifyDataset(dataset);

    int nbAttrib = fc.names.length - 1;

    // Set all attributes as dependent on the class node
    ArrayList gamma = new ArrayList();
    for (int i = 0; i < fc.names.length - 1; ++i) {
      gamma.add(new Integer(i));
    }

    // Construct network structure
    net = ClassDepend.create(null, gamma, fc.names);
    net.set_name("Naive-Bayes");

    // Learn parameters
    if (usePriors) {
      BNTools.learnParameters(net, fc, usePriors, alpha_ijk);
    } else {
//      BNTools.learnParameters(net, fc, true, 10e-100);
      BNTools.learnParameters(net, fc, true, DEFAULT_APLHA_IJK);
    }
  }

}
