package pl.put.poznan.eda.boa.multinominal;

import pl.put.poznan.eda.IMatchable;
import pl.put.poznan.eda.Model;
import ec.Individual;
import ec.vector.ShortVectorIndividual;

/**
 * 
 * Base class for managing the {@linkplain ShortBOAModel} representations. Follows the {@linkplain IMatchable} interface.
 * 
 * <p>
 * Used in classes:
 * <ul>
 * <li>{@linkplain ShortBOASampler}</li>
 * <li>{@linkplain ShortBOAUpdater}</li>
 * </ul>
 * </p>
 * 
 * @author Piotr Jessa
 * @since 0.0.2
 * 
 */
public abstract class ShortBOABase implements IMatchable {

    private static final long serialVersionUID = 1903143700211915203L;

    @Override
    public boolean matchModelType(Class<? extends Model> model) {
        if (model.equals(ShortBOAModel.class)) {
            return true;
        }

        return false;
    }

    @Override
    public boolean matchIndividualType(Class<? extends Individual> individual) {
        if (individual.equals(ShortVectorIndividual.class)) {
            return true;
        }
        return false;
    }

    @Override
    public void markIndividualType(Class<? extends Individual> individual) {
        // only ShortVectorIndividual is used for now so this method might be skipped
    }
}
